# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-06-05 19:15
from __future__ import unicode_literals

from decimal import Decimal
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import mptt.fields


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0002_auto_20180604_2124'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='level',
            field=models.PositiveIntegerField(db_index=True, default=0, editable=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='lft',
            field=models.PositiveIntegerField(db_index=True, default=1, editable=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='parent',
            field=mptt.fields.TreeForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='subitems', to='product.Product', verbose_name='Групповой товар'),
        ),
        migrations.AddField(
            model_name='product',
            name='rght',
            field=models.PositiveIntegerField(db_index=True, default=1, editable=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='product',
            name='tree_id',
            field=models.PositiveIntegerField(db_index=True, default=1, editable=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='productitem',
            name='is_opt',
            field=models.BooleanField(default=False, verbose_name='ОПТ'),
        ),
        migrations.AddField(
            model_name='productitem',
            name='unit',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='Ед.измерения'),
        ),
        migrations.AlterField(
            model_name='product',
            name='articul',
            field=models.CharField(blank=True, db_index=True, max_length=256, null=True, verbose_name='Артикул'),
        ),
        migrations.AlterField(
            model_name='productitem',
            name='articul',
            field=models.CharField(blank=True, db_index=True, max_length=256, null=True, verbose_name='Артикул'),
        ),
        migrations.AlterField(
            model_name='productitem',
            name='price',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10, validators=[django.core.validators.MinValueValidator(Decimal('0'))], verbose_name='Цена(за ед.)'),
        ),
        migrations.AlterField(
            model_name='productitem',
            name='price_purchase',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=10, validators=[django.core.validators.MinValueValidator(Decimal('0'))], verbose_name='Себестоимость'),
        ),
        migrations.AlterField(
            model_name='productitem',
            name='product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='product.Product', verbose_name='Товар'),
        ),
    ]
