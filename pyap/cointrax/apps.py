from django.apps import AppConfig


class CointraxAppConfig(AppConfig):
    name = 'cointrax'
    verbose_name = 'cointrax'

