from django.conf.urls import url
from settings_template.dasboard_modules.backup import do_dbbackup
from django.contrib.admin.views.decorators import staff_member_required
from settings_template.views import BackupDownload


urlpatterns = [
    url(r'^do_backup/$', staff_member_required(do_dbbackup), name='do_backup'),
    url(r'^download/backup/(?P<dump>.*)/$', BackupDownload.as_view(), name='download_backup'),
]
