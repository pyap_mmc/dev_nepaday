from django.views.generic import TemplateView
from settings_template.models import Backup


class BackupDownload(TemplateView):
    """
    Въюшка для отдачи представления.
    """
    def get_template_names(self):
        """ вместо название шаблона олучаем название файла с ДАМПОМ """
        dump = self.kwargs['dump'] if self.kwargs and self.kwargs['dump'] else None
        return dump

    def render_to_response(self, context, **response_kwargs):
        """ вместо шаблона отдаем на скачивание ДАМП """
        return Backup.download(self.get_template_names())


# ----------------------
# Метод тоже работает -
# ----------------------
# def download(request, dump):
#     """
#     Скачать бекап.
#         ex: /system/download/backup/default-marychev-888-2018-05-21-114339.psql/
#     """
#     dir_file = os.getcwd() + '/system/backup'
#     file_path = os.path.join(dir_file, dump)
#
#     if os.path.exists(file_path):
#         with open(file_path, 'rb') as fh:
#             response = HttpResponse(fh.read(), content_type="multipart/form-data")
#             response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
#             return response
#     raise Http404
# -----------------------------------------------------------------------------------------------------------------
