from django.contrib import messages
from django.utils.html import format_html
from django.http import HttpResponseRedirect
from settings_template.models import Backup


def do_dbbackup(request):
    """ Сделать бэкап и записать в базу. """
    _message = Backup.do_dbbackup(request)
    messages.success(request, format_html(_message))
    return HttpResponseRedirect(Backup.get_admin_url_change_list())




