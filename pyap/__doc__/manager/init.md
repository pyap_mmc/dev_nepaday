# проек Непадай. 
## План работ.



Вкратце что может - описание



#### Для входа в админку 

`username`: pyapman
`password`: superuser


[!]http://nepaday.ru - сайт для клонирования информации



#### После этих этапов сайт будет считаться завершенным

первая ссылка ведет на страницу сайта, вторая на ее редактирование в админке.

1. Разработка макета для основных разделов проекта.

2. Верстка утвержденного макета (основные разделы сайта):
    - Главная страница 
        * http://nepaday.pyap.ru/
        * http://nepaday.pyap.ru/admin/home/home/
        
    - Страница с контентом 
        * http://nepaday.pyap.ru/page/o-nas/
        * http://nepaday.pyap.ru/admin/page/page/1/change/
    
    - Разбел блога 
        * http://nepaday.pyap.ru/blog/nash-blog/
        * http://nepaday.pyap.ru/admin/blog/blog/
        
        - Страница с контентом - Пост.
            * http://nepaday.pyap.ru/blog/nash-blog/moda-sejchas/
            * http://nepaday.pyap.ru/admin/blog/post/2/change/    

    - Раздел каталога 
        * http://nepaday.pyap.ru/catalog/katalog-protivoskolzyashih-materialov/
        * http://nepaday.pyap.ru/admin/catalog/catalog/7/change/
           
        - каталог с товарами
            * http://nepaday.pyap.ru/catalog/alyuminievyj-zakladnoj-profil-uverennyj-shag-c-dvu/
            * http://nepaday.pyap.ru/admin/catalog/catalog/8/change/
        
        - карточка товара
            * http://nepaday.pyap.ru/alyuminievyj-zakladnoj-profil-uverennyj-shag-c-dvu/uverennyj-shag-50-mm/
            * http://nepaday.pyap.ru/admin/product/product/6/change/
      

3. Каждый раздел имеет настройки для СЕО (страница, каталог, товар ...). 
    - вновь созданные разделы автоматически заполнят СЕО-данные, т.е. на первых этапах можно не волноваться о них
    - зарегистрирует созданный элемент сайта в SITEMAP.XML
    - настроенный robots.txt
 


- Разработка модуля "Каталог:Товары" для продажи продукцией с сайта http://nepaday.ru 
    - [?]кратко описать принцип оформления и заказа / расчета
    
- Подключение верстки к движку. [?]Возможности движка (кратко)

   
- Заполнение контентом нового сайта(боевого сатйа). НЕ ТОВАРОВ (о товарах ниже)


- Настройка сервера клиента и подключение купленного домена для боевой версии сайта. 
 Аренда сервера и покупка домена осуществляется заказчиком!




#### БОНУСЫ после сдачи проекта.

1. Регистрация проекта в поисковых-службах и подключим к сайту
    - Гугл-Веб мастер
    - Яндекс Веб-мастер
    
2. Заполнение товаров нового сайта со старого. 
    - Товары 50 / 50%! Их очень много. Их программно никак не занести. Руками только. На это уйдет физически много времени. 
    Поэтому мы готовы только на 50% заполнения товаров. 
 