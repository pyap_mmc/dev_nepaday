    clone git@bitbucket.*****
    cd ******
    virtualenv --python=python3 venv
	source venv/bin/activate
	cd ~/projects/****/pyap/
	pip install -r requirements.txt
	python manage.py collectstatic --noinput
	python manage.py makemigrations
	python manage.py migrate
	python manage.py runserver
	
#  Развертывание проекта 

`nepaday` - название проекта используется как пример


Скачать или клонировать себе проект ``git@bitbucket.or*****nepaday``
Подготовка папок для проекта
    
    cd ~/projects
    
    
проверяем `ls`. Должны быть следующие директории 
* app 
* pyap 
* theme


Ставим виртуальное окружение. Активируем.

	virtualenv --python=python3 venv
	source venv/bin/activate


Устанавливаем Джанго и все необходимые зависимости. Входим в папку где лежит файл `manage.py`.
	
	 cd ~/projects/nepaday/pyap/
	 pip install -r requirements.txt


По умолчанию установливается база `SQLite` `nepaday` в проект 
	
	python manage.py collectstatic --noinput
	python manage.py makemigrations
	python manage.py migrate

Проект создан! Проверяем `python manage.py runserver`. 
Запускаем локальный сервер и переходим на страницу в `http://localhost:8000/`. 
	

[I]Заполним проект дефолтными данными

	python manage.py createsuperuser
 		name: `pyapman`
 		emal: `pyapmail@ya.ru`
 		password: `superuser`

Проверяем `python manage.py runserver`.

Создадим базу данных `nepaday` и настроем `postgres` для проекта

	sudo apt-get update
	sudo apt-get install python3-pip python3-dev libpq-dev postgresql postgresql-contrib 
	
	# Если нужен `postgis`
	# sudo apt-get install postgis gdal-bin	

	sudo -u postgres psql
	postgres=# 
    	CREATE LANGUAGE plpgsql;
		CREATE DATABASE nepaday;
		CREATE USER nepaday WITH PASSWORD 'nepaday';
		ALTER ROLE nepaday SET client_encoding TO 'utf8';
		ALTER ROLE nepaday SET default_transaction_isolation TO 'read committed';
		ALTER ROLE nepaday SET timezone TO 'UTC';
		GRANT ALL PRIVILEGES ON DATABASE nepaday TO nepaday;
		\q


Меняем настройку подключения к Базе в `~/projects/nepaday/pyap/pyap/settings.py`
```
...
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'nepaday',
        'USER': 'nepaday',
        'PASSWORD': 'nepaday',
        'HOST': 'localhost',
        'PORT': '',
    }
}
...
```
	
	python manage.py migrate


[I]Заполним проект дефолтными данными
	
	# ... создаем суперпользователя и приминяем фикстуры ...


Проверяем `python manage.py runserver`.

# Если сервер


## GIT / BITBACKET 
	
	cd ~/projects/pyap_v1.0.0

	git config --global user.name "Mihail"
	git config --global user.email pyapmail@gmail.com
	git config --global core.pager 'less -r'

	git init
	git add *
	git commit -m 'Hello World!'
	
	# --settings
	git remote add origin git@bitbucket.org:marychev/pyap_v1.0.0.git
	git push -u origin master


#### TRY/EXCEPT 

1.`Error: That port is already in use.`
	
	fuser -k 8000/tcp 


# ----      ---- #
  --- [pYAp] --- 
# ----      ---- #



