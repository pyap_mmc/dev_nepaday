Быстрая установка

    `clone git@bitbucket.org:marychev/pyap_v1.0.0.git`
    ``cd pyap_v1.0.0``
    ```virtualenv --python=python3 venv```
    ```source venv/bin/activate
    cd ~/projects/pyap_v1.0.0/pyap/
    pip install -r requirements.txt
    python manage.py collectstatic --noinput```
    python manage.py makemigrations
    python manage.py migrate
    python manage.py runserver


### Install the Packages from the Ubuntu Repositories

	sudo apt-get update
	sudo apt-get install python3-pip python3-dev libpq-dev postgresql postgresql-contrib 
	
	# if need postgis
	# sudo apt-get install postgis gdal-bin	

### Create the PostgreSQL Database and User

	sudo -u postgres psql

	postgres=# 
		CREATE DATABASE nepaday;
		CREATE USER nepaday WITH PASSWORD 'nepaday';
		ALTER ROLE nepaday SET client_encoding TO 'utf8';
		ALTER ROLE nepaday SET default_transaction_isolation TO 'read committed';
		ALTER ROLE nepaday SET timezone TO 'UTC';
		GRANT ALL PRIVILEGES ON DATABASE nepaday TO nepaday;


### upgrade pip and install the package by typing:

	sudo -H pip3 install --upgrade pip
	sudo -H pip3 install virtualenv

	python3 -m virtualenv ~/xxx/pyap/venv

	source pyap/venv/bin/activate

	pip install -r requirements.txt
    python manage.py makemigrations
    python manage.py migrate




