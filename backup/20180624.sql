--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4 (Ubuntu 10.4-0ubuntu0.18.04)
-- Dumped by pg_dump version 10.4 (Ubuntu 10.4-0ubuntu0.18.04)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: advertising_sliderhome; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.advertising_sliderhome (
    id integer NOT NULL,
    title character varying(126),
    description text,
    image character varying(500),
    url character varying(200),
    sort smallint,
    CONSTRAINT advertising_sliderhome_sort_check CHECK ((sort >= 0))
);


ALTER TABLE public.advertising_sliderhome OWNER TO nepaday;

--
-- Name: advertising_sliderhome_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.advertising_sliderhome_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.advertising_sliderhome_id_seq OWNER TO nepaday;

--
-- Name: advertising_sliderhome_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.advertising_sliderhome_id_seq OWNED BY public.advertising_sliderhome.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO nepaday;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO nepaday;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO nepaday;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO nepaday;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO nepaday;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO nepaday;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO nepaday;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO nepaday;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO nepaday;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO nepaday;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO nepaday;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO nepaday;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: blog_blog; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.blog_blog (
    id integer NOT NULL,
    title character varying(255),
    description text,
    slug character varying(50) NOT NULL,
    seo_title character varying(255),
    seo_description text,
    seo_keywords text,
    og_locale text,
    scripts text,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    html text,
    sort smallint NOT NULL,
    is_show boolean NOT NULL,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    author_id integer,
    parent_id integer,
    CONSTRAINT blog_blog_level_check CHECK ((level >= 0)),
    CONSTRAINT blog_blog_lft_check CHECK ((lft >= 0)),
    CONSTRAINT blog_blog_rght_check CHECK ((rght >= 0)),
    CONSTRAINT blog_blog_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.blog_blog OWNER TO nepaday;

--
-- Name: blog_blog_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.blog_blog_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.blog_blog_id_seq OWNER TO nepaday;

--
-- Name: blog_blog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.blog_blog_id_seq OWNED BY public.blog_blog.id;


--
-- Name: blog_blog_tags; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.blog_blog_tags (
    id integer NOT NULL,
    blog_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.blog_blog_tags OWNER TO nepaday;

--
-- Name: blog_blog_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.blog_blog_tags_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.blog_blog_tags_id_seq OWNER TO nepaday;

--
-- Name: blog_blog_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.blog_blog_tags_id_seq OWNED BY public.blog_blog_tags.id;


--
-- Name: blog_blogimage; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.blog_blogimage (
    id integer NOT NULL,
    image character varying(500),
    image_title character varying(255),
    image_is_main boolean NOT NULL,
    image_description text,
    blog_id integer NOT NULL
);


ALTER TABLE public.blog_blogimage OWNER TO nepaday;

--
-- Name: blog_blogimage_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.blog_blogimage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.blog_blogimage_id_seq OWNER TO nepaday;

--
-- Name: blog_blogimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.blog_blogimage_id_seq OWNED BY public.blog_blogimage.id;


--
-- Name: blog_comment; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.blog_comment (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    text text NOT NULL,
    ip_address inet,
    username character varying(125),
    email character varying(254) NOT NULL,
    is_show boolean NOT NULL,
    post_id integer,
    user_id integer
);


ALTER TABLE public.blog_comment OWNER TO nepaday;

--
-- Name: blog_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.blog_comment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.blog_comment_id_seq OWNER TO nepaday;

--
-- Name: blog_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.blog_comment_id_seq OWNED BY public.blog_comment.id;


--
-- Name: blog_post; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.blog_post (
    id integer NOT NULL,
    title character varying(255),
    description text,
    slug character varying(50) NOT NULL,
    seo_title character varying(255),
    seo_description text,
    seo_keywords text,
    og_locale text,
    scripts text,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    html text,
    sort smallint NOT NULL,
    is_show boolean NOT NULL,
    is_allow_comments boolean NOT NULL,
    comment_count integer NOT NULL,
    author_id integer,
    blog_id integer
);


ALTER TABLE public.blog_post OWNER TO nepaday;

--
-- Name: blog_post_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.blog_post_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.blog_post_id_seq OWNER TO nepaday;

--
-- Name: blog_post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.blog_post_id_seq OWNED BY public.blog_post.id;


--
-- Name: blog_post_tags; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.blog_post_tags (
    id integer NOT NULL,
    post_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.blog_post_tags OWNER TO nepaday;

--
-- Name: blog_post_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.blog_post_tags_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.blog_post_tags_id_seq OWNER TO nepaday;

--
-- Name: blog_post_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.blog_post_tags_id_seq OWNED BY public.blog_post_tags.id;


--
-- Name: blog_postimage; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.blog_postimage (
    id integer NOT NULL,
    image character varying(500),
    image_title character varying(255),
    image_is_main boolean NOT NULL,
    image_description text,
    post_id integer NOT NULL
);


ALTER TABLE public.blog_postimage OWNER TO nepaday;

--
-- Name: blog_postimage_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.blog_postimage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.blog_postimage_id_seq OWNER TO nepaday;

--
-- Name: blog_postimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.blog_postimage_id_seq OWNED BY public.blog_postimage.id;


--
-- Name: catalog_catalog; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.catalog_catalog (
    id integer NOT NULL,
    title character varying(255),
    description text,
    slug character varying(50) NOT NULL,
    seo_title character varying(255),
    seo_description text,
    seo_keywords text,
    og_locale text,
    scripts text,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    html text,
    sort smallint NOT NULL,
    is_show boolean NOT NULL,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    author_id integer,
    parent_id integer,
    CONSTRAINT catalog_catalog_level_check CHECK ((level >= 0)),
    CONSTRAINT catalog_catalog_lft_check CHECK ((lft >= 0)),
    CONSTRAINT catalog_catalog_rght_check CHECK ((rght >= 0)),
    CONSTRAINT catalog_catalog_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.catalog_catalog OWNER TO nepaday;

--
-- Name: catalog_catalog_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.catalog_catalog_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.catalog_catalog_id_seq OWNER TO nepaday;

--
-- Name: catalog_catalog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.catalog_catalog_id_seq OWNED BY public.catalog_catalog.id;


--
-- Name: catalog_catalog_tags; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.catalog_catalog_tags (
    id integer NOT NULL,
    catalog_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.catalog_catalog_tags OWNER TO nepaday;

--
-- Name: catalog_catalog_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.catalog_catalog_tags_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.catalog_catalog_tags_id_seq OWNER TO nepaday;

--
-- Name: catalog_catalog_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.catalog_catalog_tags_id_seq OWNED BY public.catalog_catalog_tags.id;


--
-- Name: catalog_catalogimage; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.catalog_catalogimage (
    id integer NOT NULL,
    image character varying(500),
    image_title character varying(255),
    image_is_main boolean NOT NULL,
    image_description text,
    catalog_id integer NOT NULL
);


ALTER TABLE public.catalog_catalogimage OWNER TO nepaday;

--
-- Name: catalog_catalogimage_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.catalog_catalogimage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.catalog_catalogimage_id_seq OWNER TO nepaday;

--
-- Name: catalog_catalogimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.catalog_catalogimage_id_seq OWNED BY public.catalog_catalogimage.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO nepaday;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO nepaday;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO nepaday;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO nepaday;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO nepaday;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO nepaday;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO nepaday;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO nepaday;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.django_site_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO nepaday;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.django_site_id_seq OWNED BY public.django_site.id;


--
-- Name: gallery_gallery; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.gallery_gallery (
    id integer NOT NULL,
    title character varying(255),
    description text,
    slug character varying(50) NOT NULL,
    seo_title character varying(255),
    seo_description text,
    seo_keywords text,
    og_locale text,
    scripts text,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    html text,
    sort smallint NOT NULL,
    is_show boolean NOT NULL,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    author_id integer,
    parent_id integer,
    CONSTRAINT gallery_gallery_level_check CHECK ((level >= 0)),
    CONSTRAINT gallery_gallery_lft_check CHECK ((lft >= 0)),
    CONSTRAINT gallery_gallery_rght_check CHECK ((rght >= 0)),
    CONSTRAINT gallery_gallery_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.gallery_gallery OWNER TO nepaday;

--
-- Name: gallery_gallery_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.gallery_gallery_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gallery_gallery_id_seq OWNER TO nepaday;

--
-- Name: gallery_gallery_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.gallery_gallery_id_seq OWNED BY public.gallery_gallery.id;


--
-- Name: gallery_gallery_tags; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.gallery_gallery_tags (
    id integer NOT NULL,
    gallery_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.gallery_gallery_tags OWNER TO nepaday;

--
-- Name: gallery_gallery_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.gallery_gallery_tags_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gallery_gallery_tags_id_seq OWNER TO nepaday;

--
-- Name: gallery_gallery_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.gallery_gallery_tags_id_seq OWNED BY public.gallery_gallery_tags.id;


--
-- Name: gallery_galleryimage; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.gallery_galleryimage (
    id integer NOT NULL,
    image character varying(500),
    image_title character varying(255),
    image_is_main boolean NOT NULL,
    image_description text,
    gallery_id integer NOT NULL
);


ALTER TABLE public.gallery_galleryimage OWNER TO nepaday;

--
-- Name: gallery_galleryimage_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.gallery_galleryimage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gallery_galleryimage_id_seq OWNER TO nepaday;

--
-- Name: gallery_galleryimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.gallery_galleryimage_id_seq OWNED BY public.gallery_galleryimage.id;


--
-- Name: home_home; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.home_home (
    id integer NOT NULL,
    slug character varying(50) NOT NULL,
    seo_title character varying(255),
    seo_description text,
    seo_keywords text,
    og_locale text,
    scripts text,
    title character varying(255) NOT NULL,
    html text,
    is_show boolean NOT NULL,
    blog_id integer
);


ALTER TABLE public.home_home OWNER TO nepaday;

--
-- Name: home_home_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.home_home_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.home_home_id_seq OWNER TO nepaday;

--
-- Name: home_home_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.home_home_id_seq OWNED BY public.home_home.id;


--
-- Name: home_homeimage; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.home_homeimage (
    id integer NOT NULL,
    image character varying(500),
    image_title character varying(255),
    image_is_main boolean NOT NULL,
    image_description text,
    home_id integer NOT NULL
);


ALTER TABLE public.home_homeimage OWNER TO nepaday;

--
-- Name: home_homeimage_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.home_homeimage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.home_homeimage_id_seq OWNER TO nepaday;

--
-- Name: home_homeimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.home_homeimage_id_seq OWNED BY public.home_homeimage.id;


--
-- Name: include_area_includearea; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.include_area_includearea (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    is_show boolean NOT NULL,
    image character varying(500),
    description text,
    html_code character varying(255),
    code character varying(20) NOT NULL,
    sort smallint NOT NULL,
    url character varying(200),
    CONSTRAINT include_area_includearea_sort_check CHECK ((sort >= 0))
);


ALTER TABLE public.include_area_includearea OWNER TO nepaday;

--
-- Name: include_area_includearea_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.include_area_includearea_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.include_area_includearea_id_seq OWNER TO nepaday;

--
-- Name: include_area_includearea_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.include_area_includearea_id_seq OWNED BY public.include_area_includearea.id;


--
-- Name: menu_mainmenu; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.menu_mainmenu (
    id integer NOT NULL,
    name character varying(80),
    is_show boolean NOT NULL,
    sort smallint NOT NULL,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    blog_id integer,
    catalog_id integer,
    gallery_id integer,
    page_id integer,
    parent_id integer,
    CONSTRAINT menu_mainmenu_level_check CHECK ((level >= 0)),
    CONSTRAINT menu_mainmenu_lft_check CHECK ((lft >= 0)),
    CONSTRAINT menu_mainmenu_rght_check CHECK ((rght >= 0)),
    CONSTRAINT menu_mainmenu_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.menu_mainmenu OWNER TO nepaday;

--
-- Name: menu_mainmenu_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.menu_mainmenu_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.menu_mainmenu_id_seq OWNER TO nepaday;

--
-- Name: menu_mainmenu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.menu_mainmenu_id_seq OWNED BY public.menu_mainmenu.id;


--
-- Name: order_order; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.order_order (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    first_name character varying(50) NOT NULL,
    last_name character varying(50) NOT NULL,
    email character varying(254) NOT NULL,
    address character varying(250) NOT NULL,
    postal_code character varying(20),
    city character varying(100),
    total_cost numeric(10,2) NOT NULL,
    ttn character varying(128),
    comment text,
    status_id integer,
    user_id integer NOT NULL
);


ALTER TABLE public.order_order OWNER TO nepaday;

--
-- Name: order_order_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.order_order_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_order_id_seq OWNER TO nepaday;

--
-- Name: order_order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.order_order_id_seq OWNED BY public.order_order.id;


--
-- Name: order_orderitem; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.order_orderitem (
    id integer NOT NULL,
    price numeric(10,2) NOT NULL,
    quantity smallint NOT NULL,
    order_id integer NOT NULL,
    product_item_id integer NOT NULL,
    CONSTRAINT order_orderitem_quantity_check CHECK ((quantity >= 0))
);


ALTER TABLE public.order_orderitem OWNER TO nepaday;

--
-- Name: order_orderitem_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.order_orderitem_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_orderitem_id_seq OWNER TO nepaday;

--
-- Name: order_orderitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.order_orderitem_id_seq OWNED BY public.order_orderitem.id;


--
-- Name: order_status; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.order_status (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.order_status OWNER TO nepaday;

--
-- Name: order_status_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.order_status_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_status_id_seq OWNER TO nepaday;

--
-- Name: order_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.order_status_id_seq OWNED BY public.order_status.id;


--
-- Name: order_story; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.order_story (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    total_cost numeric(10,2) NOT NULL,
    comment text,
    order_id integer NOT NULL,
    status_id integer
);


ALTER TABLE public.order_story OWNER TO nepaday;

--
-- Name: order_story_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.order_story_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_story_id_seq OWNER TO nepaday;

--
-- Name: order_story_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.order_story_id_seq OWNED BY public.order_story.id;


--
-- Name: page_page; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.page_page (
    id integer NOT NULL,
    title character varying(255),
    description text,
    slug character varying(50) NOT NULL,
    seo_title character varying(255),
    seo_description text,
    seo_keywords text,
    og_locale text,
    scripts text,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    html text,
    sort smallint NOT NULL,
    is_show boolean NOT NULL,
    is_allow_comments boolean NOT NULL,
    author_id integer
);


ALTER TABLE public.page_page OWNER TO nepaday;

--
-- Name: page_page_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.page_page_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.page_page_id_seq OWNER TO nepaday;

--
-- Name: page_page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.page_page_id_seq OWNED BY public.page_page.id;


--
-- Name: page_page_tags; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.page_page_tags (
    id integer NOT NULL,
    page_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.page_page_tags OWNER TO nepaday;

--
-- Name: page_page_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.page_page_tags_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.page_page_tags_id_seq OWNER TO nepaday;

--
-- Name: page_page_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.page_page_tags_id_seq OWNED BY public.page_page_tags.id;


--
-- Name: page_pagecomment; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.page_pagecomment (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    text text NOT NULL,
    ip_address inet,
    username character varying(125),
    email character varying(254) NOT NULL,
    is_show boolean NOT NULL,
    page_id integer,
    user_id integer
);


ALTER TABLE public.page_pagecomment OWNER TO nepaday;

--
-- Name: page_pagecomment_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.page_pagecomment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.page_pagecomment_id_seq OWNER TO nepaday;

--
-- Name: page_pagecomment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.page_pagecomment_id_seq OWNED BY public.page_pagecomment.id;


--
-- Name: page_pageimage; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.page_pageimage (
    id integer NOT NULL,
    image character varying(500),
    image_title character varying(255),
    image_is_main boolean NOT NULL,
    image_description text,
    page_id integer NOT NULL
);


ALTER TABLE public.page_pageimage OWNER TO nepaday;

--
-- Name: page_pageimage_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.page_pageimage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.page_pageimage_id_seq OWNER TO nepaday;

--
-- Name: page_pageimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.page_pageimage_id_seq OWNED BY public.page_pageimage.id;


--
-- Name: product_product; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.product_product (
    id integer NOT NULL,
    title character varying(255),
    description text,
    slug character varying(50) NOT NULL,
    seo_title character varying(255),
    seo_description text,
    seo_keywords text,
    og_locale text,
    scripts text,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    html text,
    sort smallint NOT NULL,
    is_show boolean NOT NULL,
    articul character varying(256),
    is_bestseller boolean NOT NULL,
    is_new boolean NOT NULL,
    author_id integer,
    level integer NOT NULL,
    lft integer NOT NULL,
    parent_id integer,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    CONSTRAINT product_product_level_check CHECK ((level >= 0)),
    CONSTRAINT product_product_lft_check CHECK ((lft >= 0)),
    CONSTRAINT product_product_rght_check CHECK ((rght >= 0)),
    CONSTRAINT product_product_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.product_product OWNER TO nepaday;

--
-- Name: product_product_catalog; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.product_product_catalog (
    id integer NOT NULL,
    product_id integer NOT NULL,
    catalog_id integer NOT NULL
);


ALTER TABLE public.product_product_catalog OWNER TO nepaday;

--
-- Name: product_product_catalog_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.product_product_catalog_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_product_catalog_id_seq OWNER TO nepaday;

--
-- Name: product_product_catalog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.product_product_catalog_id_seq OWNED BY public.product_product_catalog.id;


--
-- Name: product_product_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.product_product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_product_id_seq OWNER TO nepaday;

--
-- Name: product_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.product_product_id_seq OWNED BY public.product_product.id;


--
-- Name: product_product_recommend_products; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.product_product_recommend_products (
    id integer NOT NULL,
    from_product_id integer NOT NULL,
    to_product_id integer NOT NULL
);


ALTER TABLE public.product_product_recommend_products OWNER TO nepaday;

--
-- Name: product_product_recommend_products_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.product_product_recommend_products_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_product_recommend_products_id_seq OWNER TO nepaday;

--
-- Name: product_product_recommend_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.product_product_recommend_products_id_seq OWNED BY public.product_product_recommend_products.id;


--
-- Name: product_product_tags; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.product_product_tags (
    id integer NOT NULL,
    product_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.product_product_tags OWNER TO nepaday;

--
-- Name: product_product_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.product_product_tags_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_product_tags_id_seq OWNER TO nepaday;

--
-- Name: product_product_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.product_product_tags_id_seq OWNED BY public.product_product_tags.id;


--
-- Name: product_productcomment; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.product_productcomment (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    text text NOT NULL,
    ip_address inet,
    username character varying(125),
    email character varying(254) NOT NULL,
    is_show boolean NOT NULL,
    product_id integer,
    user_id integer
);


ALTER TABLE public.product_productcomment OWNER TO nepaday;

--
-- Name: product_productcomment_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.product_productcomment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_productcomment_id_seq OWNER TO nepaday;

--
-- Name: product_productcomment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.product_productcomment_id_seq OWNED BY public.product_productcomment.id;


--
-- Name: product_productimage; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.product_productimage (
    id integer NOT NULL,
    image character varying(500),
    image_title character varying(255),
    image_is_main boolean NOT NULL,
    image_description text,
    product_id integer NOT NULL
);


ALTER TABLE public.product_productimage OWNER TO nepaday;

--
-- Name: product_productimage_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.product_productimage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_productimage_id_seq OWNER TO nepaday;

--
-- Name: product_productimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.product_productimage_id_seq OWNED BY public.product_productimage.id;


--
-- Name: product_productitem; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.product_productitem (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    name character varying(256) NOT NULL,
    articul character varying(256),
    price numeric(10,2) NOT NULL,
    price_discount numeric(10,2) NOT NULL,
    price_purchase numeric(10,2) NOT NULL,
    quantity integer NOT NULL,
    is_main boolean NOT NULL,
    product_id integer NOT NULL,
    is_opt boolean NOT NULL,
    unit smallint NOT NULL,
    from_qty smallint,
    to_qty smallint,
    CONSTRAINT product_productitem_from_qty_check CHECK ((from_qty >= 0)),
    CONSTRAINT product_productitem_to_qty_check CHECK ((to_qty >= 0)),
    CONSTRAINT product_productitem_unit_check CHECK ((unit >= 0))
);


ALTER TABLE public.product_productitem OWNER TO nepaday;

--
-- Name: product_productitem_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.product_productitem_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_productitem_id_seq OWNER TO nepaday;

--
-- Name: product_productitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.product_productitem_id_seq OWNED BY public.product_productitem.id;


--
-- Name: settings_template_footer; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.settings_template_footer (
    id integer NOT NULL,
    title character varying(125) NOT NULL,
    is_show boolean NOT NULL,
    text_info_id integer
);


ALTER TABLE public.settings_template_footer OWNER TO nepaday;

--
-- Name: settings_template_footer_blogs; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.settings_template_footer_blogs (
    id integer NOT NULL,
    footer_id integer NOT NULL,
    blog_id integer NOT NULL
);


ALTER TABLE public.settings_template_footer_blogs OWNER TO nepaday;

--
-- Name: settings_template_footer_blogs_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.settings_template_footer_blogs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_template_footer_blogs_id_seq OWNER TO nepaday;

--
-- Name: settings_template_footer_blogs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.settings_template_footer_blogs_id_seq OWNED BY public.settings_template_footer_blogs.id;


--
-- Name: settings_template_footer_catalog; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.settings_template_footer_catalog (
    id integer NOT NULL,
    footer_id integer NOT NULL,
    catalog_id integer NOT NULL
);


ALTER TABLE public.settings_template_footer_catalog OWNER TO nepaday;

--
-- Name: settings_template_footer_catalog_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.settings_template_footer_catalog_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_template_footer_catalog_id_seq OWNER TO nepaday;

--
-- Name: settings_template_footer_catalog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.settings_template_footer_catalog_id_seq OWNED BY public.settings_template_footer_catalog.id;


--
-- Name: settings_template_footer_galleries; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.settings_template_footer_galleries (
    id integer NOT NULL,
    footer_id integer NOT NULL,
    gallery_id integer NOT NULL
);


ALTER TABLE public.settings_template_footer_galleries OWNER TO nepaday;

--
-- Name: settings_template_footer_galleries_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.settings_template_footer_galleries_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_template_footer_galleries_id_seq OWNER TO nepaday;

--
-- Name: settings_template_footer_galleries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.settings_template_footer_galleries_id_seq OWNED BY public.settings_template_footer_galleries.id;


--
-- Name: settings_template_footer_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.settings_template_footer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_template_footer_id_seq OWNER TO nepaday;

--
-- Name: settings_template_footer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.settings_template_footer_id_seq OWNED BY public.settings_template_footer.id;


--
-- Name: settings_template_footer_list_link; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.settings_template_footer_list_link (
    id integer NOT NULL,
    footer_id integer NOT NULL,
    listlink_id integer NOT NULL
);


ALTER TABLE public.settings_template_footer_list_link OWNER TO nepaday;

--
-- Name: settings_template_footer_list_link_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.settings_template_footer_list_link_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_template_footer_list_link_id_seq OWNER TO nepaday;

--
-- Name: settings_template_footer_list_link_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.settings_template_footer_list_link_id_seq OWNED BY public.settings_template_footer_list_link.id;


--
-- Name: settings_template_footer_page; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.settings_template_footer_page (
    id integer NOT NULL,
    footer_id integer NOT NULL,
    page_id integer NOT NULL
);


ALTER TABLE public.settings_template_footer_page OWNER TO nepaday;

--
-- Name: settings_template_footer_page_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.settings_template_footer_page_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_template_footer_page_id_seq OWNER TO nepaday;

--
-- Name: settings_template_footer_page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.settings_template_footer_page_id_seq OWNED BY public.settings_template_footer_page.id;


--
-- Name: settings_template_settingstemplate; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.settings_template_settingstemplate (
    id integer NOT NULL,
    title character varying(125) NOT NULL,
    is_included boolean NOT NULL,
    email character varying(254) NOT NULL,
    phone character varying(30),
    address character varying(255),
    logo character varying(500),
    robots_txt text,
    terms_of_use text,
    scripts text,
    footer_id integer,
    home_id integer,
    site_id integer,
    meta text
);


ALTER TABLE public.settings_template_settingstemplate OWNER TO nepaday;

--
-- Name: settings_template_settingstemplate_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.settings_template_settingstemplate_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_template_settingstemplate_id_seq OWNER TO nepaday;

--
-- Name: settings_template_settingstemplate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.settings_template_settingstemplate_id_seq OWNED BY public.settings_template_settingstemplate.id;


--
-- Name: site_info_listlink; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.site_info_listlink (
    id integer NOT NULL,
    title character varying(125) NOT NULL,
    url character varying(255),
    sort smallint NOT NULL,
    is_show boolean NOT NULL,
    type_link character varying(1) NOT NULL,
    CONSTRAINT site_info_listlink_sort_check CHECK ((sort >= 0))
);


ALTER TABLE public.site_info_listlink OWNER TO nepaday;

--
-- Name: site_info_listlink_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.site_info_listlink_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.site_info_listlink_id_seq OWNER TO nepaday;

--
-- Name: site_info_listlink_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.site_info_listlink_id_seq OWNED BY public.site_info_listlink.id;


--
-- Name: site_info_socialnetwork; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.site_info_socialnetwork (
    id integer NOT NULL,
    image_title character varying(255),
    image_is_main boolean NOT NULL,
    image_description text,
    title character varying(255),
    image character varying(512),
    html_link character varying(512),
    url character varying(200)
);


ALTER TABLE public.site_info_socialnetwork OWNER TO nepaday;

--
-- Name: site_info_socialnetwork_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.site_info_socialnetwork_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.site_info_socialnetwork_id_seq OWNER TO nepaday;

--
-- Name: site_info_socialnetwork_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.site_info_socialnetwork_id_seq OWNED BY public.site_info_socialnetwork.id;


--
-- Name: site_info_tag; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.site_info_tag (
    id integer NOT NULL,
    title character varying(255) NOT NULL
);


ALTER TABLE public.site_info_tag OWNER TO nepaday;

--
-- Name: site_info_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.site_info_tag_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.site_info_tag_id_seq OWNER TO nepaday;

--
-- Name: site_info_tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.site_info_tag_id_seq OWNED BY public.site_info_tag.id;


--
-- Name: site_info_textinfo; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.site_info_textinfo (
    id integer NOT NULL,
    title character varying(125) NOT NULL,
    html text NOT NULL
);


ALTER TABLE public.site_info_textinfo OWNER TO nepaday;

--
-- Name: site_info_textinfo_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.site_info_textinfo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.site_info_textinfo_id_seq OWNER TO nepaday;

--
-- Name: site_info_textinfo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.site_info_textinfo_id_seq OWNED BY public.site_info_textinfo.id;


--
-- Name: users_userlink; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.users_userlink (
    id integer NOT NULL,
    anchor character varying(100),
    url character varying(200),
    user_id integer NOT NULL
);


ALTER TABLE public.users_userlink OWNER TO nepaday;

--
-- Name: users_userlink_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.users_userlink_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_userlink_id_seq OWNER TO nepaday;

--
-- Name: users_userlink_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.users_userlink_id_seq OWNED BY public.users_userlink.id;


--
-- Name: users_userprofile; Type: TABLE; Schema: public; Owner: nepaday
--

CREATE TABLE public.users_userprofile (
    id integer NOT NULL,
    avatar character varying(500),
    patronymic character varying(155),
    birthday date,
    phone character varying(32),
    address character varying(256),
    about text,
    id_signed_news boolean NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.users_userprofile OWNER TO nepaday;

--
-- Name: users_userprofile_id_seq; Type: SEQUENCE; Schema: public; Owner: nepaday
--

CREATE SEQUENCE public.users_userprofile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_userprofile_id_seq OWNER TO nepaday;

--
-- Name: users_userprofile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nepaday
--

ALTER SEQUENCE public.users_userprofile_id_seq OWNED BY public.users_userprofile.id;


--
-- Name: advertising_sliderhome id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.advertising_sliderhome ALTER COLUMN id SET DEFAULT nextval('public.advertising_sliderhome_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: blog_blog id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_blog ALTER COLUMN id SET DEFAULT nextval('public.blog_blog_id_seq'::regclass);


--
-- Name: blog_blog_tags id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_blog_tags ALTER COLUMN id SET DEFAULT nextval('public.blog_blog_tags_id_seq'::regclass);


--
-- Name: blog_blogimage id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_blogimage ALTER COLUMN id SET DEFAULT nextval('public.blog_blogimage_id_seq'::regclass);


--
-- Name: blog_comment id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_comment ALTER COLUMN id SET DEFAULT nextval('public.blog_comment_id_seq'::regclass);


--
-- Name: blog_post id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_post ALTER COLUMN id SET DEFAULT nextval('public.blog_post_id_seq'::regclass);


--
-- Name: blog_post_tags id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_post_tags ALTER COLUMN id SET DEFAULT nextval('public.blog_post_tags_id_seq'::regclass);


--
-- Name: blog_postimage id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_postimage ALTER COLUMN id SET DEFAULT nextval('public.blog_postimage_id_seq'::regclass);


--
-- Name: catalog_catalog id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.catalog_catalog ALTER COLUMN id SET DEFAULT nextval('public.catalog_catalog_id_seq'::regclass);


--
-- Name: catalog_catalog_tags id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.catalog_catalog_tags ALTER COLUMN id SET DEFAULT nextval('public.catalog_catalog_tags_id_seq'::regclass);


--
-- Name: catalog_catalogimage id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.catalog_catalogimage ALTER COLUMN id SET DEFAULT nextval('public.catalog_catalogimage_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: django_site id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.django_site ALTER COLUMN id SET DEFAULT nextval('public.django_site_id_seq'::regclass);


--
-- Name: gallery_gallery id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.gallery_gallery ALTER COLUMN id SET DEFAULT nextval('public.gallery_gallery_id_seq'::regclass);


--
-- Name: gallery_gallery_tags id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.gallery_gallery_tags ALTER COLUMN id SET DEFAULT nextval('public.gallery_gallery_tags_id_seq'::regclass);


--
-- Name: gallery_galleryimage id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.gallery_galleryimage ALTER COLUMN id SET DEFAULT nextval('public.gallery_galleryimage_id_seq'::regclass);


--
-- Name: home_home id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.home_home ALTER COLUMN id SET DEFAULT nextval('public.home_home_id_seq'::regclass);


--
-- Name: home_homeimage id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.home_homeimage ALTER COLUMN id SET DEFAULT nextval('public.home_homeimage_id_seq'::regclass);


--
-- Name: include_area_includearea id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.include_area_includearea ALTER COLUMN id SET DEFAULT nextval('public.include_area_includearea_id_seq'::regclass);


--
-- Name: menu_mainmenu id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.menu_mainmenu ALTER COLUMN id SET DEFAULT nextval('public.menu_mainmenu_id_seq'::regclass);


--
-- Name: order_order id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.order_order ALTER COLUMN id SET DEFAULT nextval('public.order_order_id_seq'::regclass);


--
-- Name: order_orderitem id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.order_orderitem ALTER COLUMN id SET DEFAULT nextval('public.order_orderitem_id_seq'::regclass);


--
-- Name: order_status id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.order_status ALTER COLUMN id SET DEFAULT nextval('public.order_status_id_seq'::regclass);


--
-- Name: order_story id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.order_story ALTER COLUMN id SET DEFAULT nextval('public.order_story_id_seq'::regclass);


--
-- Name: page_page id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.page_page ALTER COLUMN id SET DEFAULT nextval('public.page_page_id_seq'::regclass);


--
-- Name: page_page_tags id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.page_page_tags ALTER COLUMN id SET DEFAULT nextval('public.page_page_tags_id_seq'::regclass);


--
-- Name: page_pagecomment id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.page_pagecomment ALTER COLUMN id SET DEFAULT nextval('public.page_pagecomment_id_seq'::regclass);


--
-- Name: page_pageimage id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.page_pageimage ALTER COLUMN id SET DEFAULT nextval('public.page_pageimage_id_seq'::regclass);


--
-- Name: product_product id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product ALTER COLUMN id SET DEFAULT nextval('public.product_product_id_seq'::regclass);


--
-- Name: product_product_catalog id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product_catalog ALTER COLUMN id SET DEFAULT nextval('public.product_product_catalog_id_seq'::regclass);


--
-- Name: product_product_recommend_products id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product_recommend_products ALTER COLUMN id SET DEFAULT nextval('public.product_product_recommend_products_id_seq'::regclass);


--
-- Name: product_product_tags id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product_tags ALTER COLUMN id SET DEFAULT nextval('public.product_product_tags_id_seq'::regclass);


--
-- Name: product_productcomment id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_productcomment ALTER COLUMN id SET DEFAULT nextval('public.product_productcomment_id_seq'::regclass);


--
-- Name: product_productimage id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_productimage ALTER COLUMN id SET DEFAULT nextval('public.product_productimage_id_seq'::regclass);


--
-- Name: product_productitem id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_productitem ALTER COLUMN id SET DEFAULT nextval('public.product_productitem_id_seq'::regclass);


--
-- Name: settings_template_footer id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer ALTER COLUMN id SET DEFAULT nextval('public.settings_template_footer_id_seq'::regclass);


--
-- Name: settings_template_footer_blogs id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_blogs ALTER COLUMN id SET DEFAULT nextval('public.settings_template_footer_blogs_id_seq'::regclass);


--
-- Name: settings_template_footer_catalog id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_catalog ALTER COLUMN id SET DEFAULT nextval('public.settings_template_footer_catalog_id_seq'::regclass);


--
-- Name: settings_template_footer_galleries id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_galleries ALTER COLUMN id SET DEFAULT nextval('public.settings_template_footer_galleries_id_seq'::regclass);


--
-- Name: settings_template_footer_list_link id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_list_link ALTER COLUMN id SET DEFAULT nextval('public.settings_template_footer_list_link_id_seq'::regclass);


--
-- Name: settings_template_footer_page id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_page ALTER COLUMN id SET DEFAULT nextval('public.settings_template_footer_page_id_seq'::regclass);


--
-- Name: settings_template_settingstemplate id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_settingstemplate ALTER COLUMN id SET DEFAULT nextval('public.settings_template_settingstemplate_id_seq'::regclass);


--
-- Name: site_info_listlink id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.site_info_listlink ALTER COLUMN id SET DEFAULT nextval('public.site_info_listlink_id_seq'::regclass);


--
-- Name: site_info_socialnetwork id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.site_info_socialnetwork ALTER COLUMN id SET DEFAULT nextval('public.site_info_socialnetwork_id_seq'::regclass);


--
-- Name: site_info_tag id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.site_info_tag ALTER COLUMN id SET DEFAULT nextval('public.site_info_tag_id_seq'::regclass);


--
-- Name: site_info_textinfo id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.site_info_textinfo ALTER COLUMN id SET DEFAULT nextval('public.site_info_textinfo_id_seq'::regclass);


--
-- Name: users_userlink id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.users_userlink ALTER COLUMN id SET DEFAULT nextval('public.users_userlink_id_seq'::regclass);


--
-- Name: users_userprofile id; Type: DEFAULT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.users_userprofile ALTER COLUMN id SET DEFAULT nextval('public.users_userprofile_id_seq'::regclass);


--
-- Data for Name: advertising_sliderhome; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.advertising_sliderhome (id, title, description, image, url, sort) FROM stdin;
1	Слайдер 1	Скользкие ступени?\r\nБоязнь поскользнуться и упасть?\r\nКрасивая отделка Вашей лестницы, ванной и других \r\nповерхностей, где есть риск поскользнуться?	demo/building-lines.jpg	\N	1000
3	Слайдер 3	Описание 3	demo/doska1.jpg	\N	3000
2	Слайдер 2	Описание 2	demo/sibir_vid_1_result.jpeg	\N	2000
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add FileBrowser	1	add_filebrowser
2	Can change FileBrowser	1	change_filebrowser
3	Can delete FileBrowser	1	delete_filebrowser
4	Can add log entry	2	add_logentry
5	Can change log entry	2	change_logentry
6	Can delete log entry	2	delete_logentry
7	Can add site	3	add_site
8	Can change site	3	change_site
9	Can delete site	3	delete_site
10	Can add permission	4	add_permission
11	Can change permission	4	change_permission
12	Can delete permission	4	delete_permission
13	Can add group	5	add_group
14	Can change group	5	change_group
15	Can delete group	5	delete_group
16	Can add user	6	add_user
17	Can change user	6	change_user
18	Can delete user	6	delete_user
19	Can add content type	7	add_contenttype
20	Can change content type	7	change_contenttype
21	Can delete content type	7	delete_contenttype
22	Can add session	8	add_session
23	Can change session	8	change_session
24	Can delete session	8	delete_session
25	Can add слайды	9	add_sliderhome
26	Can change слайды	9	change_sliderhome
27	Can delete слайды	9	delete_sliderhome
28	Can add Блог	10	add_blog
29	Can change Блог	10	change_blog
30	Can delete Блог	10	delete_blog
31	Can add Фотографию	11	add_blogimage
32	Can change Фотографию	11	change_blogimage
33	Can delete Фотографию	11	delete_blogimage
34	Can add Коментарий	12	add_comment
35	Can change Коментарий	12	change_comment
36	Can delete Коментарий	12	delete_comment
37	Can add Пост	13	add_post
38	Can change Пост	13	change_post
39	Can delete Пост	13	delete_post
40	Can add Фотографию	14	add_postimage
41	Can change Фотографию	14	change_postimage
42	Can delete Фотографию	14	delete_postimage
43	Can add Галерею	15	add_gallery
44	Can change Галерею	15	change_gallery
45	Can delete Галерею	15	delete_gallery
46	Can add Фотографию	16	add_galleryimage
47	Can change Фотографию	16	change_galleryimage
48	Can delete Фотографию	16	delete_galleryimage
49	Can add Каталог	17	add_catalog
50	Can change Каталог	17	change_catalog
51	Can delete Каталог	17	delete_catalog
52	Can add Фотографию	18	add_catalogimage
53	Can change Фотографию	18	change_catalogimage
54	Can delete Фотографию	18	delete_catalogimage
55	Can add товар	19	add_product
56	Can change товар	19	change_product
57	Can delete товар	19	delete_product
58	Can add Коментарий	20	add_productcomment
59	Can change Коментарий	20	change_productcomment
60	Can delete Коментарий	20	delete_productcomment
61	Can add Фотографию	21	add_productimage
62	Can change Фотографию	21	change_productimage
63	Can delete Фотографию	21	delete_productimage
64	Can add Вариант продукта	22	add_productitem
65	Can change Вариант продукта	22	change_productitem
66	Can delete Вариант продукта	22	delete_productitem
67	Can add заказ	23	add_order
68	Can change заказ	23	change_order
69	Can delete заказ	23	delete_order
70	Can add пункт заказа	24	add_orderitem
71	Can change пункт заказа	24	change_orderitem
72	Can delete пункт заказа	24	delete_orderitem
73	Can add Статус заказа	25	add_status
74	Can change Статус заказа	25	change_status
75	Can delete Статус заказа	25	delete_status
76	Can add Историю	26	add_story
77	Can change Историю	26	change_story
78	Can delete Историю	26	delete_story
79	Can add user link	27	add_userlink
80	Can change user link	27	change_userlink
81	Can delete user link	27	delete_userlink
82	Can add user profile	28	add_userprofile
83	Can change user profile	28	change_userprofile
84	Can delete user profile	28	delete_userprofile
85	Can add главная страница	29	add_home
86	Can change главная страница	29	change_home
87	Can delete главная страница	29	delete_home
88	Can add Фото	30	add_homeimage
89	Can change Фото	30	change_homeimage
90	Can delete Фото	30	delete_homeimage
91	Can add пункт меню	31	add_mainmenu
92	Can change пункт меню	31	change_mainmenu
93	Can delete пункт меню	31	delete_mainmenu
94	Can add страница	32	add_page
95	Can change страница	32	change_page
96	Can delete страница	32	delete_page
97	Can add Коментарий	33	add_pagecomment
98	Can change Коментарий	33	change_pagecomment
99	Can delete Коментарий	33	delete_pagecomment
100	Can add Фотографию	34	add_pageimage
101	Can change Фотографию	34	change_pageimage
102	Can delete Фотографию	34	delete_pageimage
103	Can add быстрая ссылка	35	add_listlink
104	Can change быстрая ссылка	35	change_listlink
105	Can delete быстрая ссылка	35	delete_listlink
106	Can add социальная сеть	36	add_socialnetwork
107	Can change социальная сеть	36	change_socialnetwork
108	Can delete социальная сеть	36	delete_socialnetwork
109	Can add Тэг	37	add_tag
110	Can change Тэг	37	change_tag
111	Can delete Тэг	37	delete_tag
112	Can add текстовая информация	38	add_textinfo
113	Can change текстовая информация	38	change_textinfo
114	Can delete текстовая информация	38	delete_textinfo
115	Can add футер	39	add_footer
116	Can change футер	39	change_footer
117	Can delete футер	39	delete_footer
118	Can add настройку шаблона	40	add_settingstemplate
119	Can change настройку шаблона	40	change_settingstemplate
120	Can delete настройку шаблона	40	delete_settingstemplate
121	Can add Включаемую область	41	add_includearea
122	Can change Включаемую область	41	change_includearea
123	Can delete Включаемую область	41	delete_includearea
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$36000$VPCcX292r4kj$rla+32p+19R0Pk6SK63An1GM29MSX7sT4fQh/++Mq6w=	2018-06-19 17:19:48.545632+00	t	pyapman			pyapmail@ya.ru	t	t	2018-06-18 19:38:43.728798+00
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: blog_blog; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.blog_blog (id, title, description, slug, seo_title, seo_description, seo_keywords, og_locale, scripts, created, updated, html, sort, is_show, lft, rght, tree_id, level, author_id, parent_id) FROM stdin;
1	СТАТЬИ	Падения вызывают 30% всех регистрируемых несчастных случаев на рабочем месте - эта цифра не может не волновать! Опасность таится в производственных и торговых помещениях, хранилищах, заводских цехах, душевых и кабинах для переодевания, на рампах, лестницах и металлических настилах - везде, где люди ежедневно ведут работу день за днем, неизбежно рискуя поскользнуться на мокром, скользком или маслянистом полу.	nash-blog	НАШ БЛОГ	НАШ БЛОГ	НАШ БЛОГ	ru_RU		2018-03-31 19:10:19.894+00	2018-06-20 19:56:59.730292+00	<p>Падения вызывают 30% всех регистрируемых несчастных случаев на рабочем месте - эта цифра не может не волновать! Опасность таится в производственных и торговых помещениях, хранилищах, заводских цехах, душевых и кабинах для переодевания, на рампах, лестницах и металлических настилах - везде, где люди ежедневно ведут работу день за днем, неизбежно рискуя поскользнуться на мокром, скользком или маслянистом полу.</p>	1000	t	1	2	1	0	\N	\N
\.


--
-- Data for Name: blog_blog_tags; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.blog_blog_tags (id, blog_id, tag_id) FROM stdin;
1	1	3
2	1	5
3	1	6
4	1	7
\.


--
-- Data for Name: blog_blogimage; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.blog_blogimage (id, image, image_title, image_is_main, image_description, blog_id) FROM stdin;
1	demo/bg1.jpg	НАШ БЛОГ	t		1
\.


--
-- Data for Name: blog_comment; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.blog_comment (id, created, updated, text, ip_address, username, email, is_show, post_id, user_id) FROM stdin;
1	2018-04-11 18:00:51.948+00	2018-04-11 18:01:25.771+00	ewrfsefsdf	127.0.0.1	marychev	marychev@garpix.com	t	2	1
2	2018-04-13 22:53:39.939+00	2018-04-13 22:54:25.406+00	Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend	127.0.0.1	marychev	marychev@garpix.com	t	1	1
3	2018-04-13 22:54:36.555+00	2018-04-13 22:54:36.555+00	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vit	127.0.0.1	marychev	marychev@garpix.com	f	1	1
\.


--
-- Data for Name: blog_post; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.blog_post (id, title, description, slug, seo_title, seo_description, seo_keywords, og_locale, scripts, created, updated, html, sort, is_show, is_allow_comments, comment_count, author_id, blog_id) FROM stdin;
2	Статья 1	Компания «Противоскользящие системы» производит различные противоскользящие ленты, противоскользящие накладки на ступени и противоскользящий профиль. Наша компания в числе первых наладила производство подобных систем и покрытий, призванных обезопасить передвижение людей по скользким поверхностям.	moda-sejchas	МОДА СЕЙЧАС	МОДА СЕЙЧАС	МОДА СЕЙЧАС	ru_RU		2018-03-31 19:22:24.207+00	2018-06-20 20:17:31.82834+00	<p><span style="font-size:small"><span style="font-family:Arial"><span style="color:#000000">Компания &laquo;Противоскользящие системы&raquo; производит различные противоскользящие ленты, противоскользящие накладки на ступени и противоскользящий профиль. Наша компания в числе первых наладила производство подобных систем и покрытий, призванных обезопасить передвижение людей по скользким поверхностям. </span></span></span></p>	1000	t	t	1	1	1
3	Статья 2	Противоскользящий материал обладает особыми свойствами, препятствующими скольжению даже в самых неблагоприятных условиях, таких как чрезмерная влажность, или же излишне скользкая подошва обуви. Как правило, противоскользящие системы изготавливаются из алюминия, резины, термоэластопластов и абразивных материалов.	statya-3	Статья 3	Статья 3	Статья 3	ru_RU		2018-06-20 19:56:03.597032+00	2018-06-20 20:21:04.926915+00	<p><span style="font-size:small"><span style="font-family:Arial"><span style="color:#000000">Противоскользящий материал обладает особыми свойствами, препятствующими скольжению даже в самых неблагоприятных условиях, таких как чрезмерная влажность, или же излишне скользкая подошва обуви. Как правило, противоскользящие системы изготавливаются из алюминия, резины, термоэластопластов и абразивных материалов.</span></span></span></p>	2000	t	t	0	\N	1
1	Статья 3	Противоскользящие системы должны быть использованы на любом покрытии, которое имеет достаточно скользкую поверхность для того чтобы угрожать здоровью прохожих. В наше время противоскользящие ступени являются стандартом, таким же, как противоскользящая лента в бассейн, в сауну. В российских условиях зачастую возникает потребность в установке подобных противоскользящих систем, особенно зимой. Снег, попадающий с улицы, придает обыкновенному покрытию высокий показатель скольжения, что может повлечь за собой многочисленные травмы среди работников и посетителей.	kto-est-kto	КТО ЕСТЬ КТО	КТО ЕСТЬ КТО	КТО ЕСТЬ КТО	ru_RU		2018-03-31 19:19:50.306+00	2018-06-20 20:21:57.135678+00	<p style="margin-left:0cm; margin-right:0cm; text-align:justify"><span style="font-size:small"><span style="font-family:Arial"><span style="color:#000000">Противоскользящие системы должны быть использованы на любом покрытии, которое имеет достаточно скользкую поверхность для того чтобы угрожать здоровью прохожих. В наше время противоскользящие ступени являются стандартом, таким же, как противоскользящая лента в бассейн, в сауну. В российских условиях зачастую возникает потребность в установке подобных противоскользящих систем, особенно зимой. </span></span></span><span style="font-size:small"><span style="font-family:Arial"><span style="color:#000000">Снег, попадающий с улицы, придает обыкновенному покрытию высокий показатель скольжения, что может повлечь за собой многочисленные травмы среди работников и посетителей. </span></span></span></p>	3000	t	t	2	\N	1
\.


--
-- Data for Name: blog_post_tags; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.blog_post_tags (id, post_id, tag_id) FROM stdin;
1	2	3
\.


--
-- Data for Name: blog_postimage; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.blog_postimage (id, image, image_title, image_is_main, image_description, post_id) FROM stdin;
1	demo/promyshlennyj-betonnyj-pol-s-uprochnyonnym-verhnim-sloem.jpeg	МОДА СЕЙЧАС	t		2
2	demo/f1f69a67ac98a8244f188e40b1fc93da.jpg	Статья 2	t		3
3	demo/rulon_perep_01.jpg	Статья 3	t		1
\.


--
-- Data for Name: catalog_catalog; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.catalog_catalog (id, title, description, slug, seo_title, seo_description, seo_keywords, og_locale, scripts, created, updated, html, sort, is_show, lft, rght, tree_id, level, author_id, parent_id) FROM stdin;
7	Каталог Противоскользящих Материалов!	Закладные противоскользящие профили. Устанавливаются под керамическую плитку в момент монтажа лестницы.	katalog-protivoskolzyashih-materialov	Каталог Противоскользящих Материалов!	Каталог Противоскользящих Материалов!	Каталог Противоскользящих Материалов!	ru_RU		2018-06-04 04:52:18.663+00	2018-06-04 04:52:18.664+00		1000	t	1	10	3	0	\N	\N
8	ГРУППА Закладные профили против скольжения	Закладные профили применяются в качестве противоскользящих элементов на углах ступеней лестниц во время установки облицовки. Закладные части фиксируются под керамическую плитку, керамогранит, мрамор или дерево, и крепятся в плиточный раствор или монтажный клей. В большинстве случаев закладные части имеют перфорацию, в которой проникает плиточный раствор и после засыхания удерживает профиль. Это самый надежный способ крепления, пригодный для очень больших нагрузок и тяжелого пешеходного трафика.	alyuminievyj-zakladnoj-profil-uverennyj-shag-c-dvu	Алюминиевый закладной профиль Уверенный Шаг c двумя закладными элементами	Алюминиевый закладной профиль Уверенный Шаг c двумя закладными элементами	Алюминиевый закладной профиль Уверенный Шаг c двумя закладными элементами	ru_RU		2018-06-04 04:55:16.003+00	2018-06-06 19:06:09.742+00	<p><span style="font-family:Arial,Helvetica,sans-serif"><span style="font-size:small">Закладные профили применяются в качестве противоскользящих элементов на углах ступеней лестниц во время установки облицовки.</span>&nbsp;<span style="font-size:small">Закладные части фиксируются под керамическую плитку, керамогранит, мрамор или дерево, и крепятся в плиточный раствор или монтажный клей. В большинстве случаев закладные части имеют перфорацию, в которой проникает плиточный раствор и после засыхания удерживает профиль. Это самый надежный способ крепления, пригодный для очень больших нагрузок и тяжелого пешеходного трафика.</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:Arial,Helvetica,sans-serif"><span style="font-size:12px"><span style="font-size:small">Закладные профили бывают монолитные (<strong>Безопасный Шаг</strong>,&nbsp;<strong>SureStep</strong>), и с жестким каркасом и гибкой вставкой против скольжения&nbsp; (<strong>Уверенный Шаг</strong>,&nbsp;<strong>Safestep</strong>,&nbsp;<strong>ALPB</strong>,&nbsp;<strong>Не Скользко</strong>).&nbsp;</span></span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:Arial,Helvetica,sans-serif"><span style="font-size:12px"><span style="font-size:small">В случае монолитных профилей закладные элементы и противоскользящая часть создают одно целое и выполнены из гибкой резины или термоэластопласта. Их преимущество - относительная дешевизна и возможность применения в случаях круглых ступеней с радиусом.&nbsp;Рекомендуются для среднего по нагрузки потока пешеходов.</span></span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:Arial,Helvetica,sans-serif"><span style="font-size:12px"><span style="font-size:small">Профили с жестким каркасом состоят из противоскользящей части и закладным каркасом из алюминия или жесткого пластика. Гибкая часть против скольжения вставляется в пазы каркаса и надежно удерживается на месте. Ее можно менять по мере износа. Этот тип профилей менее требователен в установке и надежнее в условиях больших нагрузок и частых зимних циклов заморозки/разморозки.&nbsp;</span></span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:Arial,Helvetica,sans-serif"><span style="font-size:12px"><span style="font-size:small">Гибкая часть свободно деформируется при прохождении пешеходов и ломает наледь. В то же время жесткий алюминиевый каркас изолирует саму ступень от попадания воды между гибкой части и облицовки. В противном случае попавшая вода замерзает в холодное время года и увеличившись в объеме разрушает облицовку.&nbsp;</span></span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:Arial,Helvetica,sans-serif"><span style="font-size:12px"><span style="font-size:small">Каркасные профили с двумя перфорированными закладными элементами рекомендуются для больших и очень больших нагрузок. С одним перфорированным закладным элементом - для средних нагрузок.</span></span></span></p>\r\n\r\n<p><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:small"><span style="font-family:Arial"><span style="color:#0000ff"><strong>Накладные алюминиевые профили</strong></span>&nbsp;против скольжения предназначены создания зон безопасности на поверхностях&nbsp;<u><strong>уже отделанных</strong></u>&nbsp;ступеней и площадок. В зависимости от места установки они могут предлагаться в виде полос или Г-образных порожков. Полосы монтируются на любой поверхности. Порожки устанавливаются на углах ступеней лестниц. Помимо защиты от подскальзываний угловые пороги еще защищают ступеней от механических повреждений.</span></span></span></span></span></span></span></span></p>\r\n\r\n<p><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:small"><span style="font-family:Arial">В случаях когда&nbsp;<strong><u>поверхность еще не отделана</u></strong>&nbsp;рекомендуем рассмотреть&nbsp;<span style="color:#0000ff"><strong>закладные профили против скольжения</strong></span>. По степени надежности крепления и пригодности к очень большим нагрузкам эти профили превосходят всех других разновидностей.</span></span></span></span></span></span></span></span></p>\r\n\r\n<p><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:small"><span style="font-family:Arial">Накладные алюминиевые профили крепятся с помощью с винтов и дюбелей. Это самый надежный способ крепления, пригодный для очень больших нагрузок и тяжелого пешеходного трафика.&nbsp;</span></span></span></span></span></span></span></span></p>\r\n\r\n<p><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:small"><span style="font-family:Arial">Когда сверление облицовки поверхностей нежелательно используются&nbsp;<span style="color:#0000ff"><strong>самоклеющиеся накладные алюминиевые профили</strong></span>. За счет применяемых клеев и жесткости алюминиевой части такой способ крепления достаточно надежен, хотя уступает креплению с помощью винтов и дюбелей.</span></span></span></span></span></span></span></span></p>\r\n\r\n<p><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:small"><span style="font-family:Arial">В случаях когда ступени округлые или желательно чтобы противоскользящие профили имели меньшей толщины, применяются&nbsp;<span style="color:#0000ff"><strong>самоклеющиеся полосы и углы из резины</strong></span>, или самоклеющиеся&nbsp;<strong><span style="color:#0000ff">абразивные</span></strong>&nbsp;и&nbsp;<span style="color:#0000ff"><strong>виниловые ленты</strong></span>. Резиновые профили толще и применяются когда возможно накопление снега и грязи. Абразивные и виниловые ленты имеют толщину около 1 мм и применяются когда необходимо полностью избегать возможность спотыкания или когда возможно хождение без обуви.</span></span></span></span></span></span></span></span></p>\r\n\r\n<p><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:small"><span style="font-family:Arial">За счет минимальной толщины абразивные и виниловые ленты крепятся достаточно надежно и при правильной установке могут применяться при средних и больших нагрузках.&nbsp;</span></span></span></span></span></span></span></span></p>\r\n\r\n<p><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:small"><span style="font-family:Arial">По степени надежности крепления самоклеющиеся резиновые профили уступают жестким алюминиевым профилям и рекомендуются при небольшом пешеходном потоке - до 300 проходов в сутки.</span></span></span></span></span></span></span></span></p>	1000	t	2	7	3	1	\N	7
9	Алюминиевый закладной профиль SafeStep c двумя закладными элементами	Предназначен для установки на углах ступеней лестниц в качестве противоскользящего профиля безопасности и как декоративная отделка. Устанавливается при монтаже ваших ступеней. Наиболее надёжен и долговечен.	alyuminievyj-zakladnoj-profil-safestep-c-dvumya-za	Алюминиевый закладной профиль SafeStep c двумя закладными элементами	Алюминиевый закладной профиль SafeStep c двумя закладными элементами	Алюминиевый закладной профиль SafeStep c двумя закладными элементами	ru_RU		2018-06-04 05:00:49.694+00	2018-06-04 05:00:49.694+00	<table border="1" class="duduk1" id="table447">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>Длина: 1,2 м и 2,4 м</td>\r\n\t\t\t<td>Размер горизонтальной/вертикальной части: 43 мм/17 мм</td>\r\n\t\t\t<td>Срок службы: 10 лет</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td>Материал: Алюминий и ТЭП</td>\r\n\t\t\t<td>Температурный режим: -40С до +40С</td>\r\n\t\t\t<td>Фасовка: 10 шт./уп.</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>	1000	t	8	9	3	1	\N	7
11	Уверенный Шаг 50 мм	Алюминиевый профиль \r\nс 2 закладными элементами\r\nКрепление:\tПлиточный клей\r\nКаркас:\tАлюминий\r\nВставка:\tМягкий эластопласт\r\nДлина:\t1,2м; 2,4м\r\nНагрузка:\t2000 шаг/сутки\r\n \r\nот 434 руб/м	profil-ush-50-mm	Профиль УШ-50 мм	Профиль УШ-50 мм	Профиль УШ-50 мм	ru_RU		2018-06-04 21:07:41.891+00	2018-06-06 05:40:57.886+00		1000	f	3	4	3	2	\N	8
12	Safestep 43 мм	Алюминиевый профиль\r\nc 2 закладными элементами\r\nКрепление:\tПлиточный клей\r\nКаркас:\tАлюминий\r\nВставка:\tМягкий эластопласт\r\nДлина:\t1,2м; 2,4м\r\nНагрузка:\t1250 шаг/сутки\r\n \r\nот 238 руб/м	safestep-43-mm	Safestep 43 мм	Safestep 43 мм	Safestep 43 мм	ru_RU		2018-06-04 21:19:31.677+00	2018-06-06 05:40:58.072+00	<p>Алюминиевый профиль<br />\r\nc 2 закладными элементами<br />\r\nКрепление:&nbsp;&nbsp; &nbsp;Плиточный клей<br />\r\nКаркас:&nbsp;&nbsp; &nbsp;Алюминий<br />\r\nВставка:&nbsp;&nbsp; &nbsp;Мягкий эластопласт<br />\r\nДлина:&nbsp;&nbsp; &nbsp;1,2м; 2,4м<br />\r\nНагрузка:&nbsp;&nbsp; &nbsp;1250 шаг/сутки<br />\r\n&nbsp;<br />\r\nот 238 руб/м</p>	1000	f	5	6	3	2	\N	8
\.


--
-- Data for Name: catalog_catalog_tags; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.catalog_catalog_tags (id, catalog_id, tag_id) FROM stdin;
\.


--
-- Data for Name: catalog_catalogimage; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.catalog_catalogimage (id, image, image_title, image_is_main, image_description, catalog_id) FROM stdin;
2	catalog1/ush-50_black_600x400.jpg	Алюминиевый закладной профиль Уверенный Шаг c двумя закладными элементами	t		8
3	catalog1/safestep_brown_600x400.jpg	Алюминиевый закладной профиль SafeStep c двумя закладными элементами	t		9
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2018-06-18 19:48:25.567204+00	1	nepaday.itbin.ru	2	[{"changed": {"fields": ["domain", "name"]}}]	3	1
2	2018-06-18 19:49:11.953002+00	1	Слайдер 2	2	[{"changed": {"fields": ["image"]}}]	9	1
3	2018-06-18 19:49:27.812784+00	1	Слайдер 2	2	[{"changed": {"fields": ["image"]}}]	9	1
4	2018-06-18 19:51:03.31803+00	1	Слайдер 2	2	[{"changed": {"fields": ["image"]}}]	9	1
5	2018-06-18 19:55:23.833127+00	1	Компания "Противоскользящие Системы"	2	[{"changed": {"fields": ["title", "logo", "email", "phone", "address"]}}]	40	1
6	2018-06-18 19:56:17.717854+00	1	Компания "Противоскользящие Системы"	2	[{"changed": {"fields": ["slug", "seo_title", "seo_description", "seo_keywords"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e", "object": "\\u041a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f \\"\\u041f\\u0440\\u043e\\u0442\\u0438\\u0432\\u043e\\u0441\\u043a\\u043e\\u043b\\u044c\\u0437\\u044f\\u0449\\u0438\\u0435 \\u0421\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b\\"", "fields": ["image"]}}]	29	1
7	2018-06-18 19:58:28.239258+00	1	Телефоны для связи	2	[{"changed": {"fields": ["title", "html"]}}]	38	1
8	2018-06-18 19:59:17.763786+00	1	1	2	[{"changed": {"fields": ["title", "image", "description"]}}]	9	1
9	2018-06-18 19:59:32.732184+00	1	---	2	[{"changed": {"fields": ["title", "image"]}}]	9	1
10	2018-06-18 20:01:33.283027+00	1	Компания "Противоскользящие Системы"	2	[{"deleted": {"name": "\\u0424\\u043e\\u0442\\u043e", "object": "\\u041a\\u043e\\u043c\\u043f\\u0430\\u043d\\u0438\\u044f \\"\\u041f\\u0440\\u043e\\u0442\\u0438\\u0432\\u043e\\u0441\\u043a\\u043e\\u043b\\u044c\\u0437\\u044f\\u0449\\u0438\\u0435 \\u0421\\u0438\\u0441\\u0442\\u0435\\u043c\\u044b\\""}}]	29	1
11	2018-06-18 20:24:43.416427+00	None	Одежда	3		31	1
12	2018-06-18 20:24:43.423853+00	None	Платья	3		31	1
13	2018-06-18 20:24:43.42914+00	None	Джинс	3		31	1
14	2018-06-18 20:24:43.435185+00	None	Обувь	3		31	1
15	2018-06-18 20:24:43.440738+00	None	Кеды	3		31	1
16	2018-06-18 20:24:43.446283+00	None	Ботинки	3		31	1
17	2018-06-18 20:26:36.393901+00	13	Фотолюминесцентные светонакопительные ленты	2	[{"changed": {"fields": ["sort"]}}]	31	1
18	2018-06-18 20:26:36.408896+00	14	Закладные профили из алюминия и резины	2	[{"changed": {"fields": ["sort"]}}]	31	1
19	2018-06-18 20:26:36.417917+00	15	Алюминиевый угол УШ-50	2	[{"changed": {"fields": ["sort"]}}]	31	1
20	2018-06-18 20:26:36.428493+00	16	Алюминиевый угол SafeStep	2	[{"changed": {"fields": ["sort"]}}]	31	1
21	2018-06-18 20:26:36.439314+00	18	ПВХ полоса Не Скользко	2	[{"changed": {"fields": ["sort"]}}]	31	1
22	2018-06-18 20:28:19.326388+00	None	Одежда	3		17	1
23	2018-06-18 20:28:19.336483+00	None	Платья	3		17	1
24	2018-06-18 20:28:19.345278+00	None	Джинсы	3		17	1
25	2018-06-18 20:28:19.362585+00	None	Обувь	3		17	1
26	2018-06-18 20:28:19.372026+00	None	Кеды	3		17	1
27	2018-06-18 20:28:19.381724+00	None	Ботинки	3		17	1
28	2018-06-18 20:28:47.908421+00	2	КАТАЛОГ ПРОДАЖ	2	[{"changed": {"fields": ["catalog"]}}]	31	1
29	2018-06-18 20:32:07.102548+00	17	Самоклеющиеся профили из эластопластов и резины	2	[{"changed": {"fields": ["catalog"]}}]	31	1
30	2018-06-18 20:33:18.590085+00	15	Алюминиевый угол УШ-50	2	[{"changed": {"fields": ["catalog"]}}]	31	1
31	2018-06-18 20:35:07.635539+00	6	Уверенный Шаг 50 мм	2	[{"changed": {"fields": ["catalog"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0423\\u0432\\u0435\\u0440\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0428\\u0430\\u0433 50 \\u043c\\u043c", "fields": ["image"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0423\\u0432\\u0435\\u0440\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0428\\u0430\\u0433 50 \\u043c\\u043c", "fields": ["image"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0423\\u0432\\u0435\\u0440\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0428\\u0430\\u0433 50 \\u043c\\u043c", "fields": ["image"]}}]	19	1
32	2018-06-18 20:35:29.43417+00	12	с вставкой дополнительных цветов - желтый, синий, голубой, белый, красный, зеленый	2	[{"changed": {"fields": ["catalog"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0441 \\u0432\\u0441\\u0442\\u0430\\u0432\\u043a\\u043e\\u0439 \\u0434\\u043e\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0446\\u0432\\u0435\\u0442\\u043e\\u0432 - \\u0436\\u0435\\u043b\\u0442\\u044b\\u0439, \\u0441\\u0438\\u043d\\u0438\\u0439, \\u0433\\u043e\\u043b\\u0443\\u0431\\u043e\\u0439, \\u0431\\u0435\\u043b\\u044b\\u0439, \\u043a\\u0440\\u0430\\u0441\\u043d\\u044b\\u0439, \\u0437\\u0435\\u043b\\u0435\\u043d\\u044b\\u0439", "fields": ["image"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0441 \\u0432\\u0441\\u0442\\u0430\\u0432\\u043a\\u043e\\u0439 \\u0434\\u043e\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0446\\u0432\\u0435\\u0442\\u043e\\u0432 - \\u0436\\u0435\\u043b\\u0442\\u044b\\u0439, \\u0441\\u0438\\u043d\\u0438\\u0439, \\u0433\\u043e\\u043b\\u0443\\u0431\\u043e\\u0439, \\u0431\\u0435\\u043b\\u044b\\u0439, \\u043a\\u0440\\u0430\\u0441\\u043d\\u044b\\u0439, \\u0437\\u0435\\u043b\\u0435\\u043d\\u044b\\u0439", "fields": ["image"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0441 \\u0432\\u0441\\u0442\\u0430\\u0432\\u043a\\u043e\\u0439 \\u0434\\u043e\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0446\\u0432\\u0435\\u0442\\u043e\\u0432 - \\u0436\\u0435\\u043b\\u0442\\u044b\\u0439, \\u0441\\u0438\\u043d\\u0438\\u0439, \\u0433\\u043e\\u043b\\u0443\\u0431\\u043e\\u0439, \\u0431\\u0435\\u043b\\u044b\\u0439, \\u043a\\u0440\\u0430\\u0441\\u043d\\u044b\\u0439, \\u0437\\u0435\\u043b\\u0435\\u043d\\u044b\\u0439", "fields": ["image"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0441 \\u0432\\u0441\\u0442\\u0430\\u0432\\u043a\\u043e\\u0439 \\u0434\\u043e\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0446\\u0432\\u0435\\u0442\\u043e\\u0432 - \\u0436\\u0435\\u043b\\u0442\\u044b\\u0439, \\u0441\\u0438\\u043d\\u0438\\u0439, \\u0433\\u043e\\u043b\\u0443\\u0431\\u043e\\u0439, \\u0431\\u0435\\u043b\\u044b\\u0439, \\u043a\\u0440\\u0430\\u0441\\u043d\\u044b\\u0439, \\u0437\\u0435\\u043b\\u0435\\u043d\\u044b\\u0439", "fields": ["image"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0441 \\u0432\\u0441\\u0442\\u0430\\u0432\\u043a\\u043e\\u0439 \\u0434\\u043e\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0446\\u0432\\u0435\\u0442\\u043e\\u0432 - \\u0436\\u0435\\u043b\\u0442\\u044b\\u0439, \\u0441\\u0438\\u043d\\u0438\\u0439, \\u0433\\u043e\\u043b\\u0443\\u0431\\u043e\\u0439, \\u0431\\u0435\\u043b\\u044b\\u0439, \\u043a\\u0440\\u0430\\u0441\\u043d\\u044b\\u0439, \\u0437\\u0435\\u043b\\u0435\\u043d\\u044b\\u0439", "fields": ["image"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0441 \\u0432\\u0441\\u0442\\u0430\\u0432\\u043a\\u043e\\u0439 \\u0434\\u043e\\u043f\\u043e\\u043b\\u043d\\u0438\\u0442\\u0435\\u043b\\u044c\\u043d\\u044b\\u0445 \\u0446\\u0432\\u0435\\u0442\\u043e\\u0432 - \\u0436\\u0435\\u043b\\u0442\\u044b\\u0439, \\u0441\\u0438\\u043d\\u0438\\u0439, \\u0433\\u043e\\u043b\\u0443\\u0431\\u043e\\u0439, \\u0431\\u0435\\u043b\\u044b\\u0439, \\u043a\\u0440\\u0430\\u0441\\u043d\\u044b\\u0439, \\u0437\\u0435\\u043b\\u0435\\u043d\\u044b\\u0439", "fields": ["image"]}}]	19	1
33	2018-06-19 08:58:34.920284+00	1	Слайдер 1	2	[{"changed": {"fields": ["title", "image"]}}]	9	1
34	2018-06-19 08:59:09.597888+00	2	Слайдер 2	2	[{"changed": {"fields": ["title", "image", "description", "url"]}}]	9	1
35	2018-06-19 09:00:10.396174+00	11	БЛОГ	2	[{"changed": {"fields": ["is_show"]}}]	31	1
36	2018-06-19 09:00:31.870213+00	1	НАШ БЛОГ	2	[{"changed": {"fields": ["is_show"]}}]	10	1
37	2018-06-19 09:01:53.099431+00	2	МОДА СЕЙЧАС	2	[{"changed": {"fields": ["is_show"]}}]	13	1
38	2018-06-19 09:01:53.104574+00	1	КТО ЕСТЬ КТО	2	[{"changed": {"fields": ["is_show"]}}]	13	1
39	2018-06-19 09:02:08.080314+00	2	МОДА СЕЙЧАС	2	[{"changed": {"fields": ["is_show"]}}]	13	1
40	2018-06-19 09:02:08.085918+00	1	КТО ЕСТЬ КТО	2	[{"changed": {"fields": ["is_show"]}}]	13	1
41	2018-06-19 17:23:25.697852+00	3	null	1	[{"added": {}}]	32	1
42	2018-06-19 17:23:55.200712+00	19	Партнёры и Дилеры	2	[{"changed": {"fields": ["page"]}}]	31	1
43	2018-06-19 17:24:15.604693+00	20	Для Дилеров	2	[{"changed": {"fields": ["page"]}}]	31	1
44	2018-06-19 17:24:34.707964+00	3	Пусто	2	[{"changed": {"fields": ["title"]}}]	32	1
45	2018-06-19 17:25:05.151542+00	23	Статьи	2	[{"changed": {"fields": ["page"]}}]	31	1
46	2018-06-19 20:58:15.313622+00	1	Компания "Противоскользящие Системы"	2	[{"changed": {"fields": ["logo", "email"]}}]	40	1
47	2018-06-19 21:35:25.935303+00	1	Nepaday.ru	2	[{"changed": {"fields": ["title", "logo"]}}]	40	1
48	2018-06-19 21:48:48.251356+00	1	nepaday.itbin.ru	2	[{"changed": {"fields": ["name"]}}]	3	1
49	2018-06-19 21:49:08.901256+00	1	nepaday.itbin.ru	2	[{"changed": {"fields": ["name"]}}]	3	1
50	2018-06-19 21:53:54.957285+00	2	Слайдер 2	2	[{"changed": {"fields": ["image"]}}]	9	1
51	2018-06-19 21:56:59.332919+00	1	Слайдер 1	2	[{"changed": {"fields": ["image"]}}]	9	1
52	2018-06-19 22:00:49.836695+00	2	Слайдер 2	2	[{"changed": {"fields": ["image"]}}]	9	1
53	2018-06-19 22:09:03.885913+00	2	Слайдер 2	2	[{"changed": {"fields": ["image"]}}]	9	1
54	2018-06-19 22:14:34.686197+00	2	Слайдер 2	2	[{"changed": {"fields": ["image"]}}]	9	1
55	2018-06-19 22:17:32.41191+00	2	Слайдер 2	2	[{"changed": {"fields": ["image"]}}]	9	1
56	2018-06-19 22:25:49.918515+00	2	Слайдер 2	2	[{"changed": {"fields": ["image"]}}]	9	1
57	2018-06-19 22:26:04.790851+00	2	Слайдер 2	2	[{"changed": {"fields": ["image"]}}]	9	1
58	2018-06-19 22:56:37.380005+00	3	ГАРАНТИРОВАННОЕ УДОВЛЕТВОРЕНИЕ	2	[{"changed": {"fields": ["image", "url"]}}]	41	1
59	2018-06-19 22:57:06.817179+00	3	ГАРАНТИРОВАННОЕ УДОВЛЕТВОРЕНИЕ	2	[{"changed": {"fields": ["image", "url"]}}]	41	1
60	2018-06-19 23:14:41.38397+00	1	НАШ БЛОГ	2	[{"changed": {"fields": ["description", "html"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u041d\\u0410\\u0428 \\u0411\\u041b\\u041e\\u0413", "fields": ["image"]}}]	10	1
61	2018-06-19 23:20:33.415805+00	2	test	2	[{"changed": {"fields": ["image", "url"]}}]	36	1
62	2018-06-19 23:21:48.02541+00	1	НАШ БЛОГ	2	[{"changed": {"fields": ["is_show"]}}]	10	1
63	2018-06-19 23:24:03.596939+00	3	Пусто	2	[{"changed": {"fields": ["seo_title"]}}]	32	1
64	2018-06-19 23:24:40.68851+00	2	Контакты	2	[{"changed": {"fields": ["title", "slug", "seo_title", "seo_description", "seo_keywords"]}}]	32	1
65	2018-06-19 23:24:55.974358+00	2	Контакты	2	[{"changed": {"fields": ["slug"]}}]	32	1
66	2018-06-19 23:26:09.64266+00	2	Контакты	2	[{"changed": {"fields": ["html"]}}]	32	1
67	2018-06-19 23:26:49.312366+00	2	Контакты	2	[{"changed": {"fields": ["html"]}}]	32	1
68	2018-06-19 23:27:59.386133+00	2	Контакты	2	[{"changed": {"fields": ["html"]}}]	32	1
69	2018-06-19 23:29:42.548042+00	2	Контакты	2	[{"changed": {"fields": ["html", "scripts"]}}]	32	1
70	2018-06-20 18:48:04.60506+00	2	Слайдер 2	2	[{"changed": {"fields": ["image"]}}]	9	1
71	2018-06-20 18:50:15.718589+00	2	Слайдер 2	2	[{"changed": {"fields": ["image"]}}]	9	1
72	2018-06-20 18:53:32.692083+00	1	КТО ЕСТЬ КТО	2	[{"changed": {"fields": ["description", "html"]}}]	13	1
73	2018-06-20 19:01:40.307504+00	3	Слайд 3	1	[{"added": {}}]	9	1
74	2018-06-20 19:01:48.689375+00	3	Слайд 3	2	[{"changed": {"fields": ["sort"]}}]	9	1
75	2018-06-20 19:01:50.916722+00	3	Слайд 3	2	[{"changed": {"fields": ["sort"]}}]	9	1
76	2018-06-20 19:01:59.153876+00	3	Слайд 3	2	[{"changed": {"fields": ["description"]}}]	9	1
77	2018-06-20 19:04:54.637967+00	3	Слайд 3	2	[{"changed": {"fields": ["image"]}}]	9	1
78	2018-06-20 19:12:20.400248+00	3	Слайдер 3	2	[{"changed": {"fields": ["title", "image"]}}]	9	1
79	2018-06-20 19:51:32.049346+00	2	Слайдер 2	2	[{"changed": {"fields": ["image"]}}]	9	1
80	2018-06-20 19:54:42.458838+00	2	МОДА СЕЙЧАС	2	[{"changed": {"fields": ["description", "html"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "PostImage object", "fields": ["image"]}}]	13	1
81	2018-06-20 19:54:57.144363+00	2	Статья 1	2	[{"changed": {"fields": ["title"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "PostImage object", "fields": ["image"]}}]	13	1
82	2018-06-20 19:55:25.604877+00	1	Статья 2	2	[{"changed": {"fields": ["title", "description", "html"]}}]	13	1
83	2018-06-20 19:56:03.602135+00	3	Статья 3	1	[{"added": {}}]	13	1
84	2018-06-20 19:56:28.226573+00	3	Статья 2	2	[{"changed": {"fields": ["title"]}}]	13	1
85	2018-06-20 19:56:33.687742+00	1	Статья 3	2	[{"changed": {"fields": ["title"]}}]	13	1
86	2018-06-20 19:56:37.830847+00	3	Статья 2	2	[{"changed": {"fields": ["sort"]}}]	13	1
87	2018-06-20 19:56:37.835447+00	1	Статья 3	2	[{"changed": {"fields": ["sort"]}}]	13	1
88	2018-06-20 19:56:59.736946+00	1	СТАТЬИ	2	[{"changed": {"fields": ["title"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0421\\u0422\\u0410\\u0422\\u042c\\u0418", "fields": ["image"]}}]	10	1
89	2018-06-20 20:00:26.022216+00	2	ЛУЧШИЕ ЦЕНЫ	2	[{"changed": {"fields": ["image"]}}]	41	1
90	2018-06-20 20:02:31.630429+00	1	БЫСТРАЯ ДОСТАВКА	2	[{"changed": {"fields": ["image"]}}]	41	1
91	2018-06-20 20:07:17.003653+00	1	БЫСТРАЯ ДОСТАВКА	2	[{"changed": {"fields": ["image"]}}]	41	1
92	2018-06-20 20:17:03.838466+00	2	Статья 1	2	[{"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "PostImage object", "fields": ["image"]}}]	13	1
93	2018-06-20 20:17:31.833564+00	2	Статья 1	2	[{"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "PostImage object", "fields": ["image"]}}]	13	1
94	2018-06-20 20:21:04.932456+00	3	Статья 2	2	[{"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "PostImage object"}}]	13	1
95	2018-06-20 20:21:57.143345+00	1	Статья 3	2	[{"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "PostImage object"}}]	13	1
96	2018-06-20 20:32:00.633584+00	2	ЛУЧШИЕ ЦЕНЫ	2	[{"changed": {"fields": ["image"]}}]	41	1
97	2018-06-20 20:34:08.116494+00	3	ОТЛИЧНОЕ КАЧЕСТВО	2	[{"changed": {"fields": ["title", "image"]}}]	41	1
98	2018-06-20 20:35:11.780043+00	3	ОТЛИЧНОЕ КАЧЕСТВО	2	[{"changed": {"fields": ["image"]}}]	41	1
99	2018-06-20 20:38:19.77722+00	2	ЛУЧШИЕ ЦЕНЫ	2	[{"changed": {"fields": ["image"]}}]	41	1
100	2018-06-20 20:39:29.99412+00	3	ОТЛИЧНОЕ КАЧЕСТВО	2	[{"changed": {"fields": ["image", "description"]}}]	41	1
101	2018-06-20 20:41:49.973339+00	2	ЛУЧШИЕ ЦЕНЫ	2	[{"changed": {"fields": ["image", "description"]}}]	41	1
102	2018-06-20 20:42:08.843529+00	2	ЛУЧШИЕ ЦЕНЫ	2	[{"changed": {"fields": ["image", "description"]}}]	41	1
103	2018-06-20 20:42:52.719554+00	1	БЫСТРАЯ ДОСТАВКА	2	[{"changed": {"fields": ["image", "description"]}}]	41	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	filebrowser	filebrowser
2	admin	logentry
3	sites	site
4	auth	permission
5	auth	group
6	auth	user
7	contenttypes	contenttype
8	sessions	session
9	advertising	sliderhome
10	blog	blog
11	blog	blogimage
12	blog	comment
13	blog	post
14	blog	postimage
15	gallery	gallery
16	gallery	galleryimage
17	catalog	catalog
18	catalog	catalogimage
19	product	product
20	product	productcomment
21	product	productimage
22	product	productitem
23	order	order
24	order	orderitem
25	order	status
26	order	story
27	users	userlink
28	users	userprofile
29	home	home
30	home	homeimage
31	menu	mainmenu
32	page	page
33	page	pagecomment
34	page	pageimage
35	site_info	listlink
36	site_info	socialnetwork
37	site_info	tag
38	site_info	textinfo
39	settings_template	footer
40	settings_template	settingstemplate
41	include_area	includearea
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-06-18 19:38:02.404715+00
2	auth	0001_initial	2018-06-18 19:38:02.494486+00
3	admin	0001_initial	2018-06-18 19:38:02.528944+00
4	admin	0002_logentry_remove_auto_add	2018-06-18 19:38:02.555651+00
5	advertising	0001_initial	2018-06-18 19:38:02.572765+00
6	advertising	0002_auto_20180422_1015	2018-06-18 19:38:02.584687+00
7	contenttypes	0002_remove_content_type_name	2018-06-18 19:38:02.631589+00
8	auth	0002_alter_permission_name_max_length	2018-06-18 19:38:02.644616+00
9	auth	0003_alter_user_email_max_length	2018-06-18 19:38:02.66852+00
10	auth	0004_alter_user_username_opts	2018-06-18 19:38:02.682717+00
11	auth	0005_alter_user_last_login_null	2018-06-18 19:38:02.700214+00
12	auth	0006_require_contenttypes_0002	2018-06-18 19:38:02.703494+00
13	auth	0007_alter_validators_add_error_messages	2018-06-18 19:38:02.719705+00
14	auth	0008_alter_user_username_max_length	2018-06-18 19:38:02.744892+00
15	site_info	0001_initial	2018-06-18 19:38:02.80038+00
16	blog	0001_initial	2018-06-18 19:38:03.077545+00
17	catalog	0001_initial	2018-06-18 19:38:03.224542+00
18	gallery	0001_initial	2018-06-18 19:38:03.43202+00
19	home	0001_initial	2018-06-18 19:38:03.533999+00
20	include_area	0001_initial	2018-06-18 19:38:03.563655+00
21	include_area	0002_includearea_url	2018-06-18 19:38:03.577941+00
22	page	0001_initial	2018-06-18 19:38:03.821645+00
23	menu	0001_initial	2018-06-18 19:38:03.965357+00
24	product	0001_initial	2018-06-18 19:38:04.33606+00
25	order	0001_initial	2018-06-18 19:38:04.559186+00
26	product	0002_auto_20180604_2124	2018-06-18 19:38:04.75084+00
27	product	0003_auto_20180605_1915	2018-06-18 19:38:05.227889+00
28	product	0004_auto_20180605_1931	2018-06-18 19:38:05.323969+00
29	product	0005_auto_20180605_1959	2018-06-18 19:38:05.381651+00
30	product	0006_auto_20180605_2014	2018-06-18 19:38:05.4041+00
31	product	0007_auto_20180605_2015	2018-06-18 19:38:05.432782+00
32	product	0008_auto_20180606_1810	2018-06-18 19:38:05.457956+00
33	sessions	0001_initial	2018-06-18 19:38:05.478132+00
34	sites	0001_initial	2018-06-18 19:38:05.491413+00
35	sites	0002_alter_domain_unique	2018-06-18 19:38:05.508713+00
36	settings_template	0001_initial	2018-06-18 19:38:05.734497+00
37	settings_template	0002_auto_20180422_1015	2018-06-18 19:38:05.775902+00
38	settings_template	0003_auto_20180430_0000	2018-06-18 19:38:05.8369+00
39	settings_template	0004_settingstemplate_meta	2018-06-18 19:38:05.888618+00
40	settings_template	0005_auto_20180430_1647	2018-06-18 19:38:06.090083+00
41	settings_template	0006_auto_20180430_1653	2018-06-18 19:38:06.137571+00
42	users	0001_initial	2018-06-18 19:38:06.309577+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
mtlvjw3jprhmwe087o8wg7xafl9r50n0	YTZhMzIyMzdhNWEzMjkwNTM5ZGQ2YmVlMDk3YmYzMjhhZDc2N2ZiMDqABJXwAAAAAAAAAH2UKIwNX2F1dGhfdXNlcl9pZJSMATGUjBJfYXV0aF91c2VyX2JhY2tlbmSUjClkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZJSMD19hdXRoX3VzZXJfaGFzaJSMKDMzMDMyNGFjMDZkZDU1Y2RmOTJlNzc4YWRhYmUyMzczNDUzOGExZGaUjARjYXJ0lH2UKIwCMjiUfZQojAhxdWFudGl0eZRLBIwFcHJpY2WUjAY1ODAuMDCUdYwCMjOUfZQojAhxdWFudGl0eZRLAYwFcHJpY2WUjAY1MjAuMDCUdXV1Lg==	2018-07-02 20:38:13.308978+00
pgevz0an46l7rfiez74wqanpra8jx1bg	ODk0MWM1YzFlOTdkODI2ZGE2NDAxMDVkMGE0OTA3ZTRhYzk5OGViZDqABJWXAAAAAAAAAH2UKIwNX2F1dGhfdXNlcl9pZJSMATGUjBJfYXV0aF91c2VyX2JhY2tlbmSUjClkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZJSMD19hdXRoX3VzZXJfaGFzaJSMKDMzMDMyNGFjMDZkZDU1Y2RmOTJlNzc4YWRhYmUyMzczNDUzOGExZGaUdS4=	2018-07-03 08:55:13.05615+00
lainqjdjvlatqfjeglz9n97v7ihx7szx	ZjU5ZDZmMzI0YjYzZGViZjU4NzM0YjYxNGE2MzRiMjU1ZTY4NTAxYzqABJU1AAAAAAAAAH2UjARjYXJ0lH2UjAIyOJR9lCiMCHF1YW50aXR5lEsBjAVwcmljZZSMBjU4MC4wMJR1c3Mu	2018-07-03 17:04:06.286559+00
5iqdjto3ci2wqzzhmy2xp1rsh9wvby8m	ODk0MWM1YzFlOTdkODI2ZGE2NDAxMDVkMGE0OTA3ZTRhYzk5OGViZDqABJWXAAAAAAAAAH2UKIwNX2F1dGhfdXNlcl9pZJSMATGUjBJfYXV0aF91c2VyX2JhY2tlbmSUjClkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZJSMD19hdXRoX3VzZXJfaGFzaJSMKDMzMDMyNGFjMDZkZDU1Y2RmOTJlNzc4YWRhYmUyMzczNDUzOGExZGaUdS4=	2018-07-03 17:19:48.548861+00
fxz9n8s7lpchsauihmxoym7qkmwz4j60	M2ZmMjczZmMwZTgyZDUyMzc0YjI4M2QzZjIzMWIxZjQzMDhlMmY5ZjqABJU2AAAAAAAAAH2UjARjYXJ0lH2UjAIxOZR9lCiMCHF1YW50aXR5lEtEjAVwcmljZZSMBzExNzAuMDCUdXNzLg==	2018-07-06 09:45:43.431425+00
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.django_site (id, domain, name) FROM stdin;
1	nepaday.itbin.ru	Nepaday.ru
\.


--
-- Data for Name: gallery_gallery; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.gallery_gallery (id, title, description, slug, seo_title, seo_description, seo_keywords, og_locale, scripts, created, updated, html, sort, is_show, lft, rght, tree_id, level, author_id, parent_id) FROM stdin;
1	The Best of the Beast	\N	best-beast	The Best of the Beast	The Best of the Beast	The Best of the Beast	ru_RU		2018-04-16 19:09:57.146+00	2018-04-16 19:51:03.316+00	<p>The Best of the Beast&nbsp;The Best of the Be astThe Best of the Be astThe Best of the BeastThe Best of the Beast.</p>\r\n\r\n<p>The Best of the Beast&nbsp;The Best of the Be astThe Best of the Be astThe Best of the BeastThe Best of the BeastThe Best of the Beast&nbsp;The Best of the Be astThe Best of the Be astThe Best of the BeastThe Best of the BeastThe Best of the Beast&nbsp;The Best of the Be astThe Best of the Be astThe Best of the BeastThe Best of the BeastThe Best of the Beast&nbsp;The Best of the Be astThe Best of the Be astThe Best of the BeastThe Best of the BeastThe Best of the Beast&nbsp;The Best of the Be astThe Best of the Be astThe Best of the BeastThe Best of the BeastThe Best of the Beast&nbsp;The Best of the Be astThe Best of the Be astThe Best of the BeastThe Best of the Beast</p>\r\n\r\n<p>The Best of the Beast&nbsp;The Best of the Be astThe Best of the Be astThe Best of the BeastThe Best of the BeastThe Best of the Beast&nbsp;The Best of the Be astThe Best of the Be astThe Best of the BeastThe Best of the Beast</p>\r\n\r\n<p>The Best of the Beast&nbsp;The Best of the Be astThe Best of the Be astThe Best of the BeastThe Best of the Beast</p>	1000	t	1	2	1	0	\N	\N
\.


--
-- Data for Name: gallery_gallery_tags; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.gallery_gallery_tags (id, gallery_id, tag_id) FROM stdin;
1	1	3
2	1	5
3	1	6
4	1	7
\.


--
-- Data for Name: gallery_galleryimage; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.gallery_galleryimage (id, image, image_title, image_is_main, image_description, gallery_id) FROM stdin;
1	demo/test.jpg	The Best of the Beast	f	est of the BeastThe Best of the BeastThe Best of the Beast The Best of the Be astThe Best of the Be astThe Best of the BeastThe Best of the Beast	1
2	demo/misha.jpg	The Best of the Beast	f	est of the BeastThe Best of the BeastThe Best of the Beast The Best of the Be astThe Best of the Be astThe Best of the BeastThe Best of the Beast	1
3	demo/detailsquare3.jpg	The Best of the Beast	f	est of the BeastThe Best of the BeastThe Best of the Beast The Best of the Be astThe Best of the Be astThe Best of the BeastThe Best of the Beast	1
4	demo/detailsquare.jpg	The Best of the Beast	f	est of the BeastThe Best of the BeastThe Best of the Beast The Best of the Be astThe Best of the Be astThe Best of the BeastThe Best of the Beast	1
5	demo/product1_2.jpg	The Best of the Beast	f	est of the BeastThe Best of the BeastThe Best of the Beast The Best of the Be astThe Best of the Be astThe Best of the BeastThe Best of the Beast	1
6	demo/product3.jpg	The Best of the Beast	f	est of the BeastThe Best of the BeastThe Best of the Beast The Best of the Be astThe Best of the Be astThe Best of the BeastThe Best of the Beast	1
7	demo/getinspired3.jpg	The Best of the Beast	f	est of the BeastThe Best of the BeastThe Best of the Beast The Best of the Be astThe Best of the Be astThe Best of the BeastThe Best of the Beast	1
8	demo/main-slider4.jpg	The Best of the Beast	f	est of the BeastThe Best of the BeastThe Best of the Beast The Best of the Be astThe Best of the Be astThe Best of the BeastThe Best of the Beast	1
9	demo/bg1.jpg	The Best of the Beast	t		1
\.


--
-- Data for Name: home_home; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.home_home (id, slug, seo_title, seo_description, seo_keywords, og_locale, scripts, title, html, is_show, blog_id) FROM stdin;
1	kompaniya-protivoskolzyashie-sistemy	Компания "Противоскользящие Системы"	Компания "Противоскользящие Системы"	Компания "Противоскользящие Системы"	ru_RU		Компания "Противоскользящие Системы"	<p><strong>Производство противоскользящих покрытий, противоскользящих абразивных лент, отделочных антискользящих профилей и накладок на ступени.</strong></p>\r\n\r\n<p>Основной вид деятельности Компании &quot;Противоскользящие Системы&quot; является производство и внедрение новых, высокоэффективных антискользящих покрытий, которые используются везде, где есть вероятность поскользнуться и упасть. Наша продукция - это качественные, надёжные, долговечные и эффективные решения против скольжения.</p>\r\n\r\n<h3>Снижение цен на ВСЮ продукцию!</h3>\r\n\r\n<p>Компания &laquo;Противоскользящие системы&raquo; производит различные противоскользящие ленты, противоскользящие накладки на ступени и противоскользящий профиль. Наша компания в числе первых наладила производство подобных систем и покрытий, призванных обезопасить передвижение людей по скользким поверхностям.</p>\r\n\r\n<p>Противоскользящие системы должны быть использованы на любом покрытии, которое имеет достаточно скользкую поверхность для того чтобы угрожать здоровью прохожих. В наше время противоскользящие ступени являются стандартом, таким же, как противоскользящая лента в бассейн, в сауну. В российских условиях зачастую возникает потребность в установке подобных противоскользящих систем, особенно зимой. Снег, попадающий с улицы, придает обыкновенному покрытию высокий показатель скольжения, что может повлечь за собой многочисленные травмы среди работников и посетителей.</p>\r\n\r\n<p>Противоскользящий материал обладает особыми свойствами, препятствующими скольжению даже в самых неблагоприятных условиях, таких как чрезмерная влажность, или же излишне скользкая подошва обуви. Как правило, противоскользящие системы изготавливаются из алюминия, резины, термоэластопластов и абразивных материалов.</p>\r\n\r\n<p>Резиновые профили препятствуют скольжению вследствие природных свойств резины, в то время как алюминиевые отделочные профили оснащены специальными элементами, препятствующими скольжению.</p>\r\n\r\n<p>Мы производим только высококачественную продукцию с применением современных материалов, являющуюся надежным и эффективным решением против скольжения. Высокий профессионализм наших сотрудников и гибкая ценовая политика позволила нам выйти на лидирующие позиции на рынке противоскользящих материалов и систем.</p>\r\n\r\n<h3><br />\r\nМы предлагаем:</h3>\r\n\r\n<ul>\r\n\t<li>Закладные противоскользящие профили, которые устанавливаются на край ступени во время монтажа лестниц. Это алюминиевые отделочные профили с противоскользящими накладками серии SafeStep и ALPB.</li>\r\n\t<li>Накладные антискользящие покрытия и ленты, которые устанавливают на уже готовые ступени лестниц или скользкие поверхности.</li>\r\n\t<li>Самоклеющиеся ленты серии &quot;ЗПОУ&quot;. Противоскользящие накладки на ступени и самоклеющиеся углы серии &quot;НЕ Падай&quot;. Противоскользящий профиль серии &quot;ЕвроСтупень&quot;. Противоскользящие покрытия производимой &nbsp;компанией 3М Safety Walk. Антискользящие абразивные ленты AntiSlip Systems. Алюминиевые отделочные профили с антискользящими элементами. Резиновые профили, резиновые ленты со специальными противоскользящими компонентами.</li>\r\n\t<li>Грязезащитные покрытия, сохраняющие чистоту и комфорт в помещении; грязезащитные покрытия устанавливаются снаружи и/или в вестибюлях.<br />\r\n\t&nbsp;</li>\r\n</ul>	t	1
\.


--
-- Data for Name: home_homeimage; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.home_homeimage (id, image, image_title, image_is_main, image_description, home_id) FROM stdin;
\.


--
-- Data for Name: include_area_includearea; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.include_area_includearea (id, title, is_show, image, description, html_code, code, sort, url) FROM stdin;
3	ОТЛИЧНОЕ КАЧЕСТВО	t	demo/6435667_orig.jpg	Вся предлагаемая нами продукция соответствует высочайшим мировым нормам качества	\N	advantages	1000	http://nepaday.itbin.ru
2	ЛУЧШИЕ ЦЕНЫ	t	demo/accounting-finance-recruitment.jpg	Индивидуальные предложения на выгодных условиях	\N	advantages	2000	\N
1	БЫСТРАЯ ДОСТАВКА	t	demo/52093745_banner_prodotti_diagnosi1.jpg	Различные виды доставки по стране: начиная от срочной доставки в тот же день до менее срочной доставки в течение нескольких дней	\N	advantages	3000	\N
\.


--
-- Data for Name: menu_mainmenu; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.menu_mainmenu (id, name, is_show, sort, lft, rght, tree_id, level, blog_id, catalog_id, gallery_id, page_id, parent_id) FROM stdin;
16	Алюминиевый угол SafeStep	t	2920	9	10	2	2	\N	\N	\N	\N	14
18	ПВХ полоса Не Скользко	t	2930	11	12	2	2	\N	\N	\N	\N	14
14	Закладные профили из алюминия и резины	t	2900	6	13	2	1	\N	8	\N	\N	2
9	ГАЛЕРЕЯ	t	3000	1	2	3	0	\N	\N	1	\N	\N
10	О НАС	t	4000	1	2	4	0	\N	\N	\N	1	\N
12	КОНТАКТЫ	t	6000	1	2	8	0	\N	\N	\N	2	\N
21	Сувенирная продукция	f	7000	1	2	9	0	\N	\N	\N	\N	\N
24	Ссылки	f	9000	1	2	11	0	\N	\N	\N	\N	\N
2	КАТАЛОГ ПРОДАЖ	t	2000	1	14	2	0	\N	7	\N	\N	\N
17	Самоклеющиеся профили из эластопластов и резины	t	2300	2	3	2	1	\N	9	\N	\N	2
15	Алюминиевый угол УШ-50	t	2910	7	8	2	2	\N	9	\N	\N	14
11	БЛОГ	f	5000	1	2	5	0	1	\N	\N	\N	\N
19	Партнёры и Дилеры	t	5000	1	2	6	0	\N	\N	\N	3	\N
20	Для Дилеров	t	6000	1	2	7	0	\N	\N	\N	3	\N
23	Статьи	t	8000	1	2	10	0	\N	\N	\N	3	\N
1	HOME	t	1000	1	2	1	0	\N	\N	\N	\N	\N
13	Фотолюминесцентные светонакопительные ленты	t	2800	4	5	2	1	\N	9	\N	\N	2
\.


--
-- Data for Name: order_order; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.order_order (id, created, updated, first_name, last_name, email, address, postal_code, city, total_cost, ttn, comment, status_id, user_id) FROM stdin;
1	2018-04-10 21:56:27.72+00	2018-04-10 23:07:14.075+00	Mihail	Mrychev	marychev@garpix.com	`12`12`2	\N	\N	262501.00	\N		2	1
2	2018-04-12 18:19:34.139+00	2018-04-12 18:19:34.929+00	Михаил	Марычев	marychev_mihail@mail.ru	Колесанова	\N	\N	60000.00			\N	1
3	2018-04-13 23:21:33.97+00	2018-04-13 23:21:37.602+00	Mihail	Mrychev	marychev@garpix.com	Ул. 18-Берез	\N	\N	786000.00		Отличный товар!	\N	1
4	2018-04-13 23:52:19.216+00	2018-04-13 23:52:20.03+00	Mihail	Mrychev	marychev@garpix.com	sdas sdvvdf	\N	\N	1020000.00			\N	1
5	2018-04-28 05:27:20.795+00	2018-04-28 05:27:25.348+00	Михаил	Марычев	marychev_mihail@mail.ru	address www	\N	\N	302550.00			\N	1
6	2018-06-07 18:54:52.16+00	2018-06-07 18:54:54.976+00	Михаил	Марычев	marychev_mihail@mail.ru	Колесанова	\N	\N	24270.00			\N	1
\.


--
-- Data for Name: order_orderitem; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.order_orderitem (id, price, quantity, order_id, product_item_id) FROM stdin;
19	1170.00	4	6	19
20	585.00	2	6	18
21	600.00	15	6	20
22	520.00	6	6	23
23	580.00	9	6	28
24	560.00	1	6	22
25	520.00	1	6	26
\.


--
-- Data for Name: order_status; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.order_status (id, name) FROM stdin;
1	Ожидание
2	Оплачен
3	Отправлен
\.


--
-- Data for Name: order_story; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.order_story (id, created, updated, total_cost, comment, order_id, status_id) FROM stdin;
1	2018-04-10 21:56:28.524+00	2018-04-10 21:56:28.524+00	750.00	[+]Добавлен товар `Черная блузка Versace-42` 750.00 x 1 = 750.00 p	1	\N
2	2018-04-10 21:56:29.1+00	2018-04-10 21:56:29.1+00	202500.00	[+]Добавлен товар `Белая блузка Armani - 42` 750.00 x 269 = 201750.00 p	1	\N
3	2018-04-10 21:56:29.797+00	2018-04-10 21:56:29.797+00	262500.00	[+]Добавлен товар `Меховое пальто-44` 30000.00 x 2 = 60000.00 p	1	\N
4	2018-04-10 23:05:16.829+00	2018-04-10 23:05:16.829+00	262500.00		1	3
5	2018-04-10 23:05:54.866+00	2018-04-10 23:05:54.866+00	262500.00		1	2
6	2018-04-10 23:06:14.5+00	2018-04-10 23:06:14.5+00	262501.00	[*]Цена изменена. Товар:`Черная блузка Versace-42` 751.00 x 1 = 751.00 p	1	2
7	2018-04-12 18:19:34.668+00	2018-04-12 18:19:34.668+00	60000.00	[+]Добавлен товар `роза PYAP-1` 30000.00 x 2 = 60000.00 p	2	\N
8	2018-04-13 23:21:34.41+00	2018-04-13 23:21:34.41+00	3750.00	[+]Добавлен товар `Черная блузка Versace-42` 750.00 x 5 = 3750.00 p	3	\N
9	2018-04-13 23:21:34.942+00	2018-04-13 23:21:34.942+00	333750.00	[+]Добавлен товар `Меховое пальто-46` 30000.00 x 11 = 330000.00 p	3	\N
10	2018-04-13 23:21:35.53+00	2018-04-13 23:21:35.53+00	483750.00	[+]Добавлен товар `Меховое пальто-44` 30000.00 x 5 = 150000.00 p	3	\N
11	2018-04-13 23:21:36.207+00	2018-04-13 23:21:36.207+00	663750.00	[+]Добавлен товар `Меховое пальто-42` 30000.00 x 6 = 180000.00 p	3	\N
12	2018-04-13 23:21:36.762+00	2018-04-13 23:21:36.762+00	783750.00	[+]Добавлен товар `Меховое пальто-48` 30000.00 x 4 = 120000.00 p	3	\N
13	2018-04-13 23:21:37.316+00	2018-04-13 23:21:37.316+00	786000.00	[+]Добавлен товар `Черная блузка Versace-44` 750.00 x 3 = 2250.00 p	3	\N
14	2018-04-13 23:52:19.756+00	2018-04-13 23:52:19.756+00	1020000.00	[+]Добавлен товар `роза PYAP-1` 30000.00 x 34 = 1020000.00 p	4	\N
15	2018-04-28 05:27:21.291+00	2018-04-28 05:27:21.291+00	750.00	[+]Добавлен товар `Черная блузка Versace-42` 750.00 x 1 = 750.00 p	5	\N
16	2018-04-28 05:27:21.917+00	2018-04-28 05:27:21.917+00	120750.00	[+]Добавлен товар `роза PYAP-1` 30000.00 x 4 = 120000.00 p	5	\N
17	2018-04-28 05:27:22.557+00	2018-04-28 05:27:22.557+00	150750.00	[+]Добавлен товар `Меховое пальто-44` 30000.00 x 1 = 30000.00 p	5	\N
18	2018-04-28 05:27:23.121+00	2018-04-28 05:27:23.121+00	151700.00	[+]Добавлен товар `Черная блузка Versace-48` 950.00 x 1 = 950.00 p	5	\N
19	2018-04-28 05:27:23.775+00	2018-04-28 05:27:23.775+00	271700.00	[+]Добавлен товар `Меховое пальто-48` 30000.00 x 4 = 120000.00 p	5	\N
20	2018-04-28 05:27:24.43+00	2018-04-28 05:27:24.43+00	272550.00	[+]Добавлен товар `Черная блузка Versace-46` 850.00 x 1 = 850.00 p	5	\N
21	2018-04-28 05:27:25.096+00	2018-04-28 05:27:25.096+00	302550.00	[+]Добавлен товар `Меховое пальто-46` 30000.00 x 1 = 30000.00 p	5	\N
22	2018-06-07 18:54:52.463+00	2018-06-07 18:54:52.463+00	4680.00	[+]Добавлен товар `до 60 шт` 1170.00 x 4 = 4680.00 p	6	\N
23	2018-06-07 18:54:52.896+00	2018-06-07 18:54:52.896+00	5850.00	[+]Добавлен товар `до 60 шт,,` 585.00 x 2 = 1170.00 p	6	\N
24	2018-06-07 18:54:53.276+00	2018-06-07 18:54:53.276+00	14850.00	[+]Добавлен товар `1,2 м` 600.00 x 15 = 9000.00 p	6	\N
25	2018-06-07 18:54:53.65+00	2018-06-07 18:54:53.65+00	17970.00	[+]Добавлен товар `1,2 м` 520.00 x 6 = 3120.00 p	6	\N
26	2018-06-07 18:54:54.022+00	2018-06-07 18:54:54.022+00	23190.00	[+]Добавлен товар `1,2 м-888` 580.00 x 9 = 5220.00 p	6	\N
27	2018-06-07 18:54:54.412+00	2018-06-07 18:54:54.412+00	23750.00	[+]Добавлен товар `1,2 м-` 560.00 x 1 = 560.00 p	6	\N
28	2018-06-07 18:54:54.809+00	2018-06-07 18:54:54.809+00	24270.00	[+]Добавлен товар `1,2 м-888` 520.00 x 1 = 520.00 p	6	\N
\.


--
-- Data for Name: page_page; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.page_page (id, title, description, slug, seo_title, seo_description, seo_keywords, og_locale, scripts, created, updated, html, sort, is_show, is_allow_comments, author_id) FROM stdin;
1	О НАС	Краткая информация о странице.	o-nas	О НАС	О НАС	О НАС	ru_RU		2018-04-01 11:30:09.718+00	2018-04-14 10:28:12.565+00	<p><strong>Pellentesque habitant morbi tristique</strong>&nbsp;senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper.&nbsp;<em>Aenean ultricies mi vitae est.</em>&nbsp;Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed,&nbsp;<code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui.&nbsp;<a href="#">Donec non enim</a>&nbsp;in turpis pulvinar facilisis. Ut felis.</p>\r\n\r\n<p><img alt="" src="/media/uploads/2018/04/01/blog.jpg" style="height:249px; width:600px" /></p>\r\n\r\n<h2>Header Level 2</h2>\r\n\r\n<ol>\r\n\t<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>\r\n\t<li>Aliquam tincidunt mauris eu risus.</li>\r\n</ol>\r\n\r\n<blockquote>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p>\r\n</blockquote>\r\n\r\n<h3>Header Level 3</h3>\r\n\r\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>\r\n\r\n<ul>\r\n\t<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>\r\n\t<li>Aliquam tincidunt mauris eu risus.</li>\r\n</ul>\r\n\r\n<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>	1000	t	t	\N
3	Пусто		null	Пусто	null	null	ru_RU		2018-06-19 17:23:25.694325+00	2018-06-19 23:24:03.593368+00		1000	f	f	\N
2	Контакты	Краткая информация о странице.	kontakt	Контакты	Контакты	Контакты	ru_RU	<div class="page-container container">\r\n    <div class="row">\r\n        <div class="col-md-12 entry-content">\r\n<a id="firmsonmap_biglink" href="http://maps.2gis.ru/#/?history=project/ivanovo/center/41.047377890079,56.957263892855/zoom/16/state/widget/id/9148466024633817/firms/9148466024633817">Перейти к большой карте</a>\r\n<script charset="utf-8" type="text/javascript" src="http://firmsonmap.api.2gis.ru/js/DGWidgetLoader.js"></script>\r\n<script charset="utf-8" type="text/javascript">new DGWidgetLoader({"borderColor":"#a3a3a3","width":"900","height":"600","wid":"8ef1edcf3a24f9bc527475964d453cea","pos":{"lon":"41.047377890079","lat":"56.957263892855","zoom":"16"},"opt":{"ref":"hidden","card":["name","contacts","schedule","payings"],"city":"ivanovo"},"org":[{"id":"9148466024633817","hash":"c598qa34848IJ1HJ97f5uvgc69030J341dd6178649366937596A69GC1J0I920673"}]});</script>\r\n<noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>\r\n</div>\r\n</div>\r\n</div>	2018-04-01 11:34:31.462+00	2018-06-19 23:29:42.544333+00	<table cellspacing="0" id="maintd" style="border-collapse:collapse; border:undefined; width:100%">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style="vertical-align:top">\r\n\t\t\t<p>&nbsp;</p>\r\n\t\t\t</td>\r\n\t\t\t<td style="vertical-align:top">\r\n\t\t\t<p><span style="font-size:medium"><span style="color:#cc3300"><u><strong><span style="font-family:Arial">Оптовые продажи</span></strong></u></span></span></p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td colspan="1" style="vertical-align:top"><img alt="" src="http://nepaday.ru/images/M_images/con_tel.png" style="height:15px; margin:0px; width:15px" title="" /></td>\r\n\t\t\t<td colspan="1" style="vertical-align:top"><span style="font-size:small"><span style="font-family:Arial"><strong><span style="color:#000099">+7 (495) 506-25-26</span></strong> - консультации, поддержка дилеров</span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="vertical-align:top"><img alt="" src="http://nepaday.ru/images/M_images/con_tel.png" style="height:15px; margin:0px; width:15px" title="" /></td>\r\n\t\t\t<td style="vertical-align:top">\r\n\t\t\t<p><span style="font-size:small"><span style="font-family:Arial"><strong><span style="color:#000099">+7 (495) 506-10-66</span></strong> - консультации, поддержка дилеров</span></span></p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td colspan="1" style="vertical-align:top"><img alt="" src="http://nepaday.ru/images/M_images/con_tel.png" style="height:15px; margin:0px; width:15px" title="" /></td>\r\n\t\t\t<td colspan="1" style="vertical-align:top"><span style="font-size:small"><span style="font-family:Arial"><strong><span style="color:#000099">+7 (495) 504-30-65</span></strong>&nbsp;- консультации, поддержка дилеров, <strong><span style="color:#ff0000"><u> оформление заказов</u></span></strong></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td colspan="1" style="vertical-align:top"><span style="font-size:medium"><span style="font-family:Arial"><img alt="" src="http://nepaday.ru/images/M_images/emailButton.png" style="height:15px; margin:0px; width:15px" title="" /></span></span></td>\r\n\t\t\t<td colspan="1" style="vertical-align:top"><span style="font-size:small"><span style="font-size:medium"><span style="font-family:Arial"><span style="color:#000099"><strong><a href="mailto:nepaday@mail.ru">nepaday@mail.ru</a> </strong></span></span></span> <span style="font-family:Arial">- просим отправлять свои заказы по этому адресу электронной почты</span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td colspan="1" style="vertical-align:top">\r\n\t\t\t<p>&nbsp;</p>\r\n\t\t\t</td>\r\n\t\t\t<td colspan="1" style="vertical-align:top">\r\n\t\t\t<p><span style="font-size:small"><span style="font-family:Arial">время работы 10.00-18.00 по будним дням</span></span></p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="vertical-align:top">&nbsp;</td>\r\n\t\t\t<td style="vertical-align:top">\r\n\t\t\t<p><u><strong><span style="font-size:medium"><span style="font-family:Arial"><span style="color:#cc3300">Розничные продажи</span></span></span></strong></u></p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="vertical-align:top">\r\n\t\t\t<p><img alt="" src="http://nepaday.ru/images/M_images/con_address.png" style="height:15px; margin:0px; width:15px" title="" /></p>\r\n\t\t\t</td>\r\n\t\t\t<td style="vertical-align:top">\r\n\t\t\t<p><span style="font-size:small"><span style="font-family:Arial"><span style="color:#cc3300"><strong>Розничный магазин</strong></span> и <span style="color:#cc3300"><strong>Пункт получения заказов</strong></span> для тех клиентов, кто выбрал самовывоз<br />\r\n\t\t\t<br />\r\n\t\t\tТорговый комплекс &quot;Автозапчасти М1&quot; при гостинице &quot;Можайская&quot;<br />\r\n\t\t\tМожайское ш. вл. 165 стр. 1, <strong><span style="color:#cc3300"><u>Павильон № 31</u></span></strong><br />\r\n\t\t\t<br />\r\n\t\t\tЕсли ехать со стороны центра города или с МКАД, то на территории бензозаправки &quot;Сибирьнефть&quot; резкий поворот направо через шлагбаум с табличкой &quot;Автозапчасти&quot;.</span></span><br />\r\n\t\t\t&nbsp;</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="vertical-align:top">\r\n\t\t\t<p><img alt="" src="http://nepaday.ru/images/M_images/con_tel.png" style="height:15px; margin:0px; width:15px" title="" /></p>\r\n\t\t\t</td>\r\n\t\t\t<td style="vertical-align:top">\r\n\t\t\t<p><span style="font-size:small"><span style="font-family:Arial"><strong><span style="color:#000099">+7 (495) 778-93-98 - </span></strong>09.00-18.30 <strong><span style="color:#ff0000">(суб, воскр - 09.00-15.00)</span></strong></span></span></p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="vertical-align:top">\r\n\t\t\t<p><img alt="" src="http://nepaday.ru/images/M_images/con_tel.png" style="height:15px; margin:0px; width:15px" title="" /></p>\r\n\t\t\t</td>\r\n\t\t\t<td style="vertical-align:top">\r\n\t\t\t<p><span style="font-size:small"><span style="font-family:Arial"><strong><span style="color:#000099">+7 (495) 504-18-00 - </span></strong>09.00-18.30 <strong><span style="color:#ff0000">(суб, воскр - 09.00-15.00)</span></strong></span></span></p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="vertical-align:top">\r\n\t\t\t<p><img alt="" src="http://nepaday.ru/images/M_images/con_tel.png" style="height:15px; margin:0px; width:15px" title="" /></p>\r\n\t\t\t</td>\r\n\t\t\t<td style="vertical-align:top">\r\n\t\t\t<p><span style="font-size:small"><span style="font-family:Arial"><strong><span style="color:#000099">+7 (901) 501-47-40 - </span></strong>09.00-18.30 <strong><span style="color:#ff0000">(суб, воскр - 09.00-15.00)</span></strong></span></span></p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>	1000	t	f	\N
\.


--
-- Data for Name: page_page_tags; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.page_page_tags (id, page_id, tag_id) FROM stdin;
1	1	3
2	1	5
3	1	6
4	1	7
5	2	5
6	2	6
\.


--
-- Data for Name: page_pagecomment; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.page_pagecomment (id, created, updated, text, ip_address, username, email, is_show, page_id, user_id) FROM stdin;
\.


--
-- Data for Name: page_pageimage; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.page_pageimage (id, image, image_title, image_is_main, image_description, page_id) FROM stdin;
1	demo/slider2.jpg	О НАС	t		1
\.


--
-- Data for Name: product_product; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.product_product (id, title, description, slug, seo_title, seo_description, seo_keywords, og_locale, scripts, created, updated, html, sort, is_show, articul, is_bestseller, is_new, author_id, level, lft, parent_id, rght, tree_id) FROM stdin;
7	Safestep 43 мм	Закладной алюминиевый профиль против скольжения\r\nНакладка:\tТермоэластопласт\r\nЦвет:\tТемно-коричнев\r\nУпаковка:\t5 штук	uverennyj-shag-50-2	Профиль УШ-50 мм - черный	Профиль УШ-50 мм - черный	Профиль УШ-50 мм - черный-_CLONE	ru_RU		2018-06-04 05:33:04.144+00	2018-06-04 21:03:10.502+00	<table border="1" cellspacing="0" class="tabla3" style="-webkit-text-stroke-width:0px; background-color:#fff6dd; border-collapse:collapse; border:1px solid #006699; font-family:Verdana,Arial,Helvetica,sans-serif; letter-spacing:normal; margin-top:5px; orphans:2; text-decoration-color:initial; text-decoration-style:initial; text-transform:none; widows:2; width:223px; word-spacing:0px">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td rowspan="2" style="background-color:#fff6dd; text-align:center"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><strong><span style="font-family:Arial"><span style="font-size:small">Количество</span></span></strong></span></span></span></td>\r\n\t\t\t<td colspan="2" style="background-color:#fff6dd; text-align:center"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small"><strong>Цена</strong></span></span></span></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="background-color:#fff6dd; text-align:center"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><strong><span style="font-family:Arial"><span style="font-size:small">1,2 м</span></span></strong></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; text-align:center"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small"><strong>2,4 м</strong></span></span></span></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="background-color:#fff6dd; text-align:center"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">до 60 шт</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; text-align:center"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">585 р</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; text-align:center"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">1&#39;170 р</span></span></span></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="background-color:#fff6dd; text-align:center"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">61 - 120&nbsp;</span></span><span style="font-family:Arial"><span style="font-size:small">шт</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; text-align:center"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">570 р</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; text-align:center"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">1&#39;140 р</span></span></span></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="background-color:#fff6dd; text-align:center"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">121 - 240 шт</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; text-align:center"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">520 р</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; text-align:center"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">1&#39;040 р</span></span></span></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="background-color:#fff6dd; text-align:center"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">241 - 500 шт</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; text-align:center"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">480 р</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; text-align:center"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">960 р</span></span></span></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="background-color:#fff6dd; text-align:center"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">от 501&nbsp;</span></span><span style="font-family:Arial"><span style="font-size:small">шт</span></span></span></span></span></td>\r\n\t\t\t<td colspan="2" style="background-color:#fff6dd; text-align:center"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:small"><span style="font-family:Arial"><strong><span style="color:#ff0000">ЗВОНИТЕ!</span></strong></span></span></span></span></span></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>	2000	t	\N	t	t	\N	0	1	\N	2	2
6	Уверенный Шаг 50 мм	Закладной профиль против скольжения "Уверенный Шаг" (УШ-50)\r\nАлюминиевый профиль \r\nс 2 закладными элементами\r\nКрепление:\tПлиточный клей\r\nКаркас:\tАлюминий\r\nВставка:\tМягкий эластопласт\r\nДлина:\t1,2м; 2,4м\r\nНагрузка:\t2000 шаг/сутки\r\n \r\nот 434 руб/м	uverennyj-shag-50-mm	Закладной профиль против скольжения "Уверенный Шаг" (УШ-50)	Уверенный Шаг 50 мм	Уверенный Шаг 50 мм	ru_RU		2018-06-04 05:15:15.218+00	2018-06-18 20:35:07.61277+00	<div style="-webkit-text-stroke-width:0px; text-align:justify"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="background-color:#fff6dd"><span style="font-size:small"><span style="font-family:Arial">Алюминиевый закладной профиль&nbsp;<strong><span style="color:#ff0000">Уверенный шаг (УШ-50)</span></strong>&nbsp;с гибкой вставкой из термоэластопласта и двумя закладными элементами предназначен для установки на углах ступеней лестниц в качестве противоскользящего профиля безопасности и как декоративная отделка.</span></span></span></span></span></span></div>\r\n\r\n<div style="-webkit-text-stroke-width:0px; text-align:justify"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="background-color:#fff6dd">&nbsp;</span></span></span></span></div>\r\n\r\n<div style="-webkit-text-stroke-width:0px; text-align:justify"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="background-color:#fff6dd"><span style="font-size:small"><span style="font-family:Arial">Закладные части устанавливаются при монтаже ваших ступеней под плитку, керамогранит, мрамор, дерево, и крепятся в плиточный или монтажный клей, или раствор. Эластичная накладка укладывается в пазы алюминиевого профиля и благодаря оригинальной конструкции надёжно крепится в нём.</span></span></span></span></span></span></div>\r\n\r\n<div style="-webkit-text-stroke-width:0px; text-align:justify"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="background-color:#fff6dd">&nbsp;</span></span></span></span></div>\r\n\r\n<div style="-webkit-text-stroke-width:0px; text-align:justify"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="background-color:#fff6dd"><span style="font-size:small"><span style="font-family:Arial">Ширина рабочей поверхности профиля -&nbsp;<strong>50 мм</strong>, высота -&nbsp;<strong>16 мм</strong>. Стандартная длина укомплектованного профиля -&nbsp;<strong>1,2 м</strong>&nbsp;и&nbsp;<strong>2,4 м</strong>. Возможна закупка антискользящей вставки в бухтах&nbsp;<strong>до</strong>&nbsp;<strong>24 м</strong>&nbsp;для обустройства длинных ступеней без видимых стыков рабочей поверхности.</span></span></span></span></span></span></div>\r\n\r\n<div style="-webkit-text-stroke-width:0px; text-align:justify"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="background-color:#fff6dd">&nbsp;</span></span></span></span></div>\r\n\r\n<div style="-webkit-text-stroke-width:0px; text-align:justify"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="background-color:#fff6dd"><span style="font-size:small"><span style="font-family:Arial">Конструкция допускает&nbsp;<strong>очень высокую проходимость</strong>&nbsp;и&nbsp;<strong>многократные зимние циклы заморозки/разморозки</strong>&nbsp;без разрушения поверхности ступеней. Профиль&nbsp;</span></span><span style="font-size:small"><span style="font-family:Arial"><strong><span style="color:#ff0000">Уверенный шаг</span></strong></span></span><span style="font-size:small"><span style="font-family:Arial">&nbsp;самый надёжный и долговечный из серии закладных профилей. Температурный режим эксплуатации - от&nbsp;<strong>-40&deg;С</strong>&nbsp;до&nbsp;<strong>+60&ordm;С</strong>. Нет специальных температурных требований на этапе установки.</span></span></span></span></span></span></div>\r\n\r\n<div style="-webkit-text-stroke-width:0px; text-align:left">\r\n<p><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="background-color:#fff6dd"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:small"><span style="font-family:Arial">По запросу заказчика возможно производство накладки&nbsp;<strong>любого цвета по стандарту RAL</strong>.</span></span></span></span></span></span></span></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n\t<li style="text-align:left">\r\n\t<div><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:small"><span style="font-family:Arial">Подготовьте угол ступени. Выровняйте место укладки&nbsp;профиля против скольжения и плитки с помощью любого быстросхватывающегося плиточного клея.</span></span></span></span></span></div>\r\n\t</li>\r\n\t<li style="text-align:left">\r\n\t<div><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:small"><span style="font-family:Arial">Вытащите сменную противоскользящую резиновую вставку из профиля.</span></span></span></span></span></div>\r\n\t</li>\r\n\t<li style="text-align:left">\r\n\t<div><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small"><span style="font-family:Arial">Разметьте место монтажа профиля.</span></span></span></span></span></span></div>\r\n\t</li>\r\n\t<li style="text-align:left">\r\n\t<div><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-size:small"><span style="font-family:Arial"><span style="font-size:small"><span style="font-family:Arial">Уложите слой клея на прикрепляемую поверхность.</span></span></span></span></span></span></span></div>\r\n\t</li>\r\n\t<li style="text-align:left">\r\n\t<div><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small"><span style="font-family:Arial">Установите алюминиевую часть&nbsp;профиля.</span></span></span></span></span></span></div>\r\n\t</li>\r\n\t<li style="text-align:left">\r\n\t<div><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small"><span style="font-family:Arial">Наложите слой плиточного клея на наружный слой&nbsp;закладных элементов.</span></span></span></span></span></span></div>\r\n\t</li>\r\n\t<li style="text-align:left">\r\n\t<div><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small"><span style="font-family:Arial">Установите облицовочный материал ступени (керамическая плитка, керамогранит, дерево, металл и пр.) с заходом на&nbsp;закладные элементы профиля.</span></span></span></span></span></span></div>\r\n\t</li>\r\n\t<li style="text-align:left">\r\n\t<div><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">Дайте клею затвердеть (3-7часов)<span style="font-size:small">.</span></span></span></span></span></span></div>\r\n\t</li>\r\n\t<li style="text-align:left"><span style="font-family:Arial"><span style="font-size:small"><span style="font-size:small"><span style="font-size:small">Вставьте в паз алюминиевого профиля сменную резиновую вставку.</span></span></span></span></li>\r\n</ol>\r\n</div>	1000	t	\N	t	t	\N	0	1	\N	4	1
11	с вставкой основных цветов - черный, темно-коричневый, коричневый, серый, бежевый	Алюминий + вставка\r\nКоличество\t1,2 м\t2,4 м\r\nдо 60 шт\t600 р\t1'200 р\r\n61 - 120 шт\t580 р\t1'160 р\r\n121 - 240 шт\t560 р\t1'120 р\r\n241 - 500 шт\t520 р\t1'040 р\r\nот 501 шт\tЗВОНИТЕ!	s-vstavkoj-osnovnyh-cvetov-chernyj-temno-korichnev	с вставкой основных цветов - черный, темно-коричневый, коричневый, серый, бежевый	с вставкой основных цветов - черный, темно-коричневый, коричневый, серый, бежевый	с вставкой основных цветов - черный, темно-коричневый, коричневый, серый, бежевый	ru_RU		2018-06-05 20:11:58.885+00	2018-06-07 18:40:50.226+00	<table cellspacing="0" class="tabla" style="-webkit-text-stroke-width:0px; background-color:#fff6dd; border-collapse:collapse; border:undefined; font-family:Verdana,Arial,Helvetica,sans-serif; letter-spacing:normal; orphans:2; text-decoration-color:initial; text-decoration-style:initial; text-transform:none; widows:2; width:100%; word-spacing:0px">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td colspan="3" style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><strong><span style="font-family:Arial"><span style="font-size:small">Алюминий + вставка</span></span></strong></span></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><strong><span style="font-family:Arial"><span style="font-size:small">Количество</span></span></strong></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><strong><span style="font-family:Arial"><span style="font-size:small">1,2 м</span></span></strong></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><strong><span style="font-family:Arial"><span style="font-size:small">2,4 м</span></span></strong></span></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">до 60 шт</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">600 р</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">1&#39;200 р</span></span></span></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">61 - 120 шт</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">580 р</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">1&#39;160 р</span></span></span></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">121 - 240 шт</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">560 р</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">1&#39;120 р</span></span></span></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">241 - 500 шт</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">520 р</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">1&#39;040 р</span></span></span></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">от 501 шт</span></span></span></span></span></td>\r\n\t\t\t<td colspan="2" style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="color:#ff0000"><strong><span style="font-family:Arial"><span style="font-size:small">ЗВОНИТЕ!</span></span></strong></span></span></span></span></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>	1000	t	\N	t	t	\N	1	2	6	3	1
12	с вставкой дополнительных цветов - желтый, синий, голубой, белый, красный, зеленый	Алюминий + вставка\r\nКоличество\t1,2 м\t2,4 м\r\nдо 60 шт\t600 р\t1'200 р\r\n61 - 120 шт\t580 р\t1'160 р\r\n121 - 240 шт\t560 р\t1'120 р\r\n241 - 500 шт\t520 р\t1'040 р\r\nот 501 шт\tЗВОНИТЕ!-_CLONE	s-vstavkoj-osnovnyh-cv	с вставкой основных цветов - черный, темно-коричневый, коричневый, серый, бежевый-_CLONE	с вставкой основных цветов - черный, темно-коричневый, коричневый, серый, бежевый-_CLONE	с вставкой основных цветов - черный, темно-коричневый, коричневый, серый, бежевый-_CLONE	ru_RU		2018-06-05 20:47:30.73+00	2018-06-18 20:35:29.411974+00	<table cellspacing="0" class="tabla" style="-webkit-text-stroke-width:0px; background-color:#fff6dd; border-collapse:collapse; border:undefined; font-family:Verdana,Arial,Helvetica,sans-serif; letter-spacing:normal; orphans:2; text-decoration-color:initial; text-decoration-style:initial; text-transform:none; widows:2; width:100%; word-spacing:0px">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td colspan="3" style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><strong><span style="font-family:Arial"><span style="font-size:small">Алюминий + вставка</span></span></strong></span></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><strong><span style="font-family:Arial"><span style="font-size:small">Количество</span></span></strong></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><strong><span style="font-family:Arial"><span style="font-size:small">1,2 м</span></span></strong></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><strong><span style="font-family:Arial"><span style="font-size:small">2,4 м</span></span></strong></span></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">до 60 шт</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">620 р</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">1&#39;240 р</span></span></span></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">61 - 120 шт</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">600 р</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">1&#39;200 р</span></span></span></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">121 - 240 шт</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">580 р</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">1&#39;160 р</span></span></span></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">241 - 500 шт</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">540 р</span></span></span></span></span></td>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">1&#39;080 р</span></span></span></span></span></td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="font-family:Arial"><span style="font-size:small">от 501 шт</span></span></span></span></span></td>\r\n\t\t\t<td colspan="2" style="background-color:#fff6dd; border-color:#006699"><span style="font-size:12px"><span style="color:#595959"><span style="font-family:Verdana,Arial,Helvetica,sans-serif"><span style="color:#ff0000"><strong><span style="font-family:Arial"><span style="font-size:small">ЗВОНИТЕ!</span></span></strong></span></span></span></span></td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>	1000	t	\N	t	t	\N	1	2	6	3	1
\.


--
-- Data for Name: product_product_catalog; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.product_product_catalog (id, product_id, catalog_id) FROM stdin;
1	6	8
2	7	8
3	6	11
4	12	8
5	12	11
\.


--
-- Data for Name: product_product_recommend_products; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.product_product_recommend_products (id, from_product_id, to_product_id) FROM stdin;
1	6	6
2	6	7
3	7	6
\.


--
-- Data for Name: product_product_tags; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.product_product_tags (id, product_id, tag_id) FROM stdin;
\.


--
-- Data for Name: product_productcomment; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.product_productcomment (id, created, updated, text, ip_address, username, email, is_show, product_id, user_id) FROM stdin;
1	2018-04-11 17:14:31.38+00	2018-04-11 17:19:30.769+00	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend l	127.0.0.1	marychev	marychev@garpix.com	t	\N	1
2	2018-04-11 17:17:55.643+00	2018-04-11 17:19:30.97+00	ni's new range of trendy tops, crafted with intricate details. Create a chic statement look by teaming this lace number with skinny jeans and pumps.	127.0.0.1	marychev	marychev@garpix.com	t	\N	1
3	2018-04-11 17:19:07.232+00	2018-04-11 17:19:31.148+00	ni's new range of trendy tops, crafted with intricate details. Create a chic statement look by teaming this lace number with skinny jeans and pumps.	127.0.0.1	marychev	marychev@garpix.com	t	\N	1
4	2018-04-11 17:40:46.575+00	2018-04-11 17:40:46.575+00	SUCCESS !!!	127.0.0.1	marychev	marychev@garpix.com	f	\N	1
5	2018-04-13 22:48:27.724+00	2018-04-13 22:48:27.724+00	dwa aw aw	127.0.0.1	marychev	marychev@garpix.com	f	\N	1
6	2018-04-13 22:50:40.836+00	2018-04-13 22:50:40.836+00	wsdwdw	127.0.0.1	marychev	marychev@garpix.com	f	\N	1
7	2018-04-13 22:51:59.248+00	2018-04-13 22:51:59.248+00	Define style this season with Armani's new range of trendy tops, crafted with intricate details. Create a chic statement look by teaming this lace numb	127.0.0.1	marychev	marychev@garpix.com	f	\N	1
8	2018-04-13 23:01:24.726+00	2018-04-13 23:01:24.726+00	wd wadw	127.0.0.1	marychev	marychev@garpix.com	f	\N	1
\.


--
-- Data for Name: product_productimage; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.product_productimage (id, image, image_title, image_is_main, image_description, product_id) FROM stdin;
11	catalog1/safestep_brown_600x400.jpg	Профиль УШ-50 мм Темно-коричнев	t		7
12	групповой_товар/ush-50_black_600x400.jpg	черный	t		11
13	групповой_товар/ush-50_brown_600x400.jpg	коричневый	f		11
15	групповой_товар/alpb_grey_600x400.jpg	бежевый	f		11
18	групповой_товар/ush-50_dbrown_600x400.jpg	темно-коричневый	f		11
19	групповой_товар/ush-50_grey_600x400.jpg	серый	f		11
10	catalog1/ush-50_montaj_600x400.jpg	Монтаж закладного профиля УШ-50	f		6
16	catalog1/ush-50_black_600x400.jpg	Уверенный Шаг 50 мм	t		6
17	ush-50_example_1_600x400.jpg	Пример использования закладного профиля УШ-50	f		6
14	групповой_товар/2/ush-50_green_600x400.jpg	зеленый	t		12
20	групповой_товар/2/ush-50_blue_600x400.jpg	синий	f		12
21	групповой_товар/2/ush-50_lblue_600x400.jpg	голубой	f		12
22	групповой_товар/2/ush-50_white_600x400.jpg	белый	f		12
23	групповой_товар/2/ush-50_red_600x400.jpg	красный	f		12
24	групповой_товар/2/ush-50_yellow_600x400.jpg	желтый	f		12
\.


--
-- Data for Name: product_productitem; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.product_productitem (id, created, updated, name, articul, price, price_discount, price_purchase, quantity, is_main, product_id, is_opt, unit, from_qty, to_qty) FROM stdin;
18	2018-06-04 05:35:12.319+00	2018-06-07 18:54:53.12+00	до 60 шт,,	1,2 м	585.00	0.00	0.00	-2	f	7	f	0	1	1
19	2018-06-04 05:35:12.321+00	2018-06-07 18:54:52.744+00	до 60 шт	2,4 м	1170.00	0.00	0.00	-4	f	7	f	0	1	1
20	2018-06-05 20:11:58.981+00	2018-06-07 18:54:53.494+00	1,2 м	1,2_m	600.00	0.00	0.00	-15	f	11	f	0	0	60
21	2018-06-05 20:12:23.884+00	2018-06-05 20:21:00.723+00	1,2 м	1,2_m	580.00	0.00	0.00	0	f	11	f	0	61	120
22	2018-06-05 20:17:22.396+00	2018-06-07 18:54:54.653+00	1,2 м-	1,2_m-	560.00	0.00	0.00	-1	f	11	f	0	121	240
23	2018-06-05 20:17:22.48+00	2018-06-07 18:54:53.865+00	1,2 м	1,2_m	520.00	0.00	0.00	-6	t	11	f	0	241	500
24	2018-06-05 20:22:41.607+00	2018-06-05 20:23:50.454+00	1,2 м	1,2_m	0.00	0.00	0.00	0	f	11	t	0	501	\N
25	2018-06-05 20:49:43.291+00	2018-06-05 20:53:15.138+00	1,2 м-8888	1,2_m	0.00	0.00	0.00	0	f	12	t	0	501	\N
26	2018-06-05 20:49:43.388+00	2018-06-07 18:54:55.056+00	1,2 м-888	1,2_m-_CLONE	520.00	0.00	0.00	-1	f	12	f	0	241	500
27	2018-06-05 20:49:43.454+00	2018-06-05 20:52:59.986+00	1,2 м-888	1,2_m	560.00	0.00	0.00	0	f	12	f	0	121	240
28	2018-06-05 20:49:43.521+00	2018-06-07 18:54:54.243+00	1,2 м-888	1,2_m	580.00	0.00	0.00	-9	t	12	f	0	61	120
29	2018-06-05 20:49:43.604+00	2018-06-05 20:52:40.988+00	1,2 м-888	1,2_m-	600.00	0.00	0.00	0	f	12	f	0	0	60
\.


--
-- Data for Name: settings_template_footer; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.settings_template_footer (id, title, is_show, text_info_id) FROM stdin;
1	Нижний футер	t	1
\.


--
-- Data for Name: settings_template_footer_blogs; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.settings_template_footer_blogs (id, footer_id, blog_id) FROM stdin;
\.


--
-- Data for Name: settings_template_footer_catalog; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.settings_template_footer_catalog (id, footer_id, catalog_id) FROM stdin;
7	1	8
8	1	9
9	1	7
\.


--
-- Data for Name: settings_template_footer_galleries; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.settings_template_footer_galleries (id, footer_id, gallery_id) FROM stdin;
\.


--
-- Data for Name: settings_template_footer_list_link; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.settings_template_footer_list_link (id, footer_id, listlink_id) FROM stdin;
\.


--
-- Data for Name: settings_template_footer_page; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.settings_template_footer_page (id, footer_id, page_id) FROM stdin;
5	1	1
6	1	2
\.


--
-- Data for Name: settings_template_settingstemplate; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.settings_template_settingstemplate (id, title, is_included, email, phone, address, logo, robots_txt, terms_of_use, scripts, footer_id, home_id, site_id, meta) FROM stdin;
1	Nepaday.ru	t	nepaday@mail.ru	+7 (495) 506-10-66	\N	logo_neskolzko_transparent.png	User-agent: Yandex\r\nDisallow: /admin\r\nDisallow: /dev_init	<p><strong>ПОЛЬЗОВАТЕЛЬСКОЕ СОГЛАШЕНИЕ</strong></p>\r\n\r\n<p>г.&nbsp;Город&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &laquo;день&raquo;&nbsp;месяц&nbsp;годг.</p>\r\n\r\n<p><strong>1. ОБЩИЕ ПОЛОЖЕНИЯ</strong></p>\r\n\r\n<p>1.1. Настоящее Пользовательское соглашение (далее &ndash; Соглашение) относится к сайту Интернет-магазина &laquo;название интернет-магазина&raquo;, расположенному по адресу www.адрес интернет-магазина, и ко всем соответствующим сайтам, связанным с сайтом&nbsp;адрес интернет-магазина.</p>\r\n\r\n<p>1.2. Сайт Интернет-магазина &laquo;название интернет-магазина&raquo; (далее &ndash; Сайт) является собственностью&nbsp;название организации, предприятия</p>\r\n\r\n<p>1.3. Настоящее Соглашение регулирует отношения между Администрацией сайта Интернет-магазина &laquo;&nbsp;название интернет-магазина&raquo;(далее &ndash; Администрация сайта) и Пользователем данного Сайта.</p>\r\n\r\n<p>1.4. Администрация сайта оставляет за собой право в любое время изменять, добавлять или удалять пункты настоящего Соглашения без уведомления Пользователя.</p>\r\n\r\n<p>1.5. Продолжение использования Сайта Пользователем означает принятие Соглашения и изменений, внесенных в настоящее Соглашение.</p>\r\n\r\n<p>1.6. Пользователь несет персональную ответственность за проверку настоящего Соглашения на наличие изменений в нем.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>2. ОПРЕДЕЛЕНИЯ ТЕРМИНОВ</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>2.1. Перечисленные ниже термины имеют для целей настоящего Соглашения следующее значение:</p>\r\n\r\n<p>2.1.1 &laquo;название интернет-магазина&raquo; &ndash; Интернет-магазин, расположенный на доменном имени www.адрес интернет-магазина, осуществляющий свою деятельность посредством Интернет-ресурса и сопутствующих ему сервисов.</p>\r\n\r\n<p>2.1.2. Интернет-магазин &ndash; сайт, содержащий информацию о Товарах, Продавце, позволяющий осуществить выбор, заказ и (или) приобретение Товара.</p>\r\n\r\n<p>2.1.3. Администрация сайта Интернет-магазина &ndash; уполномоченные сотрудники на управления Сайтом, действующие от имени&nbsp;название организации.</p>\r\n\r\n<p>2.1.4. Пользователь сайта Интернет-магазина (далее ? Пользователь) &ndash; лицо, имеющее доступ к Сайту, посредством сети Интернет и использующее Сайт.</p>\r\n\r\n<p>2.1.5. Содержание сайта Интернет-магазина (далее &ndash; Содержание) - охраняемые результаты интеллектуальной деятельности, включая тексты литературных произведений, их названия, предисловия, аннотации, статьи, иллюстрации, обложки, музыкальные произведения с текстом или без текста, графические, текстовые, фотографические, производные, составные и иные произведения, пользовательские интерфейсы, визуальные интерфейсы, названия товарных знаков, логотипы, программы для ЭВМ, базы данных, а также дизайн, структура, выбор, координация, внешний вид, общий стиль и расположение данного Содержания<em>,&nbsp;</em>входящего в состав Сайта&nbsp;<em>и&nbsp;</em>другие объекты интеллектуальной собственности все вместе и/или по отдельности, содержащиеся на сайте Интернет-магазина.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>3. ПРЕДМЕТ СОГЛАШЕНИЯ</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>3.1. Предметом настоящего Соглашения является предоставление Пользователю Интернет-магазина доступа к содержащимся на Сайте Товарам и оказываемым услугам.</p>\r\n\r\n<p><em>3.1.1.</em>&nbsp;<em>Интернет-магазин предоставляет Пользователю следующие виды услуг (сервисов):</em></p>\r\n\r\n<ul>\r\n\t<li><em>доступ к электронному контенту на&nbsp;бесплатной&nbsp;основе, с правом приобретения (скачивания), просмотра контента;</em></li>\r\n\t<li><em>доступ к средствам поиска и навигации Интернет-магазина;</em></li>\r\n\t<li><em>предоставление Пользователю возможности размещения сообщений, комментариев, рецензий Пользователей, выставления оценок контенту Интернет-магазина;</em></li>\r\n\t<li><em>доступ к информации о Товаре и к информации о приобретении Товара на&nbsp;бесплатной&nbsp;основе;</em></li>\r\n\t<li><em>иные виды услуг (сервисов), реализуемые на страницах Интернет-магазина.</em></li>\r\n</ul>\r\n\r\n<p><em>3.1.2. Под действие настоящего Соглашения подпадают все существующие (реально функционирующие) на данный момент услуги (сервисы) Интернет-магазина, а также любые их последующие модификации и появляющиеся в дальнейшем дополнительные услуги (сервисы) Интернет-магазина.</em></p>\r\n\r\n<p>3.2. Доступ к Интернет-магазину предоставляется на _________ основе.</p>\r\n\r\n<p>3.3. Настоящее Соглашение является публичной офертой. Получая доступ к Сайту Пользователь считается присоединившимся к настоящему Соглашению.</p>\r\n\r\n<p>3.4. Использование материалов и сервисов Сайта регулируется нормами действующего законодательства Российской Федерации</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>4. ПРАВА И ОБЯЗАННОСТИ СТОРОН</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>4.1. Администрация сайта вправе:</strong></p>\r\n\r\n<p>4.1.1. Изменять правила пользования Сайтом, а также изменять содержание данного Сайта. Изменения вступают в силу с момента публикации новой редакции Соглашения на Сайте.</p>\r\n\r\n<p>4.1.2. Ограничить доступ к Сайту в случае нарушения Пользователем условий настоящего Соглашения.</p>\r\n\r\n<p><strong>4.2. Пользователь вправе:</strong></p>\r\n\r\n<p>4.2.1. Получить доступ к использованию Сайта после соблюдения требований о регистрации&nbsp;.</p>\r\n\r\n<p>4.2.2. Пользоваться всеми имеющимися на Сайте услугами, а также приобретать любые Товары, предлагаемые на Сайте.</p>\r\n\r\n<p>4.2.3. Задавать любые вопросы, относящиеся к услугам Интернет-магазина по реквизитам, которые находятся в разделе Сайта &laquo;название раздела&raquo;.</p>\r\n\r\n<p>4.2.4. Пользоваться Сайтом исключительно в целях и порядке, предусмотренных Соглашением и не запрещенных законодательством Российской Федерации.</p>\r\n\r\n<p><strong>4.3. Пользователь Сайта обязуется:</strong></p>\r\n\r\n<p>4.3.1. Предоставлять по запросу Администрации сайта дополнительную информацию, которая имеет непосредственное отношение к предоставляемым услугам данного Сайта.</p>\r\n\r\n<p>4.3.2. Соблюдать имущественные и неимущественные права авторов и иных правообладателей при использовании Сайта.</p>\r\n\r\n<p>4.3.3. Не предпринимать действий, которые могут рассматриваться как нарушающие нормальную работу Сайта.</p>\r\n\r\n<p>4.3.4. Не распространять с использованием Сайта любую конфиденциальную и охраняемую законодательством Российской Федерации информацию о физических либо юридических лицах.</p>\r\n\r\n<p>4.3.5. Избегать любых действий, в результате которых может быть нарушена конфиденциальность охраняемой законодательством Российской Федерации информации.</p>\r\n\r\n<p>4.3.6. Не использовать Сайт для распространения информации рекламного характера, иначе как с согласия Администрации сайта.</p>\r\n\r\n<p>4.3.7. Не использовать сервисы сайта Интернет-магазина с целью:</p>\r\n\r\n<p>4.3.7. 1. загрузки контента, который является незаконным, нарушает любые права третьих лиц; пропагандирует насилие, жестокость, ненависть и (или) дискриминацию по расовому, национальному, половому, религиозному, социальному признакам; содержит недостоверные сведения и (или) оскорбления в адрес конкретных лиц, организаций, органов власти.</p>\r\n\r\n<p>4.3.7. 2. побуждения к совершению противоправных действий, а также содействия лицам, действия которых направлены на нарушение ограничений и запретов, действующих на территории Российской Федерации.</p>\r\n\r\n<p>4.3.7. 3. нарушения прав несовершеннолетних лиц и (или) причинение им вреда в любой форме.</p>\r\n\r\n<p>4.3.7. 4. ущемления прав меньшинств.</p>\r\n\r\n<p>4.3.7. 5. представления себя за другого человека или представителя организации и (или) сообщества без достаточных на то прав, в том числе за сотрудников данного Интернет-магазина.</p>\r\n\r\n<p>4.3.7. 6. введения в заблуждение относительно свойств и характеристик какого-либо Товара из каталога Интернет-магазина, размещенного на Сайте.</p>\r\n\r\n<p>4.3.7. 7. некорректного сравнения Товара, а также формирования негативного отношения к лицам, (не) пользующимся определенными Товарами, или осуждения таких лиц.</p>\r\n\r\n<p><strong>4.4. Пользователю запрещается:</strong></p>\r\n\r\n<p>4.4.1. Использовать любые устройства, программы, процедуры, алгоритмы и методы, автоматические устройства или эквивалентные ручные процессы для доступа, приобретения, копирования или отслеживания содержания Сайта данного Интернет-магазина;</p>\r\n\r\n<p>4.4.2. Нарушать надлежащее функционирование Сайта;</p>\r\n\r\n<p>4.4.3. Любым способом обходить навигационную структуру Сайта для получения или попытки получения любой информации, документов или материалов любыми средствами, которые специально не представлены сервисами данного Сайта;</p>\r\n\r\n<p>4.4.4. Несанкционированный доступ к функциям Сайта, любым другим системам или сетям, относящимся к данному Сайту, а также к любым услугам, предлагаемым на Сайте;</p>\r\n\r\n<p>4.4.4. Нарушать систему безопасности или аутентификации на Сайте или в любой сети, относящейся к Сайту.</p>\r\n\r\n<p>4.4.5. Выполнять обратный поиск, отслеживать или пытаться отслеживать любую информацию о любом другом Пользователе Сайта.</p>\r\n\r\n<p>4.4.6. Использовать Сайт и его Содержание в любых целях, запрещенных законодательством Российской Федерации, а также подстрекать к любой незаконной деятельности или другой деятельности, нарушающей права интернет-магазина или других лиц.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>5. ИСПОЛЬЗОВАНИЕ САЙТА ИНТЕРНЕТ-МАГАЗИНА</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>5.1. Сайт и Содержание, входящее в состав Сайта, принадлежит и управляется Администрацией сайта<em>.</em></p>\r\n\r\n<p>5.2. Содержание Сайта не может быть скопировано, опубликовано, воспроизведено, передано или распространено любым способом, а также размещено в глобальной сети &laquo;Интернет&raquo; без предварительного письменного согласия Администрации сайта.</p>\r\n\r\n<p>5.3. Содержание Сайта защищено авторским правом, законодательством о товарных знаках, а также другими правами, связанными с интеллектуальной собственностью, и законодательством о недобросовестной конкуренции.</p>\r\n\r\n<p>5.4. Приобретение Товара, предлагаемого на Сайте, может потребовать создания учётной записи Пользователя.</p>\r\n\r\n<p>5.5. Пользователь несет персональную ответственность за сохранение конфиденциальности информации учётной записи, включая пароль, а также за всю без исключения деятельность, которая ведётся от имени Пользователя учётной записи.</p>\r\n\r\n<p>5.6. Пользователь должен незамедлительно уведомить Администрацию сайта о несанкционированном использовании его учётной записи или пароля или любом другом нарушении системы безопасности.</p>\r\n\r\n<p>5.7. Администрация сайта обладает правом в одностороннем порядке аннулировать учетную запись Пользователя, если она не использовалась более&nbsp;количество месяцев&nbsp;календарных месяцев подряд без уведомления Пользователя.</p>\r\n\r\n<p>5.7. Настоящее Соглашение распространяет свое действия на все дополнительные положения и условия о покупке Товара и оказанию услуг, предоставляемых на Сайте.</p>\r\n\r\n<p>5.8. Информация, размещаемая на Сайте не должна истолковываться как изменение настоящего Соглашения.</p>\r\n\r\n<p>5.9. Администрация сайта имеет право в любое время без уведомления Пользователя вносить изменения в перечень Товаров и услуг, предлагаемых на Сайте, и (или) в цены, применимые к таким Товарам по их реализации и (или) оказываемым услугам Интернет-магазином.</p>\r\n\r\n<p>5.10. Документы, указанные в пунктах 5.10.1 - 5.10.4 настоящего Соглашения регулируют в соответствующей части и распространяют свое действие на использование Пользователем Сайта. В настоящее Соглашение включены следующие документы:</p>\r\n\r\n<p>5.10.1. Политика конфиденциальности;</p>\r\n\r\n<p>5.10.2. Договор купли-продажи товаров дистанционным способом;</p>\r\n\r\n<p>5.10.3. Заявка на оформление заказа;</p>\r\n\r\n<p>5.10.4. Предложения и замечания.</p>\r\n\r\n<p>5.11. Любой из документов, перечисленных в пункте 5.10. настоящего Соглашения может подлежать обновлению. Изменения вступают в силу с момента их опубликования на Сайте.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>6. ОТВЕТСТВЕННОСТЬ</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>6.1. Любые убытки, которые Пользователь может понести в случае умышленного или неосторожного нарушения любого положения настоящего Соглашения, а также вследствие несанкционированного доступа к коммуникациям другого Пользователя, Администрацией сайта не возмещаются.</p>\r\n\r\n<p>6.2. Администрация сайта не несет ответственности за:</p>\r\n\r\n<p>6.2.1. Задержки или сбои в процессе совершения операции, возникшие вследствие непреодолимой силы, а также любого случая неполадок в телекоммуникационных, компьютерных, электрических и иных смежных системах.</p>\r\n\r\n<p>6.2.2. Действия систем переводов, банков, платежных систем и за задержки связанные с их работой.</p>\r\n\r\n<p>6.2.3. Надлежащее функционирование Сайта, в случае, если Пользователь не имеет необходимых технических средств для его использования, а также не несет никаких обязательств по обеспечению пользователей такими средствами.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>7. НАРУШЕНИЕ УСЛОВИЙ ПОЛЬЗОВАТЕЛЬСКОГО СОГЛАШЕНИЯ</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>7.1. Администрация сайта вправе раскрыть любую собранную о Пользователе данного Сайта информацию, если раскрытие необходимо в связи с расследованием или жалобой в отношении неправомерного использования Сайта либо для установления (идентификации) Пользователя, который может нарушать или вмешиваться в права Администрации сайта или в права других Пользователей Сайта.</p>\r\n\r\n<p>7.2. Администрация сайта имеет право раскрыть любую информацию о Пользователе, которую посчитает необходимой для выполнения положений действующего законодательства или судебных решений, обеспечения выполнения условий настоящего Соглашения, защиты прав или безопасности&nbsp;название организации, Пользователей.</p>\r\n\r\n<p>7.3. Администрация сайта имеет право раскрыть информацию о Пользователе, если действующее законодательство Российской Федерации требует или разрешает такое раскрытие.</p>\r\n\r\n<p>7.4. Администрация сайта вправе без предварительного уведомления Пользователя прекратить и (или) заблокировать доступ к Сайту, если Пользователь нарушил настоящее Соглашение или содержащиеся в иных документах условия пользования Сайтом, а также в случае прекращения действия Сайта либо по причине технической неполадки или проблемы.</p>\r\n\r\n<p>7.5. Администрация сайта не несет ответственности перед Пользователем или третьими лицами за прекращение доступа к Сайту в случае нарушения Пользователем любого положения настоящего Соглашения или иного документа, содержащего условия пользования Сайтом.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>8. РАЗРЕШЕНИЕ СПОРОВ</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>8.1. В случае возникновения любых разногласий или споров между Сторонами настоящего Соглашения обязательным условием до обращения в суд является предъявление претензии (письменного предложения о добровольном урегулировании спора).</p>\r\n\r\n<p>8.2. Получатель претензии в течение 30 календарных дней со дня ее получения, письменно уведомляет заявителя претензии о результатах рассмотрения претензии.</p>\r\n\r\n<p>8.3. При невозможности разрешить спор в добровольном порядке любая из Сторон вправе обратиться в суд за защитой своих прав, которые предоставлены им действующим законодательством Российской Федерации.</p>\r\n\r\n<p>8.4. Любой иск в отношении условий использования Сайта должен быть предъявлен в течение&nbsp;срок&nbsp;после возникновения оснований для иска, за исключением защиты авторских прав на охраняемые в соответствии с законодательством материалы Сайта. При нарушении условий данного пункта любой иск или основания для иска погашаются исковой давностью.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>9. ДОПОЛНИТЕЛЬНЫЕ УСЛОВИЯ</strong></p>\r\n\r\n<p>9.1. Администрация сайта не принимает встречные предложения от Пользователя относительно изменений настоящего Пользовательского соглашения.</p>\r\n\r\n<p>9.2. Отзывы Пользователя, размещенные на Сайте, не являются конфиденциальной информацией и могут быть использованы Администрацией сайта без ограничений.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Обновлено &laquo;день&raquo;&nbsp;месяц&nbsp;год&nbsp;г.</p>		\N	1	1	
\.


--
-- Data for Name: site_info_listlink; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.site_info_listlink (id, title, url, sort, is_show, type_link) FROM stdin;
1	Документация для админа	http://google.com	0	t	L
\.


--
-- Data for Name: site_info_socialnetwork; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.site_info_socialnetwork (id, image_title, image_is_main, image_description, title, image, html_link, url) FROM stdin;
1	vk	f	\N	vk	\N	<i class="fa fa-facebook"></i>	http://google.com
2	test	f	\N	test	demo/photo_2017-10-25_21-36-05.jpg	\N	http://google.ru
\.


--
-- Data for Name: site_info_tag; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.site_info_tag (id, title) FROM stdin;
1	куртка
2	верхняя одежда
3	мода
4	летняя обувь
5	интересное
6	важное
7	одежда
\.


--
-- Data for Name: site_info_textinfo; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.site_info_textinfo (id, title, html) FROM stdin;
1	Телефоны для связи	<h4>+7 (495) 506-10-66</h4>\r\n\r\n<h4>+7 (495) 504-30-65</h4>\r\n\r\n<h4>+7 (495) 506-25-26</h4>\r\n\r\n<h4>nepaday@mail.ru</h4>
\.


--
-- Data for Name: users_userlink; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.users_userlink (id, anchor, url, user_id) FROM stdin;
\.


--
-- Data for Name: users_userprofile; Type: TABLE DATA; Schema: public; Owner: nepaday
--

COPY public.users_userprofile (id, avatar, patronymic, birthday, phone, address, about, id_signed_news, user_id) FROM stdin;
\.


--
-- Name: advertising_sliderhome_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.advertising_sliderhome_id_seq', 3, true);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 123, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 1, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: blog_blog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.blog_blog_id_seq', 1, true);


--
-- Name: blog_blog_tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.blog_blog_tags_id_seq', 4, true);


--
-- Name: blog_blogimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.blog_blogimage_id_seq', 1, true);


--
-- Name: blog_comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.blog_comment_id_seq', 3, true);


--
-- Name: blog_post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.blog_post_id_seq', 3, true);


--
-- Name: blog_post_tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.blog_post_tags_id_seq', 1, true);


--
-- Name: blog_postimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.blog_postimage_id_seq', 3, true);


--
-- Name: catalog_catalog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.catalog_catalog_id_seq', 12, true);


--
-- Name: catalog_catalog_tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.catalog_catalog_tags_id_seq', 4, true);


--
-- Name: catalog_catalogimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.catalog_catalogimage_id_seq', 3, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 103, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 41, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 42, true);


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.django_site_id_seq', 1, true);


--
-- Name: gallery_gallery_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.gallery_gallery_id_seq', 1, true);


--
-- Name: gallery_gallery_tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.gallery_gallery_tags_id_seq', 4, true);


--
-- Name: gallery_galleryimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.gallery_galleryimage_id_seq', 9, true);


--
-- Name: home_home_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.home_home_id_seq', 1, true);


--
-- Name: home_homeimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.home_homeimage_id_seq', 2, true);


--
-- Name: include_area_includearea_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.include_area_includearea_id_seq', 3, true);


--
-- Name: menu_mainmenu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.menu_mainmenu_id_seq', 24, true);


--
-- Name: order_order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.order_order_id_seq', 6, true);


--
-- Name: order_orderitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.order_orderitem_id_seq', 25, true);


--
-- Name: order_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.order_status_id_seq', 3, true);


--
-- Name: order_story_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.order_story_id_seq', 28, true);


--
-- Name: page_page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.page_page_id_seq', 3, true);


--
-- Name: page_page_tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.page_page_tags_id_seq', 6, true);


--
-- Name: page_pagecomment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.page_pagecomment_id_seq', 1, false);


--
-- Name: page_pageimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.page_pageimage_id_seq', 1, true);


--
-- Name: product_product_catalog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.product_product_catalog_id_seq', 5, true);


--
-- Name: product_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.product_product_id_seq', 12, true);


--
-- Name: product_product_recommend_products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.product_product_recommend_products_id_seq', 3, true);


--
-- Name: product_product_tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.product_product_tags_id_seq', 1, false);


--
-- Name: product_productcomment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.product_productcomment_id_seq', 8, true);


--
-- Name: product_productimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.product_productimage_id_seq', 24, true);


--
-- Name: product_productitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.product_productitem_id_seq', 29, true);


--
-- Name: settings_template_footer_blogs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.settings_template_footer_blogs_id_seq', 1, false);


--
-- Name: settings_template_footer_catalog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.settings_template_footer_catalog_id_seq', 9, true);


--
-- Name: settings_template_footer_galleries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.settings_template_footer_galleries_id_seq', 1, false);


--
-- Name: settings_template_footer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.settings_template_footer_id_seq', 1, true);


--
-- Name: settings_template_footer_list_link_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.settings_template_footer_list_link_id_seq', 1, false);


--
-- Name: settings_template_footer_page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.settings_template_footer_page_id_seq', 6, true);


--
-- Name: settings_template_settingstemplate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.settings_template_settingstemplate_id_seq', 1, true);


--
-- Name: site_info_listlink_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.site_info_listlink_id_seq', 1, true);


--
-- Name: site_info_socialnetwork_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.site_info_socialnetwork_id_seq', 2, true);


--
-- Name: site_info_tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.site_info_tag_id_seq', 7, true);


--
-- Name: site_info_textinfo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.site_info_textinfo_id_seq', 1, true);


--
-- Name: users_userlink_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.users_userlink_id_seq', 1, false);


--
-- Name: users_userprofile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nepaday
--

SELECT pg_catalog.setval('public.users_userprofile_id_seq', 1, false);


--
-- Name: advertising_sliderhome advertising_sliderhome_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.advertising_sliderhome
    ADD CONSTRAINT advertising_sliderhome_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: blog_blog blog_blog_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_blog
    ADD CONSTRAINT blog_blog_pkey PRIMARY KEY (id);


--
-- Name: blog_blog_tags blog_blog_tags_blog_id_tag_id_211c6be5_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_blog_tags
    ADD CONSTRAINT blog_blog_tags_blog_id_tag_id_211c6be5_uniq UNIQUE (blog_id, tag_id);


--
-- Name: blog_blog_tags blog_blog_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_blog_tags
    ADD CONSTRAINT blog_blog_tags_pkey PRIMARY KEY (id);


--
-- Name: blog_blog blog_blog_title_parent_id_slug_581a794f_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_blog
    ADD CONSTRAINT blog_blog_title_parent_id_slug_581a794f_uniq UNIQUE (title, parent_id, slug);


--
-- Name: blog_blogimage blog_blogimage_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_blogimage
    ADD CONSTRAINT blog_blogimage_pkey PRIMARY KEY (id);


--
-- Name: blog_comment blog_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_comment
    ADD CONSTRAINT blog_comment_pkey PRIMARY KEY (id);


--
-- Name: blog_post blog_post_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_post
    ADD CONSTRAINT blog_post_pkey PRIMARY KEY (id);


--
-- Name: blog_post_tags blog_post_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_post_tags
    ADD CONSTRAINT blog_post_tags_pkey PRIMARY KEY (id);


--
-- Name: blog_post_tags blog_post_tags_post_id_tag_id_4925ec37_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_post_tags
    ADD CONSTRAINT blog_post_tags_post_id_tag_id_4925ec37_uniq UNIQUE (post_id, tag_id);


--
-- Name: blog_postimage blog_postimage_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_postimage
    ADD CONSTRAINT blog_postimage_pkey PRIMARY KEY (id);


--
-- Name: catalog_catalog catalog_catalog_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.catalog_catalog
    ADD CONSTRAINT catalog_catalog_pkey PRIMARY KEY (id);


--
-- Name: catalog_catalog_tags catalog_catalog_tags_catalog_id_tag_id_231057c6_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.catalog_catalog_tags
    ADD CONSTRAINT catalog_catalog_tags_catalog_id_tag_id_231057c6_uniq UNIQUE (catalog_id, tag_id);


--
-- Name: catalog_catalog_tags catalog_catalog_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.catalog_catalog_tags
    ADD CONSTRAINT catalog_catalog_tags_pkey PRIMARY KEY (id);


--
-- Name: catalog_catalog catalog_catalog_title_parent_id_slug_03b445d7_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.catalog_catalog
    ADD CONSTRAINT catalog_catalog_title_parent_id_slug_03b445d7_uniq UNIQUE (title, parent_id, slug);


--
-- Name: catalog_catalogimage catalog_catalogimage_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.catalog_catalogimage
    ADD CONSTRAINT catalog_catalogimage_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site django_site_domain_a2e37b91_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_domain_a2e37b91_uniq UNIQUE (domain);


--
-- Name: django_site django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: gallery_gallery gallery_gallery_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.gallery_gallery
    ADD CONSTRAINT gallery_gallery_pkey PRIMARY KEY (id);


--
-- Name: gallery_gallery_tags gallery_gallery_tags_gallery_id_tag_id_97c422eb_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.gallery_gallery_tags
    ADD CONSTRAINT gallery_gallery_tags_gallery_id_tag_id_97c422eb_uniq UNIQUE (gallery_id, tag_id);


--
-- Name: gallery_gallery_tags gallery_gallery_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.gallery_gallery_tags
    ADD CONSTRAINT gallery_gallery_tags_pkey PRIMARY KEY (id);


--
-- Name: gallery_gallery gallery_gallery_title_parent_id_slug_4948e644_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.gallery_gallery
    ADD CONSTRAINT gallery_gallery_title_parent_id_slug_4948e644_uniq UNIQUE (title, parent_id, slug);


--
-- Name: gallery_galleryimage gallery_galleryimage_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.gallery_galleryimage
    ADD CONSTRAINT gallery_galleryimage_pkey PRIMARY KEY (id);


--
-- Name: home_home home_home_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.home_home
    ADD CONSTRAINT home_home_pkey PRIMARY KEY (id);


--
-- Name: home_home home_home_title_key; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.home_home
    ADD CONSTRAINT home_home_title_key UNIQUE (title);


--
-- Name: home_homeimage home_homeimage_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.home_homeimage
    ADD CONSTRAINT home_homeimage_pkey PRIMARY KEY (id);


--
-- Name: include_area_includearea include_area_includearea_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.include_area_includearea
    ADD CONSTRAINT include_area_includearea_pkey PRIMARY KEY (id);


--
-- Name: include_area_includearea include_area_includearea_title_code_03a59a42_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.include_area_includearea
    ADD CONSTRAINT include_area_includearea_title_code_03a59a42_uniq UNIQUE (title, code);


--
-- Name: menu_mainmenu menu_mainmenu_name_parent_id_is_show_d30070c3_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.menu_mainmenu
    ADD CONSTRAINT menu_mainmenu_name_parent_id_is_show_d30070c3_uniq UNIQUE (name, parent_id, is_show);


--
-- Name: menu_mainmenu menu_mainmenu_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.menu_mainmenu
    ADD CONSTRAINT menu_mainmenu_pkey PRIMARY KEY (id);


--
-- Name: order_order order_order_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.order_order
    ADD CONSTRAINT order_order_pkey PRIMARY KEY (id);


--
-- Name: order_orderitem order_orderitem_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.order_orderitem
    ADD CONSTRAINT order_orderitem_pkey PRIMARY KEY (id);


--
-- Name: order_status order_status_name_key; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.order_status
    ADD CONSTRAINT order_status_name_key UNIQUE (name);


--
-- Name: order_status order_status_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.order_status
    ADD CONSTRAINT order_status_pkey PRIMARY KEY (id);


--
-- Name: order_story order_story_order_id_status_id_total_cost_6c166f53_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.order_story
    ADD CONSTRAINT order_story_order_id_status_id_total_cost_6c166f53_uniq UNIQUE (order_id, status_id, total_cost);


--
-- Name: order_story order_story_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.order_story
    ADD CONSTRAINT order_story_pkey PRIMARY KEY (id);


--
-- Name: page_page page_page_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.page_page
    ADD CONSTRAINT page_page_pkey PRIMARY KEY (id);


--
-- Name: page_page page_page_slug_created_a4ecd142_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.page_page
    ADD CONSTRAINT page_page_slug_created_a4ecd142_uniq UNIQUE (slug, created);


--
-- Name: page_page_tags page_page_tags_page_id_tag_id_97cd439d_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.page_page_tags
    ADD CONSTRAINT page_page_tags_page_id_tag_id_97cd439d_uniq UNIQUE (page_id, tag_id);


--
-- Name: page_page_tags page_page_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.page_page_tags
    ADD CONSTRAINT page_page_tags_pkey PRIMARY KEY (id);


--
-- Name: page_pagecomment page_pagecomment_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.page_pagecomment
    ADD CONSTRAINT page_pagecomment_pkey PRIMARY KEY (id);


--
-- Name: page_pageimage page_pageimage_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.page_pageimage
    ADD CONSTRAINT page_pageimage_pkey PRIMARY KEY (id);


--
-- Name: product_product_catalog product_product_catalog_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product_catalog
    ADD CONSTRAINT product_product_catalog_pkey PRIMARY KEY (id);


--
-- Name: product_product_catalog product_product_catalog_product_id_catalog_id_e156d1b4_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product_catalog
    ADD CONSTRAINT product_product_catalog_product_id_catalog_id_e156d1b4_uniq UNIQUE (product_id, catalog_id);


--
-- Name: product_product product_product_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product
    ADD CONSTRAINT product_product_pkey PRIMARY KEY (id);


--
-- Name: product_product_recommend_products product_product_recommen_from_product_id_to_produ_881dcca6_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product_recommend_products
    ADD CONSTRAINT product_product_recommen_from_product_id_to_produ_881dcca6_uniq UNIQUE (from_product_id, to_product_id);


--
-- Name: product_product_recommend_products product_product_recommend_products_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product_recommend_products
    ADD CONSTRAINT product_product_recommend_products_pkey PRIMARY KEY (id);


--
-- Name: product_product_tags product_product_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product_tags
    ADD CONSTRAINT product_product_tags_pkey PRIMARY KEY (id);


--
-- Name: product_product_tags product_product_tags_product_id_tag_id_f9d19e54_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product_tags
    ADD CONSTRAINT product_product_tags_product_id_tag_id_f9d19e54_uniq UNIQUE (product_id, tag_id);


--
-- Name: product_product product_product_title_slug_1dc8429a_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product
    ADD CONSTRAINT product_product_title_slug_1dc8429a_uniq UNIQUE (title, slug);


--
-- Name: product_productcomment product_productcomment_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_productcomment
    ADD CONSTRAINT product_productcomment_pkey PRIMARY KEY (id);


--
-- Name: product_productimage product_productimage_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_productimage
    ADD CONSTRAINT product_productimage_pkey PRIMARY KEY (id);


--
-- Name: product_productitem product_productitem_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_productitem
    ADD CONSTRAINT product_productitem_pkey PRIMARY KEY (id);


--
-- Name: settings_template_footer_blogs settings_template_footer_blogs_footer_id_blog_id_180aa4d1_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_blogs
    ADD CONSTRAINT settings_template_footer_blogs_footer_id_blog_id_180aa4d1_uniq UNIQUE (footer_id, blog_id);


--
-- Name: settings_template_footer_blogs settings_template_footer_blogs_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_blogs
    ADD CONSTRAINT settings_template_footer_blogs_pkey PRIMARY KEY (id);


--
-- Name: settings_template_footer_catalog settings_template_footer_catalog_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_catalog
    ADD CONSTRAINT settings_template_footer_catalog_pkey PRIMARY KEY (id);


--
-- Name: settings_template_footer_catalog settings_template_footer_footer_id_catalog_id_59a33efe_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_catalog
    ADD CONSTRAINT settings_template_footer_footer_id_catalog_id_59a33efe_uniq UNIQUE (footer_id, catalog_id);


--
-- Name: settings_template_footer_galleries settings_template_footer_footer_id_gallery_id_5431ae84_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_galleries
    ADD CONSTRAINT settings_template_footer_footer_id_gallery_id_5431ae84_uniq UNIQUE (footer_id, gallery_id);


--
-- Name: settings_template_footer_list_link settings_template_footer_footer_id_listlink_id_e083fab1_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_list_link
    ADD CONSTRAINT settings_template_footer_footer_id_listlink_id_e083fab1_uniq UNIQUE (footer_id, listlink_id);


--
-- Name: settings_template_footer_galleries settings_template_footer_galleries_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_galleries
    ADD CONSTRAINT settings_template_footer_galleries_pkey PRIMARY KEY (id);


--
-- Name: settings_template_footer_list_link settings_template_footer_list_link_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_list_link
    ADD CONSTRAINT settings_template_footer_list_link_pkey PRIMARY KEY (id);


--
-- Name: settings_template_footer_page settings_template_footer_page_footer_id_page_id_4c96a8bf_uniq; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_page
    ADD CONSTRAINT settings_template_footer_page_footer_id_page_id_4c96a8bf_uniq UNIQUE (footer_id, page_id);


--
-- Name: settings_template_footer_page settings_template_footer_page_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_page
    ADD CONSTRAINT settings_template_footer_page_pkey PRIMARY KEY (id);


--
-- Name: settings_template_footer settings_template_footer_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer
    ADD CONSTRAINT settings_template_footer_pkey PRIMARY KEY (id);


--
-- Name: settings_template_footer settings_template_footer_title_key; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer
    ADD CONSTRAINT settings_template_footer_title_key UNIQUE (title);


--
-- Name: settings_template_settingstemplate settings_template_settingstemplate_phone_key; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_settingstemplate
    ADD CONSTRAINT settings_template_settingstemplate_phone_key UNIQUE (phone);


--
-- Name: settings_template_settingstemplate settings_template_settingstemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_settingstemplate
    ADD CONSTRAINT settings_template_settingstemplate_pkey PRIMARY KEY (id);


--
-- Name: settings_template_settingstemplate settings_template_settingstemplate_site_id_key; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_settingstemplate
    ADD CONSTRAINT settings_template_settingstemplate_site_id_key UNIQUE (site_id);


--
-- Name: settings_template_settingstemplate settings_template_settingstemplate_title_key; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_settingstemplate
    ADD CONSTRAINT settings_template_settingstemplate_title_key UNIQUE (title);


--
-- Name: site_info_listlink site_info_listlink_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.site_info_listlink
    ADD CONSTRAINT site_info_listlink_pkey PRIMARY KEY (id);


--
-- Name: site_info_socialnetwork site_info_socialnetwork_html_link_key; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.site_info_socialnetwork
    ADD CONSTRAINT site_info_socialnetwork_html_link_key UNIQUE (html_link);


--
-- Name: site_info_socialnetwork site_info_socialnetwork_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.site_info_socialnetwork
    ADD CONSTRAINT site_info_socialnetwork_pkey PRIMARY KEY (id);


--
-- Name: site_info_tag site_info_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.site_info_tag
    ADD CONSTRAINT site_info_tag_pkey PRIMARY KEY (id);


--
-- Name: site_info_textinfo site_info_textinfo_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.site_info_textinfo
    ADD CONSTRAINT site_info_textinfo_pkey PRIMARY KEY (id);


--
-- Name: site_info_textinfo site_info_textinfo_title_key; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.site_info_textinfo
    ADD CONSTRAINT site_info_textinfo_title_key UNIQUE (title);


--
-- Name: users_userlink users_userlink_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.users_userlink
    ADD CONSTRAINT users_userlink_pkey PRIMARY KEY (id);


--
-- Name: users_userprofile users_userprofile_pkey; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.users_userprofile
    ADD CONSTRAINT users_userprofile_pkey PRIMARY KEY (id);


--
-- Name: users_userprofile users_userprofile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.users_userprofile
    ADD CONSTRAINT users_userprofile_user_id_key UNIQUE (user_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: blog_blog_author_id_8791af69; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_blog_author_id_8791af69 ON public.blog_blog USING btree (author_id);


--
-- Name: blog_blog_level_33dfff40; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_blog_level_33dfff40 ON public.blog_blog USING btree (level);


--
-- Name: blog_blog_lft_5b58ade6; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_blog_lft_5b58ade6 ON public.blog_blog USING btree (lft);


--
-- Name: blog_blog_parent_id_2195dcd3; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_blog_parent_id_2195dcd3 ON public.blog_blog USING btree (parent_id);


--
-- Name: blog_blog_rght_0b0e6ca6; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_blog_rght_0b0e6ca6 ON public.blog_blog USING btree (rght);


--
-- Name: blog_blog_slug_4812aa2c; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_blog_slug_4812aa2c ON public.blog_blog USING btree (slug);


--
-- Name: blog_blog_slug_4812aa2c_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_blog_slug_4812aa2c_like ON public.blog_blog USING btree (slug varchar_pattern_ops);


--
-- Name: blog_blog_tags_blog_id_e4cd5f6a; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_blog_tags_blog_id_e4cd5f6a ON public.blog_blog_tags USING btree (blog_id);


--
-- Name: blog_blog_tags_tag_id_36a3abc6; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_blog_tags_tag_id_36a3abc6 ON public.blog_blog_tags USING btree (tag_id);


--
-- Name: blog_blog_title_942d8a1e; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_blog_title_942d8a1e ON public.blog_blog USING btree (title);


--
-- Name: blog_blog_title_942d8a1e_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_blog_title_942d8a1e_like ON public.blog_blog USING btree (title varchar_pattern_ops);


--
-- Name: blog_blog_tree_id_0a4b3027; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_blog_tree_id_0a4b3027 ON public.blog_blog USING btree (tree_id);


--
-- Name: blog_blogimage_blog_id_17a42bec; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_blogimage_blog_id_17a42bec ON public.blog_blogimage USING btree (blog_id);


--
-- Name: blog_comment_post_id_580e96ef; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_comment_post_id_580e96ef ON public.blog_comment USING btree (post_id);


--
-- Name: blog_comment_user_id_59a54155; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_comment_user_id_59a54155 ON public.blog_comment USING btree (user_id);


--
-- Name: blog_post_author_id_dd7a8485; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_post_author_id_dd7a8485 ON public.blog_post USING btree (author_id);


--
-- Name: blog_post_blog_id_1d63f737; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_post_blog_id_1d63f737 ON public.blog_post USING btree (blog_id);


--
-- Name: blog_post_slug_b95473f2; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_post_slug_b95473f2 ON public.blog_post USING btree (slug);


--
-- Name: blog_post_slug_b95473f2_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_post_slug_b95473f2_like ON public.blog_post USING btree (slug varchar_pattern_ops);


--
-- Name: blog_post_tags_post_id_a1c71c8a; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_post_tags_post_id_a1c71c8a ON public.blog_post_tags USING btree (post_id);


--
-- Name: blog_post_tags_tag_id_0875c551; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_post_tags_tag_id_0875c551 ON public.blog_post_tags USING btree (tag_id);


--
-- Name: blog_post_title_adf2f203; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_post_title_adf2f203 ON public.blog_post USING btree (title);


--
-- Name: blog_post_title_adf2f203_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_post_title_adf2f203_like ON public.blog_post USING btree (title varchar_pattern_ops);


--
-- Name: blog_postimage_post_id_09cc1915; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX blog_postimage_post_id_09cc1915 ON public.blog_postimage USING btree (post_id);


--
-- Name: catalog_catalog_author_id_73764c6e; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX catalog_catalog_author_id_73764c6e ON public.catalog_catalog USING btree (author_id);


--
-- Name: catalog_catalog_level_88531b31; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX catalog_catalog_level_88531b31 ON public.catalog_catalog USING btree (level);


--
-- Name: catalog_catalog_lft_b0780e88; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX catalog_catalog_lft_b0780e88 ON public.catalog_catalog USING btree (lft);


--
-- Name: catalog_catalog_parent_id_fbb74747; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX catalog_catalog_parent_id_fbb74747 ON public.catalog_catalog USING btree (parent_id);


--
-- Name: catalog_catalog_rght_0cb95583; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX catalog_catalog_rght_0cb95583 ON public.catalog_catalog USING btree (rght);


--
-- Name: catalog_catalog_slug_b54675e0; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX catalog_catalog_slug_b54675e0 ON public.catalog_catalog USING btree (slug);


--
-- Name: catalog_catalog_slug_b54675e0_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX catalog_catalog_slug_b54675e0_like ON public.catalog_catalog USING btree (slug varchar_pattern_ops);


--
-- Name: catalog_catalog_tags_catalog_id_9bb9664f; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX catalog_catalog_tags_catalog_id_9bb9664f ON public.catalog_catalog_tags USING btree (catalog_id);


--
-- Name: catalog_catalog_tags_tag_id_6e6597cb; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX catalog_catalog_tags_tag_id_6e6597cb ON public.catalog_catalog_tags USING btree (tag_id);


--
-- Name: catalog_catalog_title_af2f356b; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX catalog_catalog_title_af2f356b ON public.catalog_catalog USING btree (title);


--
-- Name: catalog_catalog_title_af2f356b_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX catalog_catalog_title_af2f356b_like ON public.catalog_catalog USING btree (title varchar_pattern_ops);


--
-- Name: catalog_catalog_tree_id_6c61173c; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX catalog_catalog_tree_id_6c61173c ON public.catalog_catalog USING btree (tree_id);


--
-- Name: catalog_catalogimage_catalog_id_8c434043; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX catalog_catalogimage_catalog_id_8c434043 ON public.catalog_catalogimage USING btree (catalog_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: django_site_domain_a2e37b91_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX django_site_domain_a2e37b91_like ON public.django_site USING btree (domain varchar_pattern_ops);


--
-- Name: gallery_gallery_author_id_0e1b2c34; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX gallery_gallery_author_id_0e1b2c34 ON public.gallery_gallery USING btree (author_id);


--
-- Name: gallery_gallery_level_aaaf9a3d; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX gallery_gallery_level_aaaf9a3d ON public.gallery_gallery USING btree (level);


--
-- Name: gallery_gallery_lft_d9a9ff1a; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX gallery_gallery_lft_d9a9ff1a ON public.gallery_gallery USING btree (lft);


--
-- Name: gallery_gallery_parent_id_641f5ece; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX gallery_gallery_parent_id_641f5ece ON public.gallery_gallery USING btree (parent_id);


--
-- Name: gallery_gallery_rght_e74ab111; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX gallery_gallery_rght_e74ab111 ON public.gallery_gallery USING btree (rght);


--
-- Name: gallery_gallery_slug_7f00eb51; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX gallery_gallery_slug_7f00eb51 ON public.gallery_gallery USING btree (slug);


--
-- Name: gallery_gallery_slug_7f00eb51_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX gallery_gallery_slug_7f00eb51_like ON public.gallery_gallery USING btree (slug varchar_pattern_ops);


--
-- Name: gallery_gallery_tags_gallery_id_489c279c; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX gallery_gallery_tags_gallery_id_489c279c ON public.gallery_gallery_tags USING btree (gallery_id);


--
-- Name: gallery_gallery_tags_tag_id_b66bc31b; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX gallery_gallery_tags_tag_id_b66bc31b ON public.gallery_gallery_tags USING btree (tag_id);


--
-- Name: gallery_gallery_title_7efb69e4; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX gallery_gallery_title_7efb69e4 ON public.gallery_gallery USING btree (title);


--
-- Name: gallery_gallery_title_7efb69e4_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX gallery_gallery_title_7efb69e4_like ON public.gallery_gallery USING btree (title varchar_pattern_ops);


--
-- Name: gallery_gallery_tree_id_5cfb0bb2; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX gallery_gallery_tree_id_5cfb0bb2 ON public.gallery_gallery USING btree (tree_id);


--
-- Name: gallery_galleryimage_gallery_id_ad9f071e; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX gallery_galleryimage_gallery_id_ad9f071e ON public.gallery_galleryimage USING btree (gallery_id);


--
-- Name: home_home_blog_id_09dbd01d; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX home_home_blog_id_09dbd01d ON public.home_home USING btree (blog_id);


--
-- Name: home_home_slug_3c37aa8c; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX home_home_slug_3c37aa8c ON public.home_home USING btree (slug);


--
-- Name: home_home_slug_3c37aa8c_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX home_home_slug_3c37aa8c_like ON public.home_home USING btree (slug varchar_pattern_ops);


--
-- Name: home_home_title_9a3901d3_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX home_home_title_9a3901d3_like ON public.home_home USING btree (title varchar_pattern_ops);


--
-- Name: home_homeimage_home_id_930c6893; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX home_homeimage_home_id_930c6893 ON public.home_homeimage USING btree (home_id);


--
-- Name: menu_mainmenu_blog_id_392bfbf5; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX menu_mainmenu_blog_id_392bfbf5 ON public.menu_mainmenu USING btree (blog_id);


--
-- Name: menu_mainmenu_catalog_id_68f3bf4e; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX menu_mainmenu_catalog_id_68f3bf4e ON public.menu_mainmenu USING btree (catalog_id);


--
-- Name: menu_mainmenu_gallery_id_3c0f0648; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX menu_mainmenu_gallery_id_3c0f0648 ON public.menu_mainmenu USING btree (gallery_id);


--
-- Name: menu_mainmenu_level_9d46a0e7; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX menu_mainmenu_level_9d46a0e7 ON public.menu_mainmenu USING btree (level);


--
-- Name: menu_mainmenu_lft_55f0837f; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX menu_mainmenu_lft_55f0837f ON public.menu_mainmenu USING btree (lft);


--
-- Name: menu_mainmenu_name_89d3c302; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX menu_mainmenu_name_89d3c302 ON public.menu_mainmenu USING btree (name);


--
-- Name: menu_mainmenu_name_89d3c302_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX menu_mainmenu_name_89d3c302_like ON public.menu_mainmenu USING btree (name varchar_pattern_ops);


--
-- Name: menu_mainmenu_page_id_802be0b8; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX menu_mainmenu_page_id_802be0b8 ON public.menu_mainmenu USING btree (page_id);


--
-- Name: menu_mainmenu_parent_id_195402d7; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX menu_mainmenu_parent_id_195402d7 ON public.menu_mainmenu USING btree (parent_id);


--
-- Name: menu_mainmenu_rght_fde3f865; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX menu_mainmenu_rght_fde3f865 ON public.menu_mainmenu USING btree (rght);


--
-- Name: menu_mainmenu_tree_id_2a8d96f2; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX menu_mainmenu_tree_id_2a8d96f2 ON public.menu_mainmenu USING btree (tree_id);


--
-- Name: order_order_status_id_ec745f82; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX order_order_status_id_ec745f82 ON public.order_order USING btree (status_id);


--
-- Name: order_order_user_id_7cf9bc2b; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX order_order_user_id_7cf9bc2b ON public.order_order USING btree (user_id);


--
-- Name: order_orderitem_order_id_aba34f44; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX order_orderitem_order_id_aba34f44 ON public.order_orderitem USING btree (order_id);


--
-- Name: order_orderitem_product_item_id_0c1cac4f; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX order_orderitem_product_item_id_0c1cac4f ON public.order_orderitem USING btree (product_item_id);


--
-- Name: order_status_name_ff6ff8be_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX order_status_name_ff6ff8be_like ON public.order_status USING btree (name varchar_pattern_ops);


--
-- Name: order_story_order_id_f0d84c4a; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX order_story_order_id_f0d84c4a ON public.order_story USING btree (order_id);


--
-- Name: order_story_status_id_72564f64; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX order_story_status_id_72564f64 ON public.order_story USING btree (status_id);


--
-- Name: page_page_author_id_d62cc760; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX page_page_author_id_d62cc760 ON public.page_page USING btree (author_id);


--
-- Name: page_page_slug_d6b7c8ed; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX page_page_slug_d6b7c8ed ON public.page_page USING btree (slug);


--
-- Name: page_page_slug_d6b7c8ed_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX page_page_slug_d6b7c8ed_like ON public.page_page USING btree (slug varchar_pattern_ops);


--
-- Name: page_page_tags_page_id_6d99ef76; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX page_page_tags_page_id_6d99ef76 ON public.page_page_tags USING btree (page_id);


--
-- Name: page_page_tags_tag_id_438e06c3; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX page_page_tags_tag_id_438e06c3 ON public.page_page_tags USING btree (tag_id);


--
-- Name: page_page_title_f3e79603; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX page_page_title_f3e79603 ON public.page_page USING btree (title);


--
-- Name: page_page_title_f3e79603_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX page_page_title_f3e79603_like ON public.page_page USING btree (title varchar_pattern_ops);


--
-- Name: page_pagecomment_page_id_070504d5; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX page_pagecomment_page_id_070504d5 ON public.page_pagecomment USING btree (page_id);


--
-- Name: page_pagecomment_user_id_9a4af13a; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX page_pagecomment_user_id_9a4af13a ON public.page_pagecomment USING btree (user_id);


--
-- Name: page_pageimage_page_id_26df7250; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX page_pageimage_page_id_26df7250 ON public.page_pageimage USING btree (page_id);


--
-- Name: product_product_articul_d058d835; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_product_articul_d058d835 ON public.product_product USING btree (articul);


--
-- Name: product_product_articul_d058d835_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_product_articul_d058d835_like ON public.product_product USING btree (articul varchar_pattern_ops);


--
-- Name: product_product_author_id_9becb979; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_product_author_id_9becb979 ON public.product_product USING btree (author_id);


--
-- Name: product_product_catalog_catalog_id_28182fb8; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_product_catalog_catalog_id_28182fb8 ON public.product_product_catalog USING btree (catalog_id);


--
-- Name: product_product_catalog_product_id_2bb2e50d; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_product_catalog_product_id_2bb2e50d ON public.product_product_catalog USING btree (product_id);


--
-- Name: product_product_level_0cb0a2e0; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_product_level_0cb0a2e0 ON public.product_product USING btree (level);


--
-- Name: product_product_lft_b0b168fa; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_product_lft_b0b168fa ON public.product_product USING btree (lft);


--
-- Name: product_product_parent_id_01fef757; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_product_parent_id_01fef757 ON public.product_product USING btree (parent_id);


--
-- Name: product_product_recommend_products_from_product_id_f4fd92d7; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_product_recommend_products_from_product_id_f4fd92d7 ON public.product_product_recommend_products USING btree (from_product_id);


--
-- Name: product_product_recommend_products_to_product_id_38eb3ce3; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_product_recommend_products_to_product_id_38eb3ce3 ON public.product_product_recommend_products USING btree (to_product_id);


--
-- Name: product_product_rght_d2e69de9; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_product_rght_d2e69de9 ON public.product_product USING btree (rght);


--
-- Name: product_product_slug_76cde0ae; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_product_slug_76cde0ae ON public.product_product USING btree (slug);


--
-- Name: product_product_slug_76cde0ae_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_product_slug_76cde0ae_like ON public.product_product USING btree (slug varchar_pattern_ops);


--
-- Name: product_product_tags_product_id_a72c644e; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_product_tags_product_id_a72c644e ON public.product_product_tags USING btree (product_id);


--
-- Name: product_product_tags_tag_id_2e44bb40; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_product_tags_tag_id_2e44bb40 ON public.product_product_tags USING btree (tag_id);


--
-- Name: product_product_title_0434d32d; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_product_title_0434d32d ON public.product_product USING btree (title);


--
-- Name: product_product_title_0434d32d_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_product_title_0434d32d_like ON public.product_product USING btree (title varchar_pattern_ops);


--
-- Name: product_product_tree_id_91aa9c73; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_product_tree_id_91aa9c73 ON public.product_product USING btree (tree_id);


--
-- Name: product_productcomment_product_id_bf5e6965; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_productcomment_product_id_bf5e6965 ON public.product_productcomment USING btree (product_id);


--
-- Name: product_productcomment_user_id_e1a22ad0; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_productcomment_user_id_e1a22ad0 ON public.product_productcomment USING btree (user_id);


--
-- Name: product_productimage_product_id_544084bb; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_productimage_product_id_544084bb ON public.product_productimage USING btree (product_id);


--
-- Name: product_productitem_articul_b01fcac5; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_productitem_articul_b01fcac5 ON public.product_productitem USING btree (articul);


--
-- Name: product_productitem_articul_b01fcac5_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_productitem_articul_b01fcac5_like ON public.product_productitem USING btree (articul varchar_pattern_ops);


--
-- Name: product_productitem_from_qty_b0160935; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_productitem_from_qty_b0160935 ON public.product_productitem USING btree (from_qty);


--
-- Name: product_productitem_name_2ed60333; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_productitem_name_2ed60333 ON public.product_productitem USING btree (name);


--
-- Name: product_productitem_name_2ed60333_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_productitem_name_2ed60333_like ON public.product_productitem USING btree (name varchar_pattern_ops);


--
-- Name: product_productitem_product_id_16cb97c8; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX product_productitem_product_id_16cb97c8 ON public.product_productitem USING btree (product_id);


--
-- Name: settings_template_footer_blogs_blog_id_1531bf6f; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX settings_template_footer_blogs_blog_id_1531bf6f ON public.settings_template_footer_blogs USING btree (blog_id);


--
-- Name: settings_template_footer_blogs_footer_id_defb5a64; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX settings_template_footer_blogs_footer_id_defb5a64 ON public.settings_template_footer_blogs USING btree (footer_id);


--
-- Name: settings_template_footer_catalog_catalog_id_a6917c88; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX settings_template_footer_catalog_catalog_id_a6917c88 ON public.settings_template_footer_catalog USING btree (catalog_id);


--
-- Name: settings_template_footer_catalog_footer_id_129c20b7; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX settings_template_footer_catalog_footer_id_129c20b7 ON public.settings_template_footer_catalog USING btree (footer_id);


--
-- Name: settings_template_footer_galleries_footer_id_f9131e21; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX settings_template_footer_galleries_footer_id_f9131e21 ON public.settings_template_footer_galleries USING btree (footer_id);


--
-- Name: settings_template_footer_galleries_gallery_id_79e8c708; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX settings_template_footer_galleries_gallery_id_79e8c708 ON public.settings_template_footer_galleries USING btree (gallery_id);


--
-- Name: settings_template_footer_list_link_footer_id_79de261f; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX settings_template_footer_list_link_footer_id_79de261f ON public.settings_template_footer_list_link USING btree (footer_id);


--
-- Name: settings_template_footer_list_link_listlink_id_9240aede; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX settings_template_footer_list_link_listlink_id_9240aede ON public.settings_template_footer_list_link USING btree (listlink_id);


--
-- Name: settings_template_footer_page_footer_id_2e9b2f7f; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX settings_template_footer_page_footer_id_2e9b2f7f ON public.settings_template_footer_page USING btree (footer_id);


--
-- Name: settings_template_footer_page_page_id_75c91e8d; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX settings_template_footer_page_page_id_75c91e8d ON public.settings_template_footer_page USING btree (page_id);


--
-- Name: settings_template_footer_text_info_id_d14ebd5e; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX settings_template_footer_text_info_id_d14ebd5e ON public.settings_template_footer USING btree (text_info_id);


--
-- Name: settings_template_footer_title_7e9adf32_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX settings_template_footer_title_7e9adf32_like ON public.settings_template_footer USING btree (title varchar_pattern_ops);


--
-- Name: settings_template_settingstemplate_footer_id_5ddf2bbc; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX settings_template_settingstemplate_footer_id_5ddf2bbc ON public.settings_template_settingstemplate USING btree (footer_id);


--
-- Name: settings_template_settingstemplate_home_id_661e3df9; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX settings_template_settingstemplate_home_id_661e3df9 ON public.settings_template_settingstemplate USING btree (home_id);


--
-- Name: settings_template_settingstemplate_phone_4913118d_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX settings_template_settingstemplate_phone_4913118d_like ON public.settings_template_settingstemplate USING btree (phone varchar_pattern_ops);


--
-- Name: settings_template_settingstemplate_title_26e31781_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX settings_template_settingstemplate_title_26e31781_like ON public.settings_template_settingstemplate USING btree (title varchar_pattern_ops);


--
-- Name: site_info_listlink_sort_a165f28a; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX site_info_listlink_sort_a165f28a ON public.site_info_listlink USING btree (sort);


--
-- Name: site_info_listlink_title_0c1ab567; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX site_info_listlink_title_0c1ab567 ON public.site_info_listlink USING btree (title);


--
-- Name: site_info_listlink_title_0c1ab567_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX site_info_listlink_title_0c1ab567_like ON public.site_info_listlink USING btree (title varchar_pattern_ops);


--
-- Name: site_info_listlink_url_63ed37f5; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX site_info_listlink_url_63ed37f5 ON public.site_info_listlink USING btree (url);


--
-- Name: site_info_listlink_url_63ed37f5_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX site_info_listlink_url_63ed37f5_like ON public.site_info_listlink USING btree (url varchar_pattern_ops);


--
-- Name: site_info_socialnetwork_html_link_a63e625b_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX site_info_socialnetwork_html_link_a63e625b_like ON public.site_info_socialnetwork USING btree (html_link varchar_pattern_ops);


--
-- Name: site_info_textinfo_title_8a7ec2d6_like; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX site_info_textinfo_title_8a7ec2d6_like ON public.site_info_textinfo USING btree (title varchar_pattern_ops);


--
-- Name: users_userlink_user_id_5730a6f4; Type: INDEX; Schema: public; Owner: nepaday
--

CREATE INDEX users_userlink_user_id_5730a6f4 ON public.users_userlink USING btree (user_id);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_blog blog_blog_author_id_8791af69_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_blog
    ADD CONSTRAINT blog_blog_author_id_8791af69_fk_auth_user_id FOREIGN KEY (author_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_blog blog_blog_parent_id_2195dcd3_fk_blog_blog_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_blog
    ADD CONSTRAINT blog_blog_parent_id_2195dcd3_fk_blog_blog_id FOREIGN KEY (parent_id) REFERENCES public.blog_blog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_blog_tags blog_blog_tags_blog_id_e4cd5f6a_fk_blog_blog_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_blog_tags
    ADD CONSTRAINT blog_blog_tags_blog_id_e4cd5f6a_fk_blog_blog_id FOREIGN KEY (blog_id) REFERENCES public.blog_blog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_blog_tags blog_blog_tags_tag_id_36a3abc6_fk_site_info_tag_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_blog_tags
    ADD CONSTRAINT blog_blog_tags_tag_id_36a3abc6_fk_site_info_tag_id FOREIGN KEY (tag_id) REFERENCES public.site_info_tag(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_blogimage blog_blogimage_blog_id_17a42bec_fk_blog_blog_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_blogimage
    ADD CONSTRAINT blog_blogimage_blog_id_17a42bec_fk_blog_blog_id FOREIGN KEY (blog_id) REFERENCES public.blog_blog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_comment blog_comment_post_id_580e96ef_fk_blog_post_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_comment
    ADD CONSTRAINT blog_comment_post_id_580e96ef_fk_blog_post_id FOREIGN KEY (post_id) REFERENCES public.blog_post(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_comment blog_comment_user_id_59a54155_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_comment
    ADD CONSTRAINT blog_comment_user_id_59a54155_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_post blog_post_author_id_dd7a8485_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_post
    ADD CONSTRAINT blog_post_author_id_dd7a8485_fk_auth_user_id FOREIGN KEY (author_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_post blog_post_blog_id_1d63f737_fk_blog_blog_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_post
    ADD CONSTRAINT blog_post_blog_id_1d63f737_fk_blog_blog_id FOREIGN KEY (blog_id) REFERENCES public.blog_blog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_post_tags blog_post_tags_post_id_a1c71c8a_fk_blog_post_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_post_tags
    ADD CONSTRAINT blog_post_tags_post_id_a1c71c8a_fk_blog_post_id FOREIGN KEY (post_id) REFERENCES public.blog_post(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_post_tags blog_post_tags_tag_id_0875c551_fk_site_info_tag_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_post_tags
    ADD CONSTRAINT blog_post_tags_tag_id_0875c551_fk_site_info_tag_id FOREIGN KEY (tag_id) REFERENCES public.site_info_tag(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_postimage blog_postimage_post_id_09cc1915_fk_blog_post_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.blog_postimage
    ADD CONSTRAINT blog_postimage_post_id_09cc1915_fk_blog_post_id FOREIGN KEY (post_id) REFERENCES public.blog_post(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: catalog_catalog catalog_catalog_author_id_73764c6e_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.catalog_catalog
    ADD CONSTRAINT catalog_catalog_author_id_73764c6e_fk_auth_user_id FOREIGN KEY (author_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: catalog_catalog catalog_catalog_parent_id_fbb74747_fk_catalog_catalog_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.catalog_catalog
    ADD CONSTRAINT catalog_catalog_parent_id_fbb74747_fk_catalog_catalog_id FOREIGN KEY (parent_id) REFERENCES public.catalog_catalog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: catalog_catalog_tags catalog_catalog_tags_catalog_id_9bb9664f_fk_catalog_catalog_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.catalog_catalog_tags
    ADD CONSTRAINT catalog_catalog_tags_catalog_id_9bb9664f_fk_catalog_catalog_id FOREIGN KEY (catalog_id) REFERENCES public.catalog_catalog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: catalog_catalog_tags catalog_catalog_tags_tag_id_6e6597cb_fk_site_info_tag_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.catalog_catalog_tags
    ADD CONSTRAINT catalog_catalog_tags_tag_id_6e6597cb_fk_site_info_tag_id FOREIGN KEY (tag_id) REFERENCES public.site_info_tag(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: catalog_catalogimage catalog_catalogimage_catalog_id_8c434043_fk_catalog_catalog_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.catalog_catalogimage
    ADD CONSTRAINT catalog_catalogimage_catalog_id_8c434043_fk_catalog_catalog_id FOREIGN KEY (catalog_id) REFERENCES public.catalog_catalog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gallery_gallery gallery_gallery_author_id_0e1b2c34_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.gallery_gallery
    ADD CONSTRAINT gallery_gallery_author_id_0e1b2c34_fk_auth_user_id FOREIGN KEY (author_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gallery_gallery gallery_gallery_parent_id_641f5ece_fk_gallery_gallery_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.gallery_gallery
    ADD CONSTRAINT gallery_gallery_parent_id_641f5ece_fk_gallery_gallery_id FOREIGN KEY (parent_id) REFERENCES public.gallery_gallery(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gallery_gallery_tags gallery_gallery_tags_gallery_id_489c279c_fk_gallery_gallery_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.gallery_gallery_tags
    ADD CONSTRAINT gallery_gallery_tags_gallery_id_489c279c_fk_gallery_gallery_id FOREIGN KEY (gallery_id) REFERENCES public.gallery_gallery(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gallery_gallery_tags gallery_gallery_tags_tag_id_b66bc31b_fk_site_info_tag_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.gallery_gallery_tags
    ADD CONSTRAINT gallery_gallery_tags_tag_id_b66bc31b_fk_site_info_tag_id FOREIGN KEY (tag_id) REFERENCES public.site_info_tag(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gallery_galleryimage gallery_galleryimage_gallery_id_ad9f071e_fk_gallery_gallery_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.gallery_galleryimage
    ADD CONSTRAINT gallery_galleryimage_gallery_id_ad9f071e_fk_gallery_gallery_id FOREIGN KEY (gallery_id) REFERENCES public.gallery_gallery(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: home_home home_home_blog_id_09dbd01d_fk_blog_blog_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.home_home
    ADD CONSTRAINT home_home_blog_id_09dbd01d_fk_blog_blog_id FOREIGN KEY (blog_id) REFERENCES public.blog_blog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: home_homeimage home_homeimage_home_id_930c6893_fk_home_home_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.home_homeimage
    ADD CONSTRAINT home_homeimage_home_id_930c6893_fk_home_home_id FOREIGN KEY (home_id) REFERENCES public.home_home(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: menu_mainmenu menu_mainmenu_blog_id_392bfbf5_fk_blog_blog_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.menu_mainmenu
    ADD CONSTRAINT menu_mainmenu_blog_id_392bfbf5_fk_blog_blog_id FOREIGN KEY (blog_id) REFERENCES public.blog_blog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: menu_mainmenu menu_mainmenu_catalog_id_68f3bf4e_fk_catalog_catalog_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.menu_mainmenu
    ADD CONSTRAINT menu_mainmenu_catalog_id_68f3bf4e_fk_catalog_catalog_id FOREIGN KEY (catalog_id) REFERENCES public.catalog_catalog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: menu_mainmenu menu_mainmenu_gallery_id_3c0f0648_fk_gallery_gallery_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.menu_mainmenu
    ADD CONSTRAINT menu_mainmenu_gallery_id_3c0f0648_fk_gallery_gallery_id FOREIGN KEY (gallery_id) REFERENCES public.gallery_gallery(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: menu_mainmenu menu_mainmenu_page_id_802be0b8_fk_page_page_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.menu_mainmenu
    ADD CONSTRAINT menu_mainmenu_page_id_802be0b8_fk_page_page_id FOREIGN KEY (page_id) REFERENCES public.page_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: menu_mainmenu menu_mainmenu_parent_id_195402d7_fk_menu_mainmenu_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.menu_mainmenu
    ADD CONSTRAINT menu_mainmenu_parent_id_195402d7_fk_menu_mainmenu_id FOREIGN KEY (parent_id) REFERENCES public.menu_mainmenu(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: order_order order_order_status_id_ec745f82_fk_order_status_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.order_order
    ADD CONSTRAINT order_order_status_id_ec745f82_fk_order_status_id FOREIGN KEY (status_id) REFERENCES public.order_status(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: order_order order_order_user_id_7cf9bc2b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.order_order
    ADD CONSTRAINT order_order_user_id_7cf9bc2b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: order_orderitem order_orderitem_order_id_aba34f44_fk_order_order_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.order_orderitem
    ADD CONSTRAINT order_orderitem_order_id_aba34f44_fk_order_order_id FOREIGN KEY (order_id) REFERENCES public.order_order(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: order_orderitem order_orderitem_product_item_id_0c1cac4f_fk_product_p; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.order_orderitem
    ADD CONSTRAINT order_orderitem_product_item_id_0c1cac4f_fk_product_p FOREIGN KEY (product_item_id) REFERENCES public.product_productitem(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: order_story order_story_order_id_f0d84c4a_fk_order_order_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.order_story
    ADD CONSTRAINT order_story_order_id_f0d84c4a_fk_order_order_id FOREIGN KEY (order_id) REFERENCES public.order_order(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: order_story order_story_status_id_72564f64_fk_order_status_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.order_story
    ADD CONSTRAINT order_story_status_id_72564f64_fk_order_status_id FOREIGN KEY (status_id) REFERENCES public.order_status(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: page_page page_page_author_id_d62cc760_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.page_page
    ADD CONSTRAINT page_page_author_id_d62cc760_fk_auth_user_id FOREIGN KEY (author_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: page_page_tags page_page_tags_page_id_6d99ef76_fk_page_page_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.page_page_tags
    ADD CONSTRAINT page_page_tags_page_id_6d99ef76_fk_page_page_id FOREIGN KEY (page_id) REFERENCES public.page_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: page_page_tags page_page_tags_tag_id_438e06c3_fk_site_info_tag_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.page_page_tags
    ADD CONSTRAINT page_page_tags_tag_id_438e06c3_fk_site_info_tag_id FOREIGN KEY (tag_id) REFERENCES public.site_info_tag(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: page_pagecomment page_pagecomment_page_id_070504d5_fk_page_page_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.page_pagecomment
    ADD CONSTRAINT page_pagecomment_page_id_070504d5_fk_page_page_id FOREIGN KEY (page_id) REFERENCES public.page_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: page_pagecomment page_pagecomment_user_id_9a4af13a_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.page_pagecomment
    ADD CONSTRAINT page_pagecomment_user_id_9a4af13a_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: page_pageimage page_pageimage_page_id_26df7250_fk_page_page_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.page_pageimage
    ADD CONSTRAINT page_pageimage_page_id_26df7250_fk_page_page_id FOREIGN KEY (page_id) REFERENCES public.page_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_product product_product_author_id_9becb979_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product
    ADD CONSTRAINT product_product_author_id_9becb979_fk_auth_user_id FOREIGN KEY (author_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_product_catalog product_product_cata_catalog_id_28182fb8_fk_catalog_c; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product_catalog
    ADD CONSTRAINT product_product_cata_catalog_id_28182fb8_fk_catalog_c FOREIGN KEY (catalog_id) REFERENCES public.catalog_catalog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_product_catalog product_product_cata_product_id_2bb2e50d_fk_product_p; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product_catalog
    ADD CONSTRAINT product_product_cata_product_id_2bb2e50d_fk_product_p FOREIGN KEY (product_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_product product_product_parent_id_01fef757_fk_product_product_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product
    ADD CONSTRAINT product_product_parent_id_01fef757_fk_product_product_id FOREIGN KEY (parent_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_product_recommend_products product_product_reco_from_product_id_f4fd92d7_fk_product_p; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product_recommend_products
    ADD CONSTRAINT product_product_reco_from_product_id_f4fd92d7_fk_product_p FOREIGN KEY (from_product_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_product_recommend_products product_product_reco_to_product_id_38eb3ce3_fk_product_p; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product_recommend_products
    ADD CONSTRAINT product_product_reco_to_product_id_38eb3ce3_fk_product_p FOREIGN KEY (to_product_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_product_tags product_product_tags_product_id_a72c644e_fk_product_product_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product_tags
    ADD CONSTRAINT product_product_tags_product_id_a72c644e_fk_product_product_id FOREIGN KEY (product_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_product_tags product_product_tags_tag_id_2e44bb40_fk_site_info_tag_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_product_tags
    ADD CONSTRAINT product_product_tags_tag_id_2e44bb40_fk_site_info_tag_id FOREIGN KEY (tag_id) REFERENCES public.site_info_tag(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_productcomment product_productcomme_product_id_bf5e6965_fk_product_p; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_productcomment
    ADD CONSTRAINT product_productcomme_product_id_bf5e6965_fk_product_p FOREIGN KEY (product_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_productcomment product_productcomment_user_id_e1a22ad0_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_productcomment
    ADD CONSTRAINT product_productcomment_user_id_e1a22ad0_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_productimage product_productimage_product_id_544084bb_fk_product_product_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_productimage
    ADD CONSTRAINT product_productimage_product_id_544084bb_fk_product_product_id FOREIGN KEY (product_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_productitem product_productitem_product_id_16cb97c8_fk_product_product_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.product_productitem
    ADD CONSTRAINT product_productitem_product_id_16cb97c8_fk_product_product_id FOREIGN KEY (product_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_footer_catalog settings_template_fo_catalog_id_a6917c88_fk_catalog_c; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_catalog
    ADD CONSTRAINT settings_template_fo_catalog_id_a6917c88_fk_catalog_c FOREIGN KEY (catalog_id) REFERENCES public.catalog_catalog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_footer_catalog settings_template_fo_footer_id_129c20b7_fk_settings_; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_catalog
    ADD CONSTRAINT settings_template_fo_footer_id_129c20b7_fk_settings_ FOREIGN KEY (footer_id) REFERENCES public.settings_template_footer(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_footer_page settings_template_fo_footer_id_2e9b2f7f_fk_settings_; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_page
    ADD CONSTRAINT settings_template_fo_footer_id_2e9b2f7f_fk_settings_ FOREIGN KEY (footer_id) REFERENCES public.settings_template_footer(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_footer_list_link settings_template_fo_footer_id_79de261f_fk_settings_; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_list_link
    ADD CONSTRAINT settings_template_fo_footer_id_79de261f_fk_settings_ FOREIGN KEY (footer_id) REFERENCES public.settings_template_footer(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_footer_blogs settings_template_fo_footer_id_defb5a64_fk_settings_; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_blogs
    ADD CONSTRAINT settings_template_fo_footer_id_defb5a64_fk_settings_ FOREIGN KEY (footer_id) REFERENCES public.settings_template_footer(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_footer_galleries settings_template_fo_footer_id_f9131e21_fk_settings_; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_galleries
    ADD CONSTRAINT settings_template_fo_footer_id_f9131e21_fk_settings_ FOREIGN KEY (footer_id) REFERENCES public.settings_template_footer(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_footer_galleries settings_template_fo_gallery_id_79e8c708_fk_gallery_g; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_galleries
    ADD CONSTRAINT settings_template_fo_gallery_id_79e8c708_fk_gallery_g FOREIGN KEY (gallery_id) REFERENCES public.gallery_gallery(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_footer_list_link settings_template_fo_listlink_id_9240aede_fk_site_info; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_list_link
    ADD CONSTRAINT settings_template_fo_listlink_id_9240aede_fk_site_info FOREIGN KEY (listlink_id) REFERENCES public.site_info_listlink(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_footer settings_template_fo_text_info_id_d14ebd5e_fk_site_info; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer
    ADD CONSTRAINT settings_template_fo_text_info_id_d14ebd5e_fk_site_info FOREIGN KEY (text_info_id) REFERENCES public.site_info_textinfo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_footer_blogs settings_template_footer_blogs_blog_id_1531bf6f_fk_blog_blog_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_blogs
    ADD CONSTRAINT settings_template_footer_blogs_blog_id_1531bf6f_fk_blog_blog_id FOREIGN KEY (blog_id) REFERENCES public.blog_blog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_footer_page settings_template_footer_page_page_id_75c91e8d_fk_page_page_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_footer_page
    ADD CONSTRAINT settings_template_footer_page_page_id_75c91e8d_fk_page_page_id FOREIGN KEY (page_id) REFERENCES public.page_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_settingstemplate settings_template_se_footer_id_5ddf2bbc_fk_settings_; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_settingstemplate
    ADD CONSTRAINT settings_template_se_footer_id_5ddf2bbc_fk_settings_ FOREIGN KEY (footer_id) REFERENCES public.settings_template_footer(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_settingstemplate settings_template_se_home_id_661e3df9_fk_home_home; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_settingstemplate
    ADD CONSTRAINT settings_template_se_home_id_661e3df9_fk_home_home FOREIGN KEY (home_id) REFERENCES public.home_home(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_settingstemplate settings_template_se_site_id_a21c4a42_fk_django_si; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.settings_template_settingstemplate
    ADD CONSTRAINT settings_template_se_site_id_a21c4a42_fk_django_si FOREIGN KEY (site_id) REFERENCES public.django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_userlink users_userlink_user_id_5730a6f4_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.users_userlink
    ADD CONSTRAINT users_userlink_user_id_5730a6f4_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_userprofile users_userprofile_user_id_87251ef1_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: nepaday
--

ALTER TABLE ONLY public.users_userprofile
    ADD CONSTRAINT users_userprofile_user_id_87251ef1_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

